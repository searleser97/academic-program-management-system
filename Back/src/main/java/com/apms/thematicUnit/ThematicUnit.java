package com.apms.thematicUnit;

import java.util.List;

import javax.persistence.*;

import com.apms.content.Content;
import com.apms.evaluationSystem.EvaluationSystem;
import com.apms.learningEvaluation.LearningEvaluation;
import com.apms.learningUnit.LearningUnit;
import com.apms.topic.Topic;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class ThematicUnit {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	@Column
	private boolean isFinished;
	@Column
	private String competenceUnit;
	@Column
	private String learningStrategy;
	@ManyToOne
	@JoinColumn(nullable=false)
	private LearningUnit learningUnit;
	@OneToOne
	@JoinColumn(nullable=false)
	private Content content;
	@OneToOne
	@JoinColumn
	private EvaluationSystem evaluationSystem;
	@OneToMany
	@JoinColumn
	private List<Topic> topics;
	@OneToMany
	@JoinColumn
	private List<LearningEvaluation> learningEvaluations;

	public ThematicUnit() {
	}
}
