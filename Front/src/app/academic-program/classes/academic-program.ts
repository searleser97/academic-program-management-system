import {Workplace} from '../../workplace/classes/workplace';

export class AcademicProgram {
  id: number;
  name: string;
  titleName: string;
  workplace: Workplace;

  constructor(id?, name?, titleName?, workplace?) {
    this.id = id || 0;
    this.name = name || '';
    this.titleName = titleName || '';
    this.workplace = workplace || new Workplace();
  }
}

