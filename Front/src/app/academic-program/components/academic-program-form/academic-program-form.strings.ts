export const labels = {
  notFound: '-- No se encontraron opciones --',
  name: 'Nombre',
  titleName: 'Nombre del titulo',
  workplace: 'Lugar de trabajo',
  errors: {
    name: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
      maxlength: 'Escribe información válida',
    },
    titleName: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    }
  }
};

export const placeholders = {
  search: 'Buscar...',
  name: 'Ingenieria en Sistemas Computacionales',
  titleName: 'Ingeniero en Sistemas Computacionales',
};
