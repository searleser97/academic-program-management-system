import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {labels, placeholders} from './academic-program-form.strings';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../../shared/services/form.service';
import {AcademicProgram} from '../../classes/academic-program';
import {UserService} from '../../../shared/services/user.service';
import {RESTService} from '../../../shared/services/REST.service';
import {Workplace} from '../../../workplace/classes/workplace';
import {Observable} from 'rxjs';
import {ALFA_REGEX} from '../../../../environments/environment';

@Component({
  selector: 'app-academic-program-form',
  templateUrl: './academic-program-form.component.html',
  styleUrls: ['./academic-program-form.component.scss']
})
export class AcademicProgramFormComponent implements OnInit {

  @Output() onSubmit = new EventEmitter<boolean>();

  labels = labels;
  placeholders = placeholders;

  academicProgramForm: FormGroup;
  academicProgram: AcademicProgram;

  workplaces: Workplace[];
  filteredWorkplaces: Observable<Workplace[]>;
  searchWorkplacesCtrl = new FormControl();

  failures = 0;
  failUrl = '/programasacademicos/consultar';

  constructor(private formService: FormService, private userService: UserService, private rest: RESTService) {
    this.academicProgramForm = this.formService.newGroup(new AcademicProgram(
      [0],
      ['', Validators.compose([Validators.required, Validators.maxLength(150), Validators.pattern(ALFA_REGEX)])],
      [''],
      [{}, Validators.compose([Validators.required])]
      )
    );
  }

  ngOnInit() {
  }

  getErrors(controlName: string): string[] {
    return Object.keys(this.academicProgramForm.get(controlName).errors || {});
  }

  getAcademicProgram(): AcademicProgram {
    for (const controlName in this.academicProgramForm.controls) {
      if (this.academicProgramForm.controls.hasOwnProperty(controlName)) {
        this.academicProgramForm.controls[controlName].markAsTouched();
      }
    }
    if (this.academicProgramForm.invalid) {
      return null;
    }
    return this.academicProgramForm.getRawValue() as AcademicProgram;
  }

  async initAcademicProgramForm(academicProgram: AcademicProgram) {
    this.academicProgramForm.setValue(academicProgram);
    this.academicProgram = academicProgram;
    if (!await this.getWorkplacesForUser()) {
      return false;
    }
    return true;
  }

  async getWorkplacesForUser() {
    this.workplaces = await this.rest.getAsync<Workplace[]>('workplace/workplacesForUser/' + this.userService.getId());
    if (!this.workplaces) {
      return false;
    }
    this.filteredWorkplaces = this.formService.getFilteredItems(this.searchWorkplacesCtrl, this.workplaces);
    this.academicProgramForm.get('workplace')
      .setValue(this.workplaces.find(opt => opt.id === (this.academicProgram.workplace.id || this.userService.getWorkplace().id)));
    return true;
  }

}
