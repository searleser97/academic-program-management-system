import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RESTService} from '../../../shared/services/REST.service';

@Component({
  selector: 'app-read-academic-program',
  templateUrl: './read-academic-program.component.html',
  styleUrls: ['./read-academic-program.component.scss']
})
export class ReadAcademicProgramComponent implements OnInit {

  academicProgramId: number;
  workplaceId: number;

  constructor(private activatedRoute: ActivatedRoute, private rest: RESTService) {
    this.workplaceId = parseInt(this.activatedRoute.snapshot.params['workplaceId'], 10);
    this.academicProgramId = parseInt(this.activatedRoute.snapshot.params['academicProgramId'], 10);
  }

  ngOnInit() {
  }

}
