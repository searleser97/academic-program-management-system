export const labels = {
  formTitle: 'Editar Programa Academico',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
