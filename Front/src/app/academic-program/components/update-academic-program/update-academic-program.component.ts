import {Component, OnInit, ViewChild} from '@angular/core';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {labels} from './update-academic-program.strings';
import {AcademicProgramFormComponent} from '../academic-program-form/academic-program-form.component';
import {RESTService} from '../../../shared/services/REST.service';
import {AcademicProgram} from '../../classes/academic-program';
import {ActivatedRoute} from '@angular/router';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-update-academic-program',
  templateUrl: './update-academic-program.component.html',
  styleUrls: ['./update-academic-program.component.scss']
})
export class UpdateAcademicProgramComponent implements OnInit {

  @ViewChild(AcademicProgramFormComponent) academicProgramFormComponent: AcademicProgramFormComponent;
  labels: FormContainerLabels = labels;

  academicProgramId: string;

  actionAfter = '/programasacademicos/consultar';

  failures = 0;

  constructor(private rest: RESTService, private activatedRoute: ActivatedRoute, private dialogService: DialogService) {
    this.academicProgramId = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.rest.get<AcademicProgram>('academicProgram/' + this.academicProgramId, (academicProgram, errorMsg) => {
      if (!academicProgram) {
        this.rest.notAvailableServiceMessage(++this.failures, errorMsg, this.actionAfter);
        return;
      }
      this.academicProgramFormComponent.initAcademicProgramForm(academicProgram);
    });
  }

  submit() {
    const academicProgram = this.academicProgramFormComponent.getAcademicProgram();
    if (academicProgram) {
      this.rest.submit('patch', 'academicProgram', academicProgram, this.actionAfter);
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.actionAfter);
  }


}
