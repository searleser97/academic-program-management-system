export const labels = {
  formTitle: 'Registrar Programa Academico',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Registrar',
  cancel: 'Cancelar'
};
