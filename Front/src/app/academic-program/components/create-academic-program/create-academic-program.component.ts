import {Component, OnInit, ViewChild} from '@angular/core';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {labels} from './create-academic-program.strings';
import {RESTService} from '../../../shared/services/REST.service';
import {AcademicProgramFormComponent} from '../academic-program-form/academic-program-form.component';
import {AcademicProgram} from '../../classes/academic-program';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {ActivatedRoute} from '@angular/router';
import {Workplace} from '../../../workplace/classes/workplace';

@Component({
  selector: 'app-create-academic-program',
  templateUrl: './create-academic-program.component.html',
  styleUrls: ['./create-academic-program.component.scss']
})
export class CreateAcademicProgramComponent implements OnInit {

  labels: FormContainerLabels = labels;
  @ViewChild(AcademicProgramFormComponent) academicProgramFormComponent: AcademicProgramFormComponent;

  actionAfter = '/programasacademicos/consultar';

  workplaceId: number;

  constructor(private rest: RESTService, private dialogService: DialogService, private activatedRoute: ActivatedRoute) {
    this.workplaceId = parseInt(this.activatedRoute.snapshot.params['workplaceId'], 10);
  }

  ngOnInit() {
    console.log(this.workplaceId);
    this.academicProgramFormComponent.initAcademicProgramForm(new AcademicProgram(
      0, null, null, new Workplace(this.workplaceId)
    ));
  }

  submit() {
    const academicProgram = this.academicProgramFormComponent.getAcademicProgram();
    if (academicProgram) {
      this.rest.submit('post', 'academicProgram', academicProgram, this.actionAfter);
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.actionAfter);
  }

}
