import {Component, Input, OnInit} from '@angular/core';
import {labels, placeholders} from './find-academic-program.strings';
import {FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {FormService} from '../../../shared/services/form.service';
import {AcademicProgram} from '../../classes/academic-program';
import {RESTService} from '../../../shared/services/REST.service';
import {Workplace} from '../../../workplace/classes/workplace';
import {Router} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-find-academic-program',
  templateUrl: './find-academic-program.component.html',
  styleUrls: ['./find-academic-program.component.scss']
})
export class FindAcademicProgramComponent implements OnInit {

  @Input() academicUnitId = 0;

  labels = labels;
  placeholders = placeholders;

  searchWorkplacesCtrl = new FormControl();

  form: FormGroup;

  workplaces: Workplace[];
  filteredWorkplaces: Observable<Workplace[]>;

  displayedColumns = [
    'id',
    'name',
    'edit'
  ];

  academicPrograms: AcademicProgram[];

  failures = 0;
  failUrl = '/';

  constructor(private formService: FormService, private rest: RESTService, private router: Router,
              private userService: UserService) {
    this.form = new FormGroup({
      workplace: new FormControl({})
    });
  }

  ngOnInit() {
    this.asyncRequests();
  }

  async asyncRequests() {
    if (await this.getWorkplacesForUser()) {
      this.getAcademicProgramsByWorkplaceId(this.workplaces[0].id);
    }
  }

  async getWorkplacesForUser() {
    this.workplaces = await this.rest.getAsync<Workplace[]>('workplace/academicUnits/' + this.userService.getId());
    if (!this.workplaces) {
      return false;
    }
    this.filteredWorkplaces = this.formService.getFilteredItems(this.searchWorkplacesCtrl, this.workplaces);
    this.form.get('workplace').setValue(this.workplaces[0]);
    return true;
  }

  async getAcademicProgramsByWorkplaceId(workplaceId) {
    this.academicPrograms = await this.rest.getAsync<AcademicProgram[]>('academicProgram/academicProgramsByWorkPlaceId/' + workplaceId);
    if (!this.academicPrograms) {
      return false;
    }
    return true;
  }

  edit(id) {
    this.router.navigate(['programasacademicos/editar/' + id]);
  }

  view(id) {
    this.router.navigate(['programasacademicos/leer/' + this.form.get('workplace').value['id'] + '/' + id]);
  }

  add() {
    this.router.navigate(['programasacademicos/registrar/' + this.form.get('workplace').value['id']]);
  }

}
