export const labels = {
  formTitle: 'Editar Usuario',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
