import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './update-user.strings';
import {UserFormComponent} from '../user-form/user-form.component';
import {ActivatedRoute} from '@angular/router';
import {RESTService} from '../../../shared/services/REST.service';
import {User} from '../../classes/user';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit {

  @ViewChild(UserFormComponent) userFormComponent: UserFormComponent;

  labels = labels;
  userId: string;

  actionAfter = '/usuarios/consultar';

  constructor(private activatedRoute: ActivatedRoute, private rest: RESTService, private dialogService: DialogService) {
    this.userId = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.getUser(this.userId);
  }

  async getUser(userId) {
    const user = await this.rest.getAsync<User>('user/' + userId);
    if (user) {
      this.userFormComponent.initUserForm(user);
    }
  }

  submit() {
    const user = this.userFormComponent.getUser();
    if (user) {
      this.rest.submit('patch', 'user', user, this.actionAfter);
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.actionAfter);
  }

}
