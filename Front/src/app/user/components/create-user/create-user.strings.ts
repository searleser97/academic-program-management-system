export const labels = {
  formTitle: 'Registrar Usuario',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Registrar',
  cancel: 'Cancelar'
};
