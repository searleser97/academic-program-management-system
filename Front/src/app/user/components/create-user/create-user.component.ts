import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './create-user.strings';
import {UserFormComponent} from '../user-form/user-form.component';
import {User} from '../../classes/user';
import {RESTService} from '../../../shared/services/REST.service';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {HumanResource} from '../../../human-resource/classes/human-resource';
import {Position} from '../../../position/classes/position';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.scss']
})
export class CreateUserComponent implements OnInit {

  @ViewChild(UserFormComponent) userFormComponent: UserFormComponent;

  labels = labels;

  actionAfter = '/usuarios/consultar';

  constructor(private rest: RESTService, private dialogService: DialogService) {
  }

  ngOnInit() {
    this.userFormComponent.initUserForm(new User(
      null, null, null, null, null,
      new HumanResource(null, null, null, null, null,
        [
          new Position(1)
        ]
      )));
  }

  submit() {
    const user = this.userFormComponent.getUser();
    if (user) {
      this.rest.submitAsync<User>('post', 'user', user, this.actionAfter);
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.actionAfter);
  }
}
