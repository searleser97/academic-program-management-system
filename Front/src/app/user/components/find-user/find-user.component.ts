import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {labels, placeholders} from './find-user.strings';
import {FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {FormService} from '../../../shared/services/form.service';
import {User} from '../../classes/user';
import {MatSort, MatTableDataSource} from '@angular/material';
import {RESTService} from '../../../shared/services/REST.service';
import {Router} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';
import {Role} from '../../../role/classes/role';

@Component({
  selector: 'app-find-user',
  templateUrl: './find-user.component.html',
  styleUrls: ['./find-user.component.scss']
})
export class FindUserComponent implements OnInit {

  @Input() academicUnitId = 0;

  labels = labels;
  placeholders = placeholders;

  form: FormGroup;

  roles: Role[];
  filteredRoles: Observable<Role[]>;
  searchRolesCtrl = new FormControl();

  displayedColumns = [
    'humanResource.name',
    'humanResource.firstSurname',
    'humanResource.secondSurname',
    'humanResource.title',
    'roles',
    'humanResource.workplace',
    'edit',
    'delete'
  ];

  users: User[];
  usersDataSource: MatTableDataSource<User>;

  failures = 0;
  failUrl = '/usuarios/consultar';

  rolesId = '-1';

  constructor(private formService: FormService, private rest: RESTService, private router: Router,
              public userService: UserService) {
    this.form = new FormGroup({
      role: new FormControl()
    });
    this.usersDataSource = new MatTableDataSource<User>();
  }

  ngOnInit() {
    this.asyncRequests();
  }

  async asyncRequests() {
    if (!await this.getRoles() || !await this.getActiveUsersForUser()) {
      return false;
    }
    return true;
  }

  async getRoles() {
    let result = true;
    let roles = await this.rest.getAsync<Role[]>('role/rolesByUserId/' + this.userService.getId());
    if (!roles) {
      roles = [];
      result = false;
    }
    this.roles = roles;
    this.filteredRoles = this.formService.getFilteredItems(this.searchRolesCtrl, this.roles);
    return result;
  }

  async getActiveUsersForUserByRole(roleId) {
    let result = true;
    this.rolesId = roleId;
    let users = await this.rest.getAsync<User[]>('user/activeUsersForUserByRole/' + this.userService.getId() + '/' + roleId);
    if (!users) {
      users = [];
      result = false;
    }
    this.users = users;
    this.usersDataSource.data = this.users;
    return result;
  }

  async getActiveUsersForUser() {
    let result = true;
    this.rolesId = '-1';
    const users = await this.rest.getAsync<User[]>('user/activeUsersForUser/' + this.userService.getId());
    if (!users) {
      this.users = [];
      result = false;
    }
    this.users = users;
    this.usersDataSource.data = this.users;
    return result;
  }

  edit(id) {
    this.router.navigate(['/usuarios/editar/' + id]);
  }

  view(id) {
    // this.router.navigate(['/planesdeestudio/leer/' + id]);
  }

  add() {
    this.router.navigate(['/usuarios/registrar']);
  }

  async delete(id) {
    const success = await this.rest.deleteAsync('user/' + id, '¿Está seguro que desea eliminar al usuario?');
    if (success) {
      this.rolesId === '-1' ? this.getActiveUsersForUser() : this.getActiveUsersForUserByRole(this.rolesId);
    }
  }

  rolesToString(roles: Role[]): string {
    if (!roles.length) {
      return '';
    }
    let str = roles[0].name;
    for (let i = 1; i < roles.length; i++) {
      str += '/' + roles[i].name;
    }
    return str;
  }
}
