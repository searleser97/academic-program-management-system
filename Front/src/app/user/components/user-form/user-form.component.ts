import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {labels, placeholders} from './user-form.strings';
import {FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../../shared/services/form.service';
import {User} from '../../classes/user';
import {HumanResourceFormComponent} from '../../../human-resource/components/human-resource-form/human-resource-form.component';
import {Role} from '../../../role/classes/role';
import {PASSWD_REGEX} from '../../../../environments/environment';
import {UserService} from '../../../shared/services/user.service';
import {RESTService} from '../../../shared/services/REST.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {

  @Input() hideAdvancedOpts = false;
  @Output() onSubmit = new EventEmitter<boolean>();

  @ViewChild(HumanResourceFormComponent) humanResourceFormComponent: HumanResourceFormComponent;

  labels = labels;
  placeholders = placeholders;

  userForm: FormGroup;
  user: User;

  roles: Role[];

  failures = 0;
  failUrl = '/usuarios/consultar';

  constructor(private formService: FormService, private userService: UserService, private rest: RESTService) {
    this.userForm = this.formService.newGroup(new User(
      [0],
      [''],
      ['', Validators.compose([Validators.email])],
      ['', Validators.compose([Validators.pattern(PASSWD_REGEX)])],
      [false],
      [{}, Validators.compose([Validators.required])],
      [[], Validators.compose([Validators.required])]
    ));
  }

  ngOnInit() {
  }

  getErrors(controlName: string): string[] {
    const errors = Object.keys(this.userForm.get(controlName).errors || {});
    return errors.length ? [errors[0]] : [];
  }

  getUser(): User {

    const hr = this.humanResourceFormComponent.getHumanResource();

    for (const controlName in this.userForm.controls) {
      if (this.userForm.controls.hasOwnProperty(controlName)) {
        this.userForm.controls[controlName].markAsTouched();
      }
    }

    if (!hr) {
      return null;
    }
    this.userForm.get('humanResource').setValue(hr);
    return this.userForm.invalid ? null : this.userForm.getRawValue() as User;
  }

  async initUserForm(user: User) {
    if (!await this.humanResourceFormComponent.initHumanResourceForm(user.humanResource)) {
      return false;
    }
    this.userForm.setValue(user);
    this.user = user;

    this.getRolesAync(this.humanResourceFormComponent.humanResourceForm.get('workplace').value['id']);

    this.humanResourceFormComponent.humanResourceForm.get('workplace').valueChanges.subscribe(value => {
      this.getRolesAync(value.id);
    });
    return true;
  }

  async getRolesAync(workplaceId) {
    this.roles = await this.rest.getAsync<Role[]>('role/rolesByUserIdAndWorkplaceId/' + this.userService.getId() + '/' + workplaceId);
    if (!this.roles) {
      return false;
    }
    const selectedRoles = [];
    for (const role of this.user.roles) {
      selectedRoles.push(this.roles.find(opt => opt.id === role.id));
    }
    this.userForm.get('roles').setValue(selectedRoles);
    return true;
  }
}
