export const labels = {
  id: 'Matrícula o Nombre',
  advancedOptions: 'Opciones Avanzadas',
  email: 'Correo',
  password: 'Establecer Contraseña',
  access: 'Restringir Acceso',
  roles: 'Cargos',
  errors: {
    email: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
      email: 'Escribe información válida'
    },
    password: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
      minlength: 'Escribe información válida'
    },
    roles: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
      minlength: 'Escribe información válida'
    }
  }
};

export const placeholders = {
  email: 'ejemplo@empleado.ipn.mx',
  password: '********',
  id: '2014090680'
};
