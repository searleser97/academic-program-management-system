import {HumanResource} from '../../human-resource/classes/human-resource';
import {Role} from '../../role/classes/role';

export class User {
  id: number;
  token: string;
  email: string;
  password: string;
  accountBlocked: boolean;
  humanResource: HumanResource;
  roles: Role[];

  constructor(id?, token?, email?, password?, isAccountBlocked?, humanResource?, roles?) {
    this.id = id || 0;
    this.token = token || '';
    this.email = email || '';
    this.password = password || '';
    this.accountBlocked = isAccountBlocked || false;
    this.humanResource = humanResource || new HumanResource();
    this.roles = roles || [];
  }
}
