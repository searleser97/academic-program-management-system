export class WorkplaceType {
  id: number;
  name: string;
  abbreviation: string;
  description: string;

  constructor(id?, name?, abbreviation?, description?) {
    this.id = id || 0;
    this.name = name || '';
    this.abbreviation = abbreviation || '';
    this.description = description || '';
  }
}
