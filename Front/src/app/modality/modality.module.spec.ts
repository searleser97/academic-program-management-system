import { ModalityModule } from './modality.module';

describe('ModalityModule', () => {
  let studyPlanModeModule: ModalityModule;

  beforeEach(() => {
    studyPlanModeModule = new ModalityModule();
  });

  it('should create an instance', () => {
    expect(studyPlanModeModule).toBeTruthy();
  });
});
