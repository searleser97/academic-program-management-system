import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalityRoutingModule } from './modality-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ModalityRoutingModule
  ],
  declarations: []
})
export class ModalityModule { }
