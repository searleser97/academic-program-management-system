import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {labels, placeholders} from './human-resource-form.strings';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {FormService} from '../../../shared/services/form.service';
import {Workplace} from '../../../workplace/classes/workplace';
import {Position} from '../../../position/classes/position';
import {Title} from '../../../title/classes/title';
import {HumanResource} from '../../classes/human-resource';
import {RESTService} from '../../../shared/services/REST.service';
import {UserService} from '../../../shared/services/user.service';
import {ALFA_REGEX} from '../../../../environments/environment';

@Component({
  selector: 'app-human-resource-form',
  templateUrl: './human-resource-form.component.html',
  styleUrls: ['./human-resource-form.component.scss']
})
export class HumanResourceFormComponent implements OnInit {

  @Input() hidePositionField = false;
  @Output() submit = new EventEmitter<boolean>();

  labels = labels;
  placeholders = placeholders;

  titles: Title[];
  positions: Position[];
  workplaces: Workplace[];

  filteredTitles: Observable<Title[]>;
  filteredPositions: Observable<Position[]>;
  filteredWorkplaces: Observable<Workplace[]>;

  searchTitlesCtrl = new FormControl();
  searchPositionsCtrl = new FormControl();
  searchWorkplacesCtrl = new FormControl();

  humanResourceForm: FormGroup;
  humanResource: HumanResource;

  failures = 0;

  failUrl = '/recursoshumanos/consultar';

  constructor(private formService: FormService, private rest: RESTService, private userService: UserService) {
    this.humanResourceForm = this.formService.newGroup(new HumanResource(
      [0],
      ['', Validators.compose([Validators.required, Validators.pattern(ALFA_REGEX)])],
      ['', Validators.compose([Validators.pattern(ALFA_REGEX)])],
      ['', Validators.compose([Validators.pattern(ALFA_REGEX)])],
      [{}, Validators.compose([Validators.required])],
      [[], Validators.compose([Validators.required])],
      [{}, Validators.compose([Validators.required])]
    ));
  }

  ngOnInit() {
  }

  getErrors(controlName: string): string[] {
    return Object.keys(this.humanResourceForm.get(controlName).errors || {});
  }

  getHumanResource(): HumanResource {
    for (const controlName in this.humanResourceForm.controls) {
      if (this.humanResourceForm.controls.hasOwnProperty(controlName)) {
        this.humanResourceForm.controls[controlName].markAsTouched();
      }
    }

    console.log(this.humanResourceForm.value);

    if (this.humanResourceForm.invalid) {
      return null;
    }
    return this.humanResourceForm.getRawValue();
  }

  async initHumanResourceForm(humanResource: HumanResource) {
    this.humanResourceForm.setValue(humanResource);
    this.humanResource = humanResource;

    if (!await this.getPositions() || !await this.getTitles() || !await this.getWorkplacesForUser()) {
      return false;
    }
    return true;
  }

  // API CALLS
  async getPositions(): Promise<boolean> {
    this.positions = await this.rest.getAsync<Position[]>('position');
    if (!this.positions) {
      return false;
    }
    this.filteredPositions = this.formService.getFilteredItems(this.searchPositionsCtrl, this.positions);

    const selectedPositions = [];
    for (const position of this.humanResource.positions) {
      selectedPositions.push(this.positions.find(opt => opt.id === position.id));
    }
    this.humanResourceForm.get('positions').setValue(selectedPositions);
    return true;
  }

  async getTitles() {
    this.titles = await this.rest.getAsync<Title[]>('title');
    if (!this.titles) {
      return false;
    }
    this.filteredTitles = this.formService.getFilteredItems(this.searchTitlesCtrl, this.titles);
    this.humanResourceForm.get('title')
      .setValue(this.titles.find(opt => opt.id === this.humanResource.title.id));
    return true;
  }

  async getWorkplacesForUser() {
    this.workplaces = await this.rest.getAsync<Workplace[]>('workplace/workplacesForUser/' + this.userService.getId());
    if (!this.workplaces) {
      return false;
    }
    this.filteredWorkplaces = this.formService.getFilteredItems(this.searchWorkplacesCtrl, this.workplaces);
    this.humanResourceForm.get('workplace')
      .setValue(this.workplaces.find(opt => opt.id === (this.humanResource.workplace.id || this.userService.getWorkplace().id)));
    return true;
  }

  // END API CALLS
}
