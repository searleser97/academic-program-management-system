export const labels = {
  formTitle: 'Registrar Recurso Humano',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Registrar',
  cancel: 'Cancelar'
};
