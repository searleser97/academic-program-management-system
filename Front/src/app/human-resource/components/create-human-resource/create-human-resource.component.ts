import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './create-human-resource.strings';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {HumanResourceFormComponent} from '../human-resource-form/human-resource-form.component';
import {RESTService} from '../../../shared/services/REST.service';
import {HumanResource} from '../../classes/human-resource';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-create',
  templateUrl: './create-human-resource.component.html',
  styleUrls: ['./create-human-resource.component.scss']
})
export class CreateHumanResourceComponent implements OnInit {

  @ViewChild(HumanResourceFormComponent) humanResourceFormComponent: HumanResourceFormComponent;
  labels: FormContainerLabels = labels;

  actionAfter = '/recursoshumanos/consultar/';

  constructor(private rest: RESTService, private dialogService: DialogService) {
  }

  ngOnInit() {
    this.humanResourceFormComponent.initHumanResourceForm(new HumanResource());
  }

  submit() {
    const humanResource = this.humanResourceFormComponent.getHumanResource();
    if (humanResource) {
      this.rest.submit('post', 'humanResource', humanResource, this.actionAfter);
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.actionAfter);
  }

}
