import {Component, OnInit, ViewChild} from '@angular/core';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {labels} from './update-human-resource.strings';
import {HumanResourceFormComponent} from '../human-resource-form/human-resource-form.component';
import {ActivatedRoute} from '@angular/router';
import {RESTService} from '../../../shared/services/REST.service';
import {HumanResource} from '../../classes/human-resource';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-update-human-resource',
  templateUrl: './update-human-resource.component.html',
  styleUrls: ['./update-human-resource.component.scss']
})
export class UpdateHumanResourceComponent implements OnInit {

  @ViewChild(HumanResourceFormComponent) humanResourceFormComponent: HumanResourceFormComponent;

  labels: FormContainerLabels = labels;
  humanResourceId: string;

  actionAfter = '/recursoshumanos/consultar';

  failures = 0;

  constructor(private rest: RESTService, private activatedRoute: ActivatedRoute, private dialogService: DialogService) {
    this.humanResourceId = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.rest.get<HumanResource>('humanResource/' + this.humanResourceId, (humanResource, errorMsg) => {
      if (!humanResource) {
        this.rest.notAvailableServiceMessage(++this.failures, errorMsg, this.actionAfter);
        return;
      }
      this.humanResourceFormComponent.initHumanResourceForm(humanResource);
    });
  }

  submit() {
    const humanResource = this.humanResourceFormComponent.getHumanResource();
    if (humanResource) {
      this.rest.submit('patch', 'humanResource', humanResource, this.actionAfter);
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.actionAfter);
  }

}
