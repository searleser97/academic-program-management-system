export const labels = {
  formTitle: 'Editar Recurso Humano',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
