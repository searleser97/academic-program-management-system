import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {labels, placeholders} from './find-human-resource.strings';
import {FormControl, FormGroup} from '@angular/forms';
import {FormService} from '../../../shared/services/form.service';
import {Observable} from 'rxjs';
import {HumanResource} from '../../classes/human-resource';
import {Position} from '../../../position/classes/position';
import {MatSort, MatTableDataSource} from '@angular/material';
import {RESTService} from '../../../shared/services/REST.service';
import {Router} from '@angular/router';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-find-human-resource',
  templateUrl: './find-human-resource.component.html',
  styleUrls: ['./find-human-resource.component.scss']
})
export class FindHumanResourceComponent implements OnInit {

  @Input() workplaceId = 0;

  labels = labels;
  placeholders = placeholders;

  searchPositionsCtrl = new FormControl();

  form: FormGroup;

  positions: Position[];
  filteredPositions: Observable<Position[]>;

  displayedColumns = [
    'name',
    'firstSurname',
    'secondSurname',
    'title',
    'positions',
    'workplace',
    'edit',
    'delete'
  ];

  humanResources: HumanResource[];
  humanResourcesDataSource: MatTableDataSource<HumanResource>;

  failures = 0;
  failUrl = '';

  positionId = '-1';

  constructor(private formService: FormService, private rest: RESTService, private router: Router,
              private userService: UserService) {
    this.form = new FormGroup({
      position: new FormControl({})
    });
    this.humanResourcesDataSource = new MatTableDataSource<HumanResource>(this.humanResources);
  }

  ngOnInit() {
    this.asyncRequests();
  }

  async asyncRequests() {
    if (!await this.getPositions() || !await this.getHumanResourcesByWorkplaceId()) {
      return false;
    }
    return true;
  }

  async getPositions() {
    let result = true;
    let positions = await this.rest.getAsync<Position[]>('position');
    if (!positions) {
      positions = [];
      result = false;
    }
    this.positions = positions;
    this.filteredPositions = this.formService.getFilteredItems(this.searchPositionsCtrl, this.positions);
    return result;
  }

  async getHumanResourcesByWorkplaceId() {
    this.positionId = '-1';
    const humanResources = await this.rest.getAsync<HumanResource[]>(
      'humanResource/humanResourcesByWorkplaceId/' + this.userService.getWorkplace().id);
    if (!humanResources) {
      this.humanResources = [];
      this.humanResourcesDataSource.data = this.humanResources;
      return false;
    }
    this.humanResources = humanResources;
    this.humanResourcesDataSource.data = this.humanResources;
    return true;
  }

  async getHumanResourcesByPositionId(positionId) {
    this.positionId = positionId;
    const humanResources = await this.rest.getAsync<HumanResource[]>(
      'humanResource/humanResourcesByWorkplaceIdAndPositionId/' + this.userService.getWorkplace().id + '/' + positionId);
    if (!humanResources) {
      this.humanResources = [];
      this.humanResourcesDataSource.data = this.humanResources;
      return false;
    }
    this.humanResources = humanResources;
    this.humanResourcesDataSource.data = this.humanResources;
    return true;
  }

  edit(id) {
    this.router.navigate(['/recursoshumanos/editar/' + id]);
  }

  view(id) {
    // console.log('view: ' + id);
  }

  add() {
    this.router.navigate(['/recursoshumanos/registrar']);
  }

  async delete(id) {
    const success = await this.rest.deleteAsync('humanResource/' + id, '¿Está seguro de eliminar al recurso humano?');
    if (success) {
      this.positionId === '-1' ? this.getHumanResourcesByWorkplaceId() : this.getHumanResourcesByPositionId(this.positionId);
    }
  }

  arrayToString(positions: any[]): string {
    if (!positions.length) {
      return '';
    }
    let str = positions[0].name;
    for (let i = 1; i < positions.length; i++) {
      str += '/' + positions[i].name;
    }
    return str;
  }
}
