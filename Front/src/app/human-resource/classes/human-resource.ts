import {Title} from '../../title/classes/title';
import {Position} from '../../position/classes/position';
import {Workplace} from '../../workplace/classes/workplace';

export class HumanResource {
  id: number;
  name: string;
  firstSurname: string;
  secondSurname: string;
  title: Title;
  positions: Position[];
  workplace: Workplace;

  constructor(id?, name?, firstSurname?, secondSurname?, title?, positions?, workplace?) {
    this.id = id || 0;
    this.name = name || '';
    this.firstSurname = firstSurname || '';
    this.secondSurname = secondSurname || '';
    this.title = title ? title : new Title();
    this.positions = positions || [];
    this.workplace = workplace || new Workplace();
  }
}
