import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UaManagementRoutingModule } from './ua-management-routing.module';
import { RegistrarProgramaSinteticoComponent } from './components/registrar-programa-sintetico/registrar-programa-sintetico.component';
import {NgModel, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from '../shared/modules/material/material.module';
import {UtilsModule} from '../utils/utils.module';
import {RegistrarProgramaExtensoComponent} from './components/registrar-programa-extenso/registrar-programa-extenso.component';
import { RegistrarUnidadTematicaComponent } from './components/registrar-unidad-tematica/registrar-unidad-tematica.component';
import { RegistrarRelacionPracticasComponent } from './components/registrar-relacion-practicas/registrar-relacion-practicas.component';
import { RegistrarSistemaEvaluacionComponent } from './components/registrar-sistema-evaluacion/registrar-sistema-evaluacion.component';
import {BibliographyComponent} from './components/bibliography/bibliography.component';
import {MainUamComponent} from './components/main-uam/main-uam.component';
import {AuthorComponent} from './components/author/author.component';
import { RegistrarContenidoComponent } from './components/registrar-contenido/registrar-contenido.component';
import { RegistrarEvaluacionAcreditacionComponent } from './components/registrar-evaluacion-acreditacion/registrar-evaluacion-acreditacion.component';
import {MatTabsModule} from '@angular/material';
import { AddAccreditationTypeComponent } from './components/add-accreditation-type/add-accreditation-type.component';
import { RegistrarTemasUaComponent } from './components/registrar-temas-ua/registrar-temas-ua.component';
import { RegistrarSubtemasUaComponent } from './components/registrar-subtemas-ua/registrar-subtemas-ua.component';
import { RegistrarEvalLearningsComponent } from './components/registrar-eval-learnings/registrar-eval-learnings.component';
import { TeachingProfileComponent } from './components/teaching-profile/teaching-profile.component';
import { RegistrarElaboracionAutorizacionComponent } from './components/registrar-elaboracion-autorizacion/registrar-elaboracion-autorizacion.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MaterialModule,
    UtilsModule,
    UaManagementRoutingModule,
    MatTabsModule
  ],
  declarations: [
    RegistrarProgramaSinteticoComponent,
    RegistrarProgramaExtensoComponent,
    RegistrarUnidadTematicaComponent,
    RegistrarRelacionPracticasComponent,
    RegistrarSistemaEvaluacionComponent,
    BibliographyComponent,
    MainUamComponent,
    AuthorComponent,
    RegistrarContenidoComponent,
    RegistrarEvaluacionAcreditacionComponent,
    AddAccreditationTypeComponent,
    RegistrarTemasUaComponent,
    RegistrarSubtemasUaComponent,
    RegistrarEvalLearningsComponent,
    TeachingProfileComponent,
    RegistrarElaboracionAutorizacionComponent
  ]
})
export class UaManagementModule { }


