import {LearningUnit} from '../../learning-unit/classes/learning-unit';
import {Practice} from './Practice';
import {PracticeRelationEvaluation} from './PracticeRelationEvaluation';

export class PracticeRelation {
  id: number;
  finished: boolean;
  accreditation: string;
  totalHours: number;
  learningUnit: LearningUnit;
   practices: Practice[];
  practiceRelationEvaluations: PracticeRelationEvaluation[];
  constructor(
    id: number = 0, accreditation: string = '', totalHours: number = 0,
    learningUnit: LearningUnit = new LearningUnit(),
    practices: Practice[] = [new Practice()],
    practiceRelationEvaluation: PracticeRelationEvaluation[] = [new PracticeRelationEvaluation()],
    f: boolean = false
  ) {
    this.id = id;
    this.accreditation = accreditation;
    this.totalHours = totalHours;
    this.learningUnit = learningUnit;
    this.practices = practices;
    this.practiceRelationEvaluations = practiceRelationEvaluation;
    this.finished = f;
  }
}
