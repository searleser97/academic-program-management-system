import {SchoolingGrade} from './SchoolingGrade';
import {Knowledge} from './Knowledge';
import {ProfessionalExperience} from './ProfessionalExperience';
import {Ability} from './Ability';
import {Attitude} from './Attitude';

export class TeachingProfile {
  id: number;
  schoolingGrades: SchoolingGrade[];
  knowledges: Knowledge[];
  professionalExperiences: ProfessionalExperience[];
  ability: Ability[];
  attitude: Attitude[];

  constructor(id: number = 0,
              schoolingGrades: SchoolingGrade[] = [new SchoolingGrade()],
              knowledges: Knowledge[] = [new Knowledge()],
              professionalExperiences: ProfessionalExperience[] = [new ProfessionalExperience()],
              ability: Ability[] = [new Ability()],
              attitude: Attitude[] = [new Attitude()]
  ) {
    this.id = id;
    this.schoolingGrades = schoolingGrades;
    this.knowledges = knowledges;
    this.professionalExperiences = professionalExperiences;
    this.ability = ability;
    this.attitude = attitude;
  }
}
