export  class PracticeRelationEvaluation {
   id: number;
   name: string;
   percentage: number;

  constructor(id: number = 0, name: string = '', percentage: number = 0) {
    this.id = id;
    this.name = name;
    this.percentage = percentage;
  }
}
