export class BibliographyType {
  id: number;
  abbreviation: string;
  type: string;
  description: string;
  constructor(type: string = null, ab: string = null, description: string = null, id: number = 0)
  {
    this.type = type;
    this.abbreviation = ab;
    this.description = description;
    this.id = id;
  }
}
