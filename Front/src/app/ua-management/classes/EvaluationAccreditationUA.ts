import {EvaluationUA} from './EvaluationUA';
import {AccreditationType} from './AccreditationType';

export class EvaluationAccreditationUA {
  id: number;
  evaluationUA: EvaluationUA[];
  accreditationType: AccreditationType[];
  constructor(id: number = 0, ev: EvaluationUA[] = [new EvaluationUA()], at: AccreditationType[] = [new AccreditationType()]) {
    this.evaluationUA = ev;
    this.accreditationType = at;
    this.id = id;
  }
}
