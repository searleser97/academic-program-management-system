import {LearningUnit} from '../../learning-unit/classes/learning-unit';
import { Type } from './Type';
import { AssignedTime } from './AssignedTime';
import { TeachingProfile } from './TeachingProfile';
import { Modality } from './Modality';
import { Teaching } from './Teaching';
import { type } from 'os';

export class ExtensiveProgram {
    id: number;
    educationalIntention: String;
    validity: number;
    types : Type[];
    learningUnit: LearningUnit;
    assignedTime: AssignedTime;
    teachingProfile: TeachingProfile;
    modality: Modality;
    teaching: Teaching;

    constructor(
        id?,
        educationalIntention?,
        validity?,
        types ?,
        learningUnit?,
        assignedTime?,
        teachingProfile?,
        modality?,
        teaching?,
    ){
        this.id = id || 0;
        this.educationalIntention = educationalIntention || '';
        this.validity = validity || 0;
        this.types  = types || [];
        this.learningUnit = learningUnit || new LearningUnit();
        this.assignedTime = assignedTime || new AssignedTime();
        this.teachingProfile = teachingProfile || new TeachingProfile();
        this.modality = modality || new Modality(); 
        this.teaching = teaching || new Teaching(); 
    }
}  