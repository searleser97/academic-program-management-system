export class LearningEvaluation {
  id: number;
  name: string;
  percentage: number;
  constructor(id: number = 0, name: string = '', p: number = 0) {
    this.id = id;
    this.name = name;
    this.percentage = p;
  }
}
