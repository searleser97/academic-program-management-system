import { AcademicLevel } from './AcademicLevel';

export class SchoolingGrade{
    id : number;
    specialty : string;
    academicLevel : AcademicLevel;
    constructor(id : number = 0, specialty : string = '',
            academicLevel : AcademicLevel = new AcademicLevel() ){
        this.id = id;
        this.specialty = specialty
        this.academicLevel  = academicLevel;
    }
}