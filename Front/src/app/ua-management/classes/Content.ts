import {SyntheticProgram} from './SyntheticProgram';

export class Content {
  id: number;
  name: string;
  number: number;
  constructor(id: number = 0, name: string = '', number: number = 1) {
    this.id = id;
    this.name = name;
    this.number = number;
  }
}
