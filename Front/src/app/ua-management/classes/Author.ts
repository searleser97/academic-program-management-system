export class Author
{
  id: number;
  name: string;
  parentalSurname: string;
  maternalSurname: string;
  constructor(name: string = null, parentalSurname: string = null, maternalSurname: string = null) {
    this.id = 0;
    this.name = name;
    this.parentalSurname = parentalSurname;
    this.maternalSurname = maternalSurname;
  }
}
