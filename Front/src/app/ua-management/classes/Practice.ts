import {ThematicUnit} from './ThematicUnit';

export class Practice {
  number: number;
  name: string;
  thematicUnit: ThematicUnit;
  length: number;
  placeOfPractice: string;
  constructor(number: number = 0, name: string = '', thematicUnit: ThematicUnit = new ThematicUnit(), length: number = 0, plc: string = '')
  {
    this.name = name;
    this.number = number;
    this.thematicUnit = thematicUnit;
    this.length = length;
    this.placeOfPractice = plc;
  }
}
