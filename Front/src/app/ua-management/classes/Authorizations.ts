import {HumanResource} from '../../human-resource/classes/human-resource';
import {Academy} from './Academy';
import {SyntheticProgram} from './SyntheticProgram';

export class Authorizations {
  id: number;
  elaboratedBy: Academy;
  revisedBy: HumanResource;
  authorizedBy: HumanResource;
  approvedBy: HumanResource;
  syntheticProgram: SyntheticProgram;

  constructor(id: number, elaboratedBy: HumanResource, revisedBy: HumanResource, authorizedBy: HumanResource, approvedBy: HumanResource) {
    this.id = id;
    this.elaboratedBy = elaboratedBy;
    this.revisedBy = revisedBy;
    this.authorizedBy = authorizedBy;
    this.approvedBy = approvedBy;
  }
}