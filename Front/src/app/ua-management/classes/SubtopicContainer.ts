import {Subtopic} from './Subtopic';

export class SubtopicContainer {
  parentTopicNumber: number;
  subtopics: Subtopic[];
}
