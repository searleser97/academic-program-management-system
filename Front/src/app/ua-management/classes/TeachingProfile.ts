import {SchoolingGrade} from './SchoolingGrade';
import {Knowledge} from './Knowledge';
import {ProfessionalExperience} from './ProfessionalExperience';
import {Ability} from './Ability';
import {Attitude} from './Attitude';

export class TeachingProfile {
  id: number;
  schoolingGrades: string;
  knowledges: string;
  professionalExperiences: string;
  ability: string;
  attitude: string;

  constructor(id: number = 0,
              schoolingGrades: string = '',
              knowledges: string = '',
              professionalExperiences: string = '',
              ability: string = '',
              attitude: string = ''
  ) {
    this.id = id;
    this.schoolingGrades = schoolingGrades;
    this.knowledges = knowledges;
    this.professionalExperiences = professionalExperiences;
    this.ability = ability;
    this.attitude = attitude;
  }
}
