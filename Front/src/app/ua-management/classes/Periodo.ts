
export class Periodo {
  period: any;
  thematicUnit: any;
  evalProcedure: any;
  percentage: any;
  constructor(period = ' ', thematicUnit = '', evalProcedure = ' ', percentage = 1)
  {
    this.period = period;
    this.thematicUnit = thematicUnit;
    this.evalProcedure = evalProcedure;
    this.percentage = percentage;
  }
}
