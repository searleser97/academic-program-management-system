import {Content} from './Content';
import {Subtopic} from './Subtopic';

export class Topic {
  id: number;
  name: string;
  number: number;
  subtopics: Subtopic[];
  theoricHours: number;
  practicalHours: number;
  autonomousHours: number;
  constructor(
    id: number = 0,
    name: string = '',
    number: number = 0,
    subtopics: Subtopic[] = [new Subtopic()],
    theoricHours: number = 0,
    practicalHours: number = 0,
    autonomousHours: number = 0
  ) {
    this.id = id;
    this.name = name;
    this.number = number;
    this.subtopics = subtopics;
    this.theoricHours = theoricHours;
    this.practicalHours = practicalHours;
    this.autonomousHours = autonomousHours;
  }
}
