export class AssignedTime{
    id: number;
    theoreticalHourWeek: number;
    practiceHourWeek: number;
    theoreticalHourSemester: number;
    practiceHourSemester: number;
    automaticTeaching: number;
    totalSemsterHour: number;
    constructor( id: number=0, theoreticalHourWeek: number=0,PracticeHourWeek: number=0,
        theoreticalHourSemester: number=0,practiceHourSemester: number=0,
        automaticTeaching: number=0,totalSemsterHour: number=0
    ){
        this.id = id;
        this.theoreticalHourWeek = theoreticalHourWeek;
        this.practiceHourWeek = PracticeHourWeek;
        this.theoreticalHourSemester = theoreticalHourSemester;
        this.practiceHourSemester = practiceHourSemester;
        this.automaticTeaching = automaticTeaching;
        this.totalSemsterHour = totalSemsterHour;
    }
}