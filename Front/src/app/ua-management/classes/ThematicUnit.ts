import {LearningUnit} from '../../learning-unit/classes/learning-unit';
import {Topic} from './Topic';
import {LearningEvaluation} from './LearningEvaluation';
import {Content} from './Content';

export class ThematicUnit {
  id: number;
  learningUnit: LearningUnit;
  content: Content;
  competenceUnit: string;
  topics: Topic[];
  learningStrategy: string;
  learningEvaluation: LearningEvaluation[];
  isFinished: boolean;

  constructor(id: number  = 0,
    learningUnit: LearningUnit = new LearningUnit(),
    content: Content = new Content(),
    competenceUnit: string = '',
    topics: Topic[] = [new Topic()],
    learningStrategy: string = '',
    learningEvaluation: LearningEvaluation[] = [new LearningEvaluation()],
    isFinished: boolean = false)
  {
    this.id = id;
    this.learningUnit = learningUnit;
    this.content = content;
    this.competenceUnit = competenceUnit;
    this.topics = topics;
    this.learningStrategy = learningStrategy;
    this.learningEvaluation = learningEvaluation;
    this.isFinished = isFinished;
  }
}
