import {LearningUnit} from '../../learning-unit/classes/learning-unit';
import {Bibliography} from './Bibliography';
import {BibliographyType} from './BibliographyType';

export class BibliographyRelation {
  id: number;
  isClassic: boolean;
  learningUnit: LearningUnit;
  bibliography: Bibliography;
  bibliographyType: BibliographyType;
  number: number;
  constructor(number: number = 0, id: number = 0, isClassic: boolean = false, learningUnit: LearningUnit = null, b: Bibliography = null, bt: BibliographyType = null)
  {
    this.id = id;
    this.isClassic = isClassic;
    this.learningUnit = learningUnit;
    this.bibliography = b;
    this.bibliographyType = bt;
    this.number = number;
  }
}
