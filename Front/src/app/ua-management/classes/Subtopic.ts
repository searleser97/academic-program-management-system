export class Subtopic {
  id: number;
  number: number;
  name: string;
  constructor(id: number = 0, number: number = 0, name: string = '') {
    this.id = id;
    this.number = number;
    this.name = name;
  }
}
