import {Author} from './Author';
import {BibliographyType} from './BibliographyType';
import {Country} from './Country';

export class Bibliography {
  ISBN: string;
  authors: Author[];
  title: string;
  publicationYear: number;
  publicationPlace: Country;
  editorial: string;

  constructor(
    ISBN: string = null,
    authors: Author[] = null,
    title: string = null,
    publicationYear: number = null,
    publicationPlace: Country = null,
    editorial: string = null)
  {
    this.ISBN = ISBN;
    this.authors = authors;
    this.title = title;
    this.publicationPlace = publicationPlace;
    this.publicationYear = publicationYear;
    this.editorial = editorial;
  }
}
