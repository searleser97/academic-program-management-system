import {LearningUnit} from '../../learning-unit/classes/learning-unit';
import {EvaluationAccreditationUA} from './EvaluationAccreditationUA';
import {Content} from './Content';

export class SyntheticProgram {
  id: number;
  regard: string;
  didacticOrientation: string;
  learningUnit: LearningUnit;
  evaluationAccreditationUA: EvaluationAccreditationUA;
  content: Content[];
}
