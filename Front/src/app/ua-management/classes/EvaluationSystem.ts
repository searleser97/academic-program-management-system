import {LearningUnit} from '../../learning-unit/classes/learning-unit';
import {ThematicUnit} from './ThematicUnit';
import {Period} from './Period';

export class EvaluationSystem {
  id: number;
  period: number;
  thematicUnit: ThematicUnit;
  percentage: number;
}
