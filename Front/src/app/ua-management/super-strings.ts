import {DialogService} from '../shared/modules/material/services/dialog.service';

export const  confirms = {
  cancel: {
    title: 'Mensaje',
    body: '¿Está seguro que desea cancelar? Se perderán todos los avances sin guardar.',
    ok: 'Sí',
    nel: 'No'
  }
};
export const cancelWindowConfirm = function (dialog: DialogService) {
  dialog.simpleConfirmDialog(confirms.cancel.title, confirms.cancel.body, confirms.cancel.ok, confirms.cancel.nel)
    .afterClosed().subscribe((result) => {
    if(result === 'ok') {
      window.close();
    }
  });
};
export const cancelAndBackConfirm = function (dialog: DialogService) {
  dialog.simpleConfirmDialog(confirms.cancel.title, confirms.cancel.body, confirms.cancel.ok, confirms.cancel.nel)
    .afterClosed().subscribe((result) => {
    if(result === 'ok') {
      window.history.back();
    }
  });
};
