import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RegistrarProgramaSinteticoComponent} from './components/registrar-programa-sintetico/registrar-programa-sintetico.component';
import {RegistrarProgramaExtensoComponent} from './components/registrar-programa-extenso/registrar-programa-extenso.component';
import {RegistrarUnidadTematicaComponent} from './components/registrar-unidad-tematica/registrar-unidad-tematica.component';
import {RegistrarRelacionPracticasComponent} from './components/registrar-relacion-practicas/registrar-relacion-practicas.component';
import {RegistrarSistemaEvaluacionComponent} from './components/registrar-sistema-evaluacion/registrar-sistema-evaluacion.component';
import {BibliographyComponent} from './components/bibliography/bibliography.component';
import {MainUamComponent} from './components/main-uam/main-uam.component';
import {TeachingProfileComponent} from './components/teaching-profile/teaching-profile.component';

export const UaManagementRoutes: Routes = [
  {path: 'RPS', component: RegistrarProgramaSinteticoComponent},
  {path: 'RPS', component: RegistrarProgramaSinteticoComponent},
  {path: 'RPE', component: RegistrarProgramaExtensoComponent},
  {path: 'RUT', component: RegistrarUnidadTematicaComponent},
  {path: 'RRP', component: RegistrarRelacionPracticasComponent},
  {path: 'RSE', component: RegistrarSistemaEvaluacionComponent},
  {path: 'BPHY', component: BibliographyComponent},
  {path: 'RPD', component: TeachingProfileComponent},
  {path: 'MUAM/:idLearningUnit', component: MainUamComponent}
];

@NgModule({
  imports: [RouterModule.forChild(UaManagementRoutes)],
  exports: [RouterModule]
})
export class UaManagementRoutingModule { }
