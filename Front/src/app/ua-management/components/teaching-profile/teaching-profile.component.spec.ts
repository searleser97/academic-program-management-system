import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeachingProfileComponent } from './teaching-profile.component';

describe('TeachingProfileComponent', () => {
  let component: TeachingProfileComponent;
  let fixture: ComponentFixture<TeachingProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeachingProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeachingProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
