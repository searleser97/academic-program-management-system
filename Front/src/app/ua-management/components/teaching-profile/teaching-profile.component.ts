import {Component, OnInit} from '@angular/core';
import {labels, placeholders} from './teaching-profile.strings';
import {FormControl, FormGroup} from '@angular/forms';
import {SchoolingGrade} from '../../classes/SchoolingGrade';
import {Attitude} from '../../classes/Attitude';
import {Ability} from '../../classes/Ability';
import {ProfessionalExperience} from '../../classes/ProfessionalExperience';
import {Knowledge} from '../../classes/Knowledge';

@Component({
  selector: 'app-teaching-profile',
  templateUrl: './teaching-profile.component.html',
  styleUrls: ['./teaching-profile.component.scss']
})
export class TeachingProfileComponent implements OnInit {

  labels = labels;
  placeholders = placeholders;

  teachingProfileForm: FormGroup;

  searchSchoolingGradesCtrl: FormControl;
  filteredSchoolingGrades: SchoolingGrade[];

  searchKnowledgesCtrl: FormControl;
  filteredKnowledges: Knowledge[];

  searchProfessionalExperiencesCtrl: FormControl;
  filteredProfessionalExperiences: ProfessionalExperience[];

  searchAbilityCtrl: FormControl;
  filteredAbility: Ability[];

  searchAttitudeCtrl: FormControl;
  filteredAttitude: Attitude[];

  constructor() {
  }

  ngOnInit() {
  }



  submit() {
  }

  cancel() {
  }
}
