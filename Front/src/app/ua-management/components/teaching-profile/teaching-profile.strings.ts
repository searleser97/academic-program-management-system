export const labels = {
  formTitle: 'Registrar Perfil Docente',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Registrar',
  cancel: 'Cancelar',
  notFound: '-- No se encontraron opciones --',
  id: 'Numero de Empleado',
  password: 'Contraseña',
  snackBarMessage: 'Credenciales Incorrectas',
  snackBarAction: 'Omitir',
  name: 'Nombre',
  firstSurname: 'Primer Apellido',
  secondSurname: 'Segundo Apellido',
  title: 'Título',
  position: 'Cargo',
  workplace: 'Lugar de trabajo',
  role: 'Rol',
  errors: {
    name: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    firstSurname: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    secondSurname: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    title: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    positions: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    workplace: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    }
  }
};

export const placeholders = {
  search: 'Buscar...',
  id: '2014090123',
  password: '********',
  name: 'Juan',
  firstSurname: 'Garcia',
  secondSurname: 'Vazquez',
  title: 'Ingeniero',
  position: 'Docente',
  workplace: 'DES'
};
