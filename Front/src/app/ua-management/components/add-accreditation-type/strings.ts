export const labels = {
  pageTitle: 'Registrar Tipo de Acreditación',
  name: 'Nombre',
  button: {
    save: 'Aceptar',
    cancel: 'Cancelar'
  }
}
