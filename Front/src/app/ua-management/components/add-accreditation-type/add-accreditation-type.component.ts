import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RESTService} from '../../../shared/services/REST.service';
import {labels} from './strings';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';
import {Router} from '@angular/router';
import {cancelAndBackConfirm} from '../../super-strings';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-add-accreditation-type',
  templateUrl: './add-accreditation-type.component.html',
  styleUrls: ['./add-accreditation-type.component.scss']
})
export class AddAccreditationTypeComponent implements OnInit {
  labels = labels
  private regex = '^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ .,;:"\\d-]+$';
  formGroup: FormGroup;
  constructor(private dialog: DialogService, private r2: PreviousRouteService, private r: Router, private fb: FormBuilder, private rest: RESTService) {
    this.formGroup = this.fb.group({
      id: [0],
      name: ['', Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])]
    });
  }
  createAccrType() {
    this.rest.submit('post', 'accreditationtype', this.formGroup.value, 'REA');
  }
  closeWindow() {
    cancelAndBackConfirm(this.dialog);
  }
  ngOnInit() {
  }

}
