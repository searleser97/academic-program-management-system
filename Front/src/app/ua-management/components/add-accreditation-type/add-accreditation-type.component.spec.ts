import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAccreditationTypeComponent } from './add-accreditation-type.component';

describe('AddAccreditationTypeComponent', () => {
  let component: AddAccreditationTypeComponent;
  let fixture: ComponentFixture<AddAccreditationTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAccreditationTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAccreditationTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
