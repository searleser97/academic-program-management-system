import { Component, OnInit } from '@angular/core';
import {labels} from './strings';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {EvaluationUA} from '../../classes/EvaluationUA';
import {EvaluationAccreditationUA} from '../../classes/EvaluationAccreditationUA';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {Local} from 'protractor/built/driverProviders';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {of} from 'rxjs';
import {RESTService} from '../../../shared/services/REST.service';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {cancelWindowConfirm} from '../../super-strings';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-eval-learnings',
  templateUrl: './registrar-eval-learnings.component.html',
  styleUrls: ['./registrar-eval-learnings.component.scss']
})
export class RegistrarEvalLearningsComponent implements OnInit {
  labels = labels;
  learningUnitID: number;
  formGroup: FormGroup;
  objEvalType: SelectOptionModel[] = [];
  sEvalType: SelectDataModel = new SelectDataModel(this.labels.evaluationType, of(this.objEvalType));
  private regex = "^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ .,;:\"\\d-\\/]+$";
  constructor(private ar: ActivatedRoute, private fb: FormBuilder, private dialog: DialogService, private rest: RESTService) {
    this.learningUnitID = ar.snapshot.params.id;
    this.formGroup = fb.group({
      evals: this.fb.array(
      [this. fb.group({
        id: [0],
        name: ['', Validators.required],
        percentage: [100, Validators.required],
        evaluationType: ['', Validators.required]
      })]
    )});
    const toBeLoaded = <EvaluationUA[]>LocalStorage.getObject(this.learningUnitID + ':evaluations');
    if(toBeLoaded)
    {
      for(let i = 1; i < toBeLoaded.length; i++ ) {
        this.addEval();
      }
      this.formGroup.get('evals').patchValue(toBeLoaded);
    }
    this.rest.get('learningUnit/1', (data: LearningUnit, msg) => {
      if(data) {
        this.objEvalType.push(new SelectOptionModel(this.labels.CONTINUA, this.labels.CONTINUA));
        this.objEvalType.push(new SelectOptionModel(this.labels.ESCRITA, this.labels.ESCRITA));
      }
    })
  }

  get evals() {
    return this.formGroup.get('evals') as FormArray;
  }
  ngOnInit() {
  }
  closeWindow() {
    cancelWindowConfirm(this.dialog);
  }
  addEval(p: number = 100, n: string = '', i: number = 0, e: string = '') {
    this.evals.push(this.fb.group({
      id: [i, Validators.required],
      percentage: [p, Validators.required],
      name: [n, Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])],
      evaluationType: []
    }));
  }
  popEval() {
    if (this.evals.length > 1) {
      this.evals.removeAt(this.evals.length - 1);
    }
  }
  onSubmit(){
    console.log(this.formGroup.value)
    if(!this.formGroup.valid) {
      this.dialog.errorDialog(this.labels.camposEmpty.title, this.labels.camposEmpty.body, this.labels.camposEmpty.ok)
      return;
    }
    this.saveEvalFromLocalStorage(this.formGroup.get('evals').value);
    let sum = 0;
    for (let i = 0;  i < this.evals.length; i++) {
      sum += (<EvaluationUA>this.evals.at(i).value).percentage;
    }
    if (sum != 100) {
      this.dialog.messageDialog(
        this.labels.avisoCienNotCompleto.title,
        this.labels.avisoCienNotCompleto.message,
        this.labels.avisoCienNotCompleto.ok);
    } else {
      this.dialog.messageDialog(labels.avisoEval.title, this.labels.avisoEval.message, this.labels.avisoEval.ok, null, () => {
        window.close();
      });
    }
  }
  saveEvalFromLocalStorage(c: EvaluationUA[]): void {
    LocalStorage.saveObject(this.learningUnitID + ':evaluations', c);
  }
}
