import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarEvalLearningsComponent } from './registrar-eval-learnings.component';

describe('RegistrarEvalLearningsComponent', () => {
  let component: RegistrarEvalLearningsComponent;
  let fixture: ComponentFixture<RegistrarEvalLearningsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarEvalLearningsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarEvalLearningsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
