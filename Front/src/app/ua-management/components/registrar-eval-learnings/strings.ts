export const labels = {
  pageTitle: 'Registrar Evaluaciones de Aprendizaje',
  percentage: 'Porcentaje: ',
  evalName: 'Nombre de evaluación ',
  button: {
    save: 'Guardar',
    close: 'Cancelar',
    dropEval: 'Eliminar última evaluación',
    addEval: 'Agregar evaluación'
  },
  avisoEval: {
    title: 'Aviso',
    message: 'Registro finalizado exitosamente',
    ok: 'Aceptar'
  },
  avisoCienNotCompleto: {
    title: 'Aviso',
    message: 'Los porcentajes de evaluación no cumplen con el porcentaje total obligatorio',
    ok: 'Aceptar'
  },
  camposEmpty: {
    title: 'Error',
    body: 'Todos los campos maracados con (*) son obligatorios.',
    ok: 'Aceptar'
  },
  evaluationType: 'Tipo de evaluación',
  CONTINUA: 'Continua',
  ESCRITA: 'Escrita'
}
