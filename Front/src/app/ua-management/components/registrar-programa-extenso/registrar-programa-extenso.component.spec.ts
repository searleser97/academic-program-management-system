import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarProgramaExtensoComponent } from './registrar-programa-extenso.component';

describe('RegistrarProgramaExtensoComponent', () => {
  let component: RegistrarProgramaExtensoComponent;
  let fixture: ComponentFixture<RegistrarProgramaExtensoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarProgramaExtensoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarProgramaExtensoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
