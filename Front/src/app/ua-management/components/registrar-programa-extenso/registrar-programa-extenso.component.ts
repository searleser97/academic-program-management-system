import { Component, OnInit, Input } from '@angular/core';
import { labels } from './strings';

import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { FormService } from '../../../shared/services/form.service';
import { RESTService } from '../../../shared/services/REST.service';
import { SelectOptionModel } from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import { SelectDataModel } from '../../../utils/components/select-form-control/classes/SelectDataModel';
import { of } from 'rxjs';

import { ALFA_REGEX, FLOAT_REGEX } from '../../../../environments/environment';
import { DialogService} from '../../../shared/modules/material/services/dialog.service';
import { LocalStorage} from '../../../shared/classes/LocalStorage';

import { LearningUnit } from 'src/app/learning-unit/classes/learning-unit';
import { ExtensiveProgram } from '../../classes/ExtensiveProgram';
import { Modality } from '../../classes/Modality';
import { Teaching } from '../../classes/Teaching';
import { Type } from '../../classes/Type';
import { AssignedTime } from '../../classes/AssignedTime';
import { TeachingProfile } from '../../classes/TeachingProfile';

@Component({
  selector: 'app-registrar-programa-extenso',
  templateUrl: './registrar-programa-extenso.component.html',
  styleUrls: ['./registrar-programa-extenso.component.scss']
})
export class RegistrarProgramaExtensoComponent implements OnInit {
  labels = labels;
  //
  //@Input() public learningUnit: LearningUnit;

  //Variables
  learningUnitForm: FormGroup;
  learningUnit: LearningUnit;
  extensiveProgram : ExtensiveProgram;
  credits : String;
  isFinished = false;
  learningUnitID : number;

  //Validate variables
  date : Date;
  year : number;
  theoryHoursPerWeek : number;
  theoryHoursPerSemester : number;
  practiceHoursPerWeek : number;
  practiceHoursPerSemester : number;
  automaticTeaching : number;
  totalHoursPerSemester : number;

  //Cataloge
  modalities : Modality[];
  types : Type[];
  teachings : Teaching[];

  //error control 
  failures = 0;
  //failUrl = 'unidadesaprendizaje/consultar';

  fb: FormBuilder;
  formGroup;

  private auOptions: SelectOptionModel[] = [];
  private apOptions: SelectOptionModel[] = [];
  private luOptions: SelectOptionModel[] = [];
  private formationAreaOption: SelectOptionModel[] = [];
  private modalityOptions: SelectOptionModel[] = [];
  private typeOptions: SelectOptionModel[] = [];
  private teachingOptions: SelectOptionModel[] = [];
  private sOptions: SelectOptionModel[] = [];

  auSelect: SelectDataModel;
  apSelect: SelectDataModel;
  luSelect: SelectDataModel;
  formationAreaSelect: SelectDataModel;
  modalitySelect: SelectDataModel;
  typeSelect: SelectDataModel;
  teachingSelect : SelectDataModel;
  sSelect: SelectDataModel;

  constructor( fb: FormBuilder, private formService: FormService, private dialog: DialogService, private rest: RESTService) {
    this.date = new Date();
    this.year = this.date.getFullYear()+1;
    this.theoryHoursPerWeek = 0;
    this.practiceHoursPerWeek = 0;
    this.automaticTeaching =  0;
    this.totalHoursPerSemester = 0;
    this.automaticTeaching = 0;
    this.totalHoursPerSemester = 0;
    this.learningUnitID = 0;
    this.fb = fb;
    this.auSelect   = new SelectDataModel(this.labels.academicUnit, of(this.auOptions));
    this.apSelect   = new SelectDataModel(this.labels.academicProgram, of(this.apOptions));
    this.luSelect   = new SelectDataModel(this.labels.learningUnit, of(this.luOptions));
    this.formationAreaSelect   = new SelectDataModel(this.labels.formationArea, of(this.formationAreaOption));
    this.modalitySelect   = new SelectDataModel(this.labels.modality, of(this.modalityOptions));
    this.typeSelect = new SelectDataModel(this.labels.learningUnitType, of(this.typeOptions));
    this.teachingSelect = new SelectDataModel(this.labels.teaching, of(this.teachingOptions));
    this.sSelect    = new SelectDataModel(this.labels.semester, of(this.sOptions));
    this.credits = '';

    this.formGroup = fb.group({
      id: [0],
      educationalIntention: ['', Validators.compose([
        Validators.pattern(ALFA_REGEX),
        Validators.required
      ])],
      validity: [ this.year , Validators.compose([
        Validators.min(this.year),
        Validators.max(this.year+5),
        Validators.required
      ])],
      types:[ [] , Validators.compose([ Validators.required]) ],
      learningUnit : [ new LearningUnit() ],
      assignedTime: [ new AssignedTime() ],
      teachingProfile : this.fb.group({
        schoolingGrades: [ '', Validators.compose([
          Validators.pattern(ALFA_REGEX),
          Validators.required
        ])],
        knowledges: [ '', Validators.compose([
          Validators.pattern(ALFA_REGEX),
          Validators.required
        ])],
        professionalExperiences: [ '', Validators.compose([
          Validators.pattern(ALFA_REGEX),
          Validators.required
        ])],
        ability: [ '', Validators.compose([
          Validators.pattern(ALFA_REGEX),
          Validators.required
        ])],
        attitude: [ '', Validators.compose([
          Validators.pattern(ALFA_REGEX),
          Validators.required
        ])]
      }),
      modality:[ {}, Validators.required],
      teaching: [ {}, Validators.required]
    });    

    this.learningUnit = new LearningUnit();
    this.extensiveProgram = new ExtensiveProgram();
  }

  ngOnInit() {
    //this.learningUnitForm.get('semester').disable();
    this.asyncRequest();
  }

  onSave(message : boolean = true) {
    LocalStorage.saveObject('extensiveProgramAux_'+this.learningUnitID, this.formGroup.value);
    if(message)
      this.dialog.messageDialog('Mensaje', 'Avances guardados exitosamente.', 'Aceptar');
  }

  onSubmit() {
    console.log(this.formGroup.getRawValue());
    this.onSave(false);
    if(this.formGroup.valid) {
      const obj = JSON.parse(JSON.stringify(this.formGroup.value));
      this.rest.huitzoRequestPOST( 'extensiveProgram', obj, null, () => {
      this.disableTodo(true);
      });
    } else {
      this.dialog.errorDialog('Error', 'Los campos marcados con (*) son obligatorios.', 'Aceptar');
    }
  }
  
  disableTodo(flag: boolean) {
    if (flag == true) {
      console.log('is Finished');
      this.formGroup.disable();
      this.modalitySelect.disabled = true;
      this.typeSelect.disabled = true;
      this.teachingSelect.disabled = true;
      this.isFinished = true;
    } else {
      this.formGroup.enable();
      this.isFinished = false;
    }
  }

  prevSession(){
    var extendProgram = LocalStorage.getObject('extensiveProgramAux_'+this.learningUnitID);
    if(extendProgram){
      this.formGroup.setValue(extendProgram);
      /*//this.setSelectValue(this.typeOptions,this.typeSelect, extendProgram.types,'', false);
      this.setSelectValue(this.modalityOptions,this.modalitySelect,
         extendProgram.modality, extendProgram.modality.name, false);
      this.setSelectValue(this.teachingOptions,this.teachingSelect,
          extendProgram.teaching,extendProgram.teaching.name, false);*/
    }
  }

  async initLearningUnitForm(learningUnit: LearningUnit) {
    //this.learningUnitForm.setValue(learningUnit);
    this.formGroup.get('learningUnit').setValue(learningUnit);
    this.learningUnit = learningUnit;
    this.learningUnitID = learningUnit.id;
    this.credits = "TEPIC "+ learningUnit.tepiccredits +"\t-\tSATCA "+ learningUnit.satcacredits;
    this.theoryHoursPerWeek = learningUnit.theoryHoursPerWeek;
    this.theoryHoursPerSemester = this.theoryHoursPerWeek * 18;
    this.practiceHoursPerWeek = learningUnit.practiceHoursPerWeek;
    this.practiceHoursPerSemester = this.practiceHoursPerWeek * 18;
    this.totalHoursPerSemester = this.theoryHoursPerSemester + this.practiceHoursPerSemester;
    this.automaticTeaching = ( this.learningUnit.satcacredits - (this.totalHoursPerSemester/16) ) * 20;
    this.setElementsValues();
    this.prevSession();
    return true;
  }

  //Async request
  async asyncRequest(){
    if( await this.getModalities() && await this.getTypes() && await this.getTeachings() ){
      this.setCatalogue();
      return true;
    }
    return false;
  }

  setCatalogue(){
    for(let modality of this.modalities){
      this.modalityOptions.push( new SelectOptionModel( modality, modality.name ) );
    }
    for(let type of this.types ){
      this.typeOptions.push( new SelectOptionModel( type, type.name ) );
    }
    for(let teaching of this.teachings){
      this.teachingOptions.push( new SelectOptionModel( teaching, teaching.name ) );
    }
  }

  async getModalities() {
    const modalities = await this.rest.getAsync<Modality[]>('modality');
    if( modalities ){
      this.modalities = modalities;
      return true;
    }
    return false;
  }

  async getTypes() {
    const types = await this.rest.getAsync<Modality[]>('type');
    if( types ){
      this.types = types;
      return true;
    }
    return false;
  }

  async getTeachings() {
    const teachings = await this.rest.getAsync<Modality[]>('teaching');
    if( teachings ){
      this.teachings = teachings;
      return true;
    }
    return false;
  }

  //Getters and Setters
  setElementsValues(){
    this.setSelectValue(
      this.auOptions,this.auSelect,this.learningUnit.semester.studyPlan.academicProgram.workplace,
      this.learningUnit.semester.studyPlan.academicProgram.workplace.name,true);
    this.setSelectValue(this.apOptions,this.apSelect,this.learningUnit.semester.studyPlan.academicProgram,
      this.learningUnit.semester.studyPlan.academicProgram.name,true);
    this.setSelectValue(this.luOptions,this.luSelect,this.learningUnit,this.learningUnit.name,true);
    this.setSelectValue(this.sOptions,this.sSelect,this.learningUnit.semester,
      this.learningUnit.semester.semesterNumber,true);
    this.setSelectValue(this.formationAreaOption,this.formationAreaSelect,this.learningUnit.formationArea,
      this.learningUnit.formationArea.name,true);
    
  }

  setSelectValue( selectOption: SelectOptionModel[], selectDataModel: SelectDataModel,
        selectValueModel: any = null, selectViewVal: (string | number) = null, disable: boolean = false){
    selectOption.push(new SelectOptionModel(selectValueModel, selectViewVal));
    selectDataModel.valueSelected = selectValueModel;
    selectDataModel.disabled = disable;
  }
  
  setTEPICCredits() {
    this.learningUnitForm.get('tepiccredits')
      .setValue(2 * parseFloat(this.learningUnitForm.get('theoryHoursPerWeek').value) +
        parseFloat(this.learningUnitForm.get('practiceHoursPerWeek').value));
  }

}