export const labels = {
  pageTitle: ['Registrar programa en extenso.', 'Registrar Perfil Docente'],
  pageDescription: 'Alguna descripción',
  academicUnit: 'Unidad Académica',
  academicProgram: 'Programa Académico',
  learningUnit: 'Unidad de Aprendizaje',
  formationArea: 'Área de formación',
  modality: 'Modalidad',
  learningUnitType: 'Tipo de unidad de aprendizaje',
  teaching: 'Enseñanza',
  validity: 'Vigencia',
  semester: 'Semestre',
  credits: 'Creditos',
  educIntention: 'Intención educativa',
  propositoLeaningUnit: 'Propósito de la Unidad de Aprendizaje',
  didacticOrient: 'Orientación didáctica',
  timesAsig: 'Tiempos asignados',
  TheoryTimePerWeek: 'Horas teoría/semana',
  PracticeTimePerWeek: 'Horas práctica/semana',
  TheoryTimePerSemester: 'Horas teoría/semestre',
  PracticeTimePerSemester: 'Horas práctica/semestre',
  AutoTimePerSemester: 'Horas de aprendizaje autónomo',
  TotalTimePerSemester: 'Horas totales/semestre',
  //Aiuda :v
  schoolingGrades: "Grado de Estudio",
  knowledges: "Conocimientos",
  professionalExperiences: "Experiencia Profesional",
  ability: "Habilidad",
  attitude: "Actitud",

  button: {
    save: 'Guardar',
    finish: 'Finalizar'
  }
}
