import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarUnidadTematicaComponent } from './registrar-unidad-tematica.component';

describe('RegistrarUnidadTematicaComponent', () => {
  let component: RegistrarUnidadTematicaComponent;
  let fixture: ComponentFixture<RegistrarUnidadTematicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarUnidadTematicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarUnidadTematicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
