export const labels = {
  pageTitle: 'Registrar Unidad Temática.',
  pageDescription: 'Alguna descripción',
  learningUnit: 'Unidad de Aprendizaje',
  thematicUnitNumber: 'No. Unidad Temática',
  thematicUnitName: 'Nombre',
  competenceUnit: 'Unidad de Competencia',
  topics: 'Temas (*) ',
  learningStrategies: 'Estrategias de Aprendizaje',
  learningEvaluation: 'Evaluación de los aprendizajes (*) ',
  button: {
    save: 'Guardar',
    finish: 'Finalizar',
    next: 'Siguiente',
    prev: 'Anterior'
  },
  finished: {
    body: 'Se ha finalizado exitosamente la unidad tematica.',
    title: 'Aviso',
    ok: 'Aceptar'
  },
  redirects: {
    topics: 'RTUA',
    evals: 'REDA'
  },
  popupFeature: 'scrollbar=0, width=720, height=960',
  incompleteMsg: {
    body: 'Todos los campos con (*) son obligatorios.',
    title: 'Error',
    ok: 'Aceptar'
  },
  errorNoPSFound: {
    title: 'Error',
    body: 'Debe llenar el Programa Sintético para realizar este registro.',
    ok: 'Aceptar'
  }
}
