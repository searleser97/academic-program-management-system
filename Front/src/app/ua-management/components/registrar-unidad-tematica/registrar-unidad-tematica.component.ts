import {Component, Input, OnInit} from '@angular/core';
import {labels} from './strings';
import {FormBuilder, Validators} from '@angular/forms';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {of} from 'rxjs';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {RESTService} from '../../../shared/services/REST.service';
import {Content} from '../../classes/Content';
import {Topic} from '../../classes/Topic';
import {LearningEvaluation} from '../../classes/LearningEvaluation';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {ThematicUnit} from '../../classes/ThematicUnit';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {URL_PREFIX} from '../../../../environments/environment';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-registrar-unidad-tematica',
  templateUrl: './registrar-unidad-tematica.component.html',
  styleUrls: ['./registrar-unidad-tematica.component.scss']
})
export class RegistrarUnidadTematicaComponent implements OnInit {
  @Input() learningUnitID: number;
  labels = labels;
  btnFin = false;
  isFinished = false;
  btnGuardar = false;
  private luOptions: SelectOptionModel[] = [];
  luSelect: SelectDataModel = new SelectDataModel(this.labels.learningUnit, of(this.luOptions));
  formGroup;
  learnProv: LearningUnit;
  contentArray: Content[];
  currentContent: Content;
  numberOfUT = 1;
  actualContentIndex = 0;
  thematicUnitSet: ThematicUnit[];
  constructor(private ac: ActivatedRoute, private fb: FormBuilder, private rest: RESTService, private  dialog: DialogService) {
    this.formGroup = this.fb.group({
      id: [0],
      isFinished: [false],
      learningUnit: ['', Validators.required],
      content: this.fb.group({
        id: [0, Validators.required],
        name: ['', Validators.required],
        number: [0, Validators.required]
      }),
      competenceUnit: ['', Validators.required],
      topics: ['', Validators.required],
      learningStrategy: ['', Validators.required],
      learningEvaluation: ['', Validators.required]
    });

    this.loadUTData();
  }

  ngOnInit() {

  }
  buildModel() {
    console.log(this.learningUnitID + ':evaluations')
    this.formGroup.patchValue({learningEvaluation: LocalStorage.getObject(this.learningUnitID + ':evaluations')});
    this.formGroup.patchValue({topics: LocalStorage.getObject(this.learningUnitID + ':topics')});
    let saved = <ThematicUnit[]>LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray');
    if(!saved) {
      saved = new Array(this.numberOfUT);
    }
    const toSave = <ThematicUnit>this.formGroup.value;
    toSave.learningUnit = this.learnProv;
    toSave.content = this.currentContent;
    saved[this.actualContentIndex] = toSave;
    LocalStorage.saveObject(this.learningUnitID + ':ThematicUnitArray', saved );
    console.log(LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray'));
  }
  traspileModel(th: ThematicUnit) {
    if (th) {
      if (th.topics && th.topics.length != 0) {
        LocalStorage.saveObject(this.learningUnitID + ':topics', th.topics);
        for (let i = 0; i < th.topics.length; i ++) {
          LocalStorage.saveObject(this.learningUnitID + ':sub' + (i + 1), th.topics[i].subtopics );
        }
      }
      if (th.learningEvaluation) {
        LocalStorage.saveObject(this.learningUnitID + ':evaluations', th.learningEvaluation);
      }
    }
  }

  onSubmit() {
    this.saveThis();
    this.patch2DB();
  }
  saveThis() {
    this.buildModel();
  }
  goTopics() {
    const myWindow = window.open(URL_PREFIX + this.labels.redirects.topics + '/' + this.learningUnitID, 'blank', this.labels.popupFeature);
    myWindow.focus();
  }
  goLearningEval() {
    const myWindow = window.open(URL_PREFIX + this.labels.redirects.evals + '/' + this.learningUnitID, 'blank', this.labels.popupFeature);
    myWindow.focus();
  }
  /*setProvisionalLearningUnit() {
    if(!this.learningUnitID)
    {
      this.learningUnitID = 1;
    }
    this.rest.get('learningUnit/' + this.learningUnitID.toString(), (lu: LearningUnit, errorMsg) => {
      if (lu) {
        this.learnProv = lu;
        this.formGroup.patchValue({learningUnit: lu});
        console.log(this.formGroup.value)
        this.rest.get('content/contentbylearningunit/' + this.learnProv.id, (c: Content[]) => {
          if (c) {
            console.log(c);
            this.numberOfUT = c.length;
            this.actualContentIndex = 0;
            this.contentArray = c;
            this.currentContent = c[this.actualContentIndex];
            this.loadFromDB();
            if (!LocalStorage.getObject('ThematicUnitArray')) {
              LocalStorage.saveObject('ThematicUnitArray', new Array(this.numberOfUT));
            }

            this.initFarolearSelects();
          }
        });
      }
    });
  }*/
  initProvisionalLearningUnit( lu: LearningUnit ) {
      if (lu) {
        this.learningUnitID = lu.id;
        this.learnProv = lu;
        this.formGroup.patchValue({learningUnit: lu});
        console.log(this.formGroup.value)
        this.rest.get('content/contentbylearningunit/' + this.learnProv.id, (c: Content[]) => {
          if (c) {
            console.log(c);
            this.numberOfUT = c.length;
            this.actualContentIndex = 0;
            this.contentArray = c;
            this.currentContent = c[this.actualContentIndex];
            this.loadFromDB();
            if (!LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray')) {
              LocalStorage.saveObject(this.learningUnitID + ':ThematicUnitArray', new Array(this.numberOfUT));
            }
            this.initFarolearSelects();
          } else {
            this.dialog.errorDialog(this.labels.errorNoPSFound.title,
              this.labels.errorNoPSFound.body,
              this.labels.errorNoPSFound.ok);
          }
        });
      }
  }
  nextContet() {
    if (this.actualContentIndex + 1 !== this.numberOfUT) {
      this.actualContentIndex += 1;
      this.currentContent = this.contentArray[this.actualContentIndex];
      this.formGroup.get('content').patchValue(this.currentContent);
      this.formGroup.get('content').disable();
      this.flushData();
      this.loadUTData();
    }

  }
  disableTodo(flag: boolean) {
    if (flag == true) {
      console.log('is Finished');
      this.formGroup.disable();
      this.isFinished = true;
    } else {
      this.formGroup.enable();
      this.isFinished = false;
      this.formGroup.get('learningUnit').disable();
      this.formGroup.get('content').disable();
    }
  }
  loadUTData() {
    let iWantToLoadThis;
    if(LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray')) {
     iWantToLoadThis = (<ThematicUnit[]>LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray'))[this.actualContentIndex];
    } else {
      iWantToLoadThis = null;
    }
    if (iWantToLoadThis) {
      this.formGroup.patchValue(iWantToLoadThis);
      console.log(iWantToLoadThis);
      this.formGroup.patchValue({isFinished: iWantToLoadThis.isFinished});
      this.disableTodo(iWantToLoadThis.isFinished);
      this.traspileModel(iWantToLoadThis);
    } else {
      this.disableTodo(false);
      this.formGroup.patchValue({
        topics: null,
        competenceUnit: null,
        learningStrategy: null,
        learningEvaluation: null});

    }

  }
  previousContent() {
    if (this.actualContentIndex > 0) {
      this.actualContentIndex -= 1;
      this.currentContent = this.contentArray[this.actualContentIndex];
      this.formGroup.get('content').patchValue(this.currentContent);
      this.formGroup.get('content').disable();
      this.flushData();
      this.loadUTData();
    }
  }
  flushData() {
    Object.keys(localStorage)
      .forEach(function(key) {
        if (/\bsub\d*/.test(key)) {
          localStorage.removeItem(key);
        }
      });
    LocalStorage.removeObject(this.learningUnitID + ':evaluations');
    LocalStorage.removeObject(this.learningUnitID + ':topics');
  }
  private initFarolearSelects() {
    this.luOptions.push(new SelectOptionModel(this.learnProv,
      this.learnProv.name));
    this.luSelect.valueSelected = this.learnProv;
    this.luSelect.disabled = true;
    this.formGroup.get('content').patchValue(this.currentContent);
    this.formGroup.get('content').disable();
    const thUnits = LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray');
    if (thUnits) {
      if (thUnits[this.actualContentIndex]) {
        this.formGroup.patchValue(thUnits[this.actualContentIndex]);
      }
    }
  }
  onFinished() {
    console.log(this.formGroup.value);
    this.saveThis();
    if (this.formGroup.valid) {
      this.formGroup.patchValue({isFinished: true});
      this.onSubmit();
      this.disableTodo(true);

    } else {
      this.dialog.errorDialog(
        this.labels.incompleteMsg.title,
        this.labels.incompleteMsg.body,
        this.labels.incompleteMsg.ok
      );
      return;
    }

  }
  actualizaUnidadTematica() {

  }
  loadFromDB() {
    this.rest.get('thematicUnit/thematicUnitByLearningUnitId/' + this.learnProv.id, (ths: ThematicUnit[]) => {
      if(ths) {
        const buff = <ThematicUnit[]>LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray');
        for(let t of ths) {
          buff[t.content.number-1] = t;
        }
        LocalStorage.saveObject(this.learningUnitID + ':ThematicUnitArray', buff);
        this.loadUTData();
      }
    });
  }
  patch2DB() {
    this.checkIfExist();
  }
  checkIfExist(): void {
    let flag = false;
    this.rest.get('thematicUnit/thematicUnitByLearningUnitId/' + this.learnProv.id, (uts: ThematicUnit[]) => {
      if(uts){
        let buff = <ThematicUnit[]>LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray');
        let buff1 = buff[this.actualContentIndex];
        for(let ut of uts) {
          if(ut.content.number == buff1.content.number) {
            buff1.id = ut.id;
            flag = true;
            break;
          }
        }
        buff[this.actualContentIndex] = buff1;
        LocalStorage.saveObject(this.learningUnitID + ':ThematicUnitArray', buff);
      }
      const toUpload = <ThematicUnit>LocalStorage.getObject(this.learningUnitID + ':ThematicUnitArray')[this.actualContentIndex];
      console.log(toUpload);
      if(!flag) {
        console.log('post')
        this.rest.submit('post', 'thematicUnit', toUpload);
      } else {
        console.log('patch')
        this.rest.submit('patch', 'thematicUnit', toUpload);
      }
    });

  }
}
