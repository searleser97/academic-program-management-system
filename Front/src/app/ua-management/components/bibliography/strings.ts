export const labels = {
  pageTitle: 'Registrar Bibliografía',
  author: 'Autor',
  pubYear: 'Año de publicación',
  pubPlace: 'Lugar de publicación',
  title: 'Título del libro',
  editorial: 'Editorial',
  isbn: 'ISBN',
  bibliographyType: 'Tipo',
  isClassic: 'Es clásica: ',
  thematicUnit: 'Unidad Temática',
  button: {
    save: 'Guardar',
    cancel: 'Cancelar',
    addAuthor: 'Agregar autor',
    ok: 'Ok'
  },
  errors: {
    catalogs: 'Por el momento no se puede realizar el registro'
  },
  alerts: {
    title: 'Aviso',
    outOfBounds: 'Solo se permite seleccionar a lo más 3 autores'
  },
  redirects: {
    mainUaManagement: 'MUAM',
    author: 'ATHR'
  },
  popupFeature: 'scrollbar=0, width=720, height=411'
};
