import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {labels} from './strings';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {of} from 'rxjs';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Author} from '../../classes/Author';
import {BibliographyType} from '../../classes/BibliographyType';
import {RESTService} from '../../../shared/services/REST.service';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {Country} from '../../classes/Country';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {BibliographyRelation} from '../../classes/BibliographyRelation';
import {Bibliography} from '../../classes/Bibliography';
import {ActivatedRoute, Router} from '@angular/router';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';
import {URL_PREFIX} from '../../../../environments/environment';
import {ThematicUnit} from '../../classes/ThematicUnit';


@Component({
  selector: 'app-bibliography',
  templateUrl: './bibliography.component.html',
  styleUrls: ['./bibliography.component.scss']
})
export class BibliographyComponent implements  OnInit {
  labels = labels;
  authOpt: SelectOptionModel[];
  plcOpt: SelectOptionModel[];
  tpeOpt: SelectOptionModel[];
  authSelect: SelectDataModel;
  plcSelect: SelectDataModel;
  tpeSelect: SelectDataModel;
  thOpt: SelectOptionModel[];
  thSelect: SelectDataModel;

  fb: FormBuilder;
  superFormGroup: FormGroup;
  private regexISBN = '^[\\d{10,13}]|[(\\d+)-]+[\\d+]$';
  private regex = '^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ .,;:"\\d-]+$';
  private learningUnitID: number;
  localThematics: ThematicUnit[] = [];
  constructor(private ac: ActivatedRoute, private r: Router, fb: FormBuilder, private rest: RESTService, private dialog: DialogService) {
    this.fb = fb;
    // Bibliography
    // BibliographyRelation
    this.createFormDataModel();
    this.initSelects();
    this.setCurrentBibliographyNumber();
  }
  createFormDataModel() {
    this.superFormGroup = this.fb.group({
      id: [0],
      isClassic: ['', Validators.required],
      number: [''],
      learningUnit: ['', Validators.required],
      bibliographyType: ['', Validators.required],
      bibliography: this.fb.group({
        isbn: ['', Validators.compose([
          Validators.pattern(this.regexISBN),
          Validators.required
        ])],
        authors: ['', Validators.required],
        title: ['', Validators.compose([
          Validators.pattern(this.regex),
          Validators.required
        ])],
        publicationYear: ['', Validators.compose([
          Validators.required,
          Validators.min(1454),
          Validators.max(2018)
        ])],
        thematicUnits: this.fb.array([]),
        publicationPlace: ['', Validators.required],
        editorial: ['', Validators.compose([
          Validators.pattern(this.regex),
          Validators.required
        ])]})
    });
  }
  initSelects() {
    this.tpeOpt = [];
    this.plcOpt  = [];
    this.authOpt = [];
    this.thOpt = [];
    this.thSelect = new SelectDataModel(this.labels.thematicUnit, of(this.thOpt));
    this.authSelect = new SelectDataModel(this.labels.author, of(this.authOpt));
    this.plcSelect = new SelectDataModel(this.labels.pubPlace, of(this.plcOpt));
    this.tpeSelect  = new SelectDataModel(this.labels.bibliographyType, of(this.tpeOpt));

  }
  async dameUTs() {
    return this.rest.getAsync<ThematicUnit[]>('thematicUnit/thematicUnitByLearningUnitId/' + this.learningUnitID);
  }
  async cargaCatUTs() {
    const uts = await  this.dameUTs();
    if (uts) {
      for (const ut of uts) {
        this.thOpt.push(new SelectOptionModel(ut, ut.content.name));
      }
    }
  }
  setCurrentBibliographyNumber() {
    let newNumber = 0;
    this.rest.get('bibliographyRelation', (bts: BibliographyRelation[], errorMsg) => {
      if (bts) {
        for (const b of bts) {
          if (b.learningUnit === this.superFormGroup.get('learningUnit').value) {
            if (newNumber < b.number) {
              newNumber = b.number;
            }
          }
        }
      }
      this.superFormGroup.patchValue({number: newNumber});
    });

  }
  /*setProvisionalLearningUnit() {
    if(!this.learningUnitID)
    {
      this.learningUnitID = 1;
    }
    this.rest.get('learningUnit/' + this.learningUnitID, (lu: LearningUnit, errorMsg) => {
      if (lu) {
        this.superFormGroup.patchValue({learningUnit: lu});
      }
    });
}*/
  initProvisionalLearningUnit(lu: LearningUnit) {
    if (lu) {
      this.superFormGroup.patchValue({learningUnit: lu});
    }
    this.learningUnitID = lu.id;
    this.cargaCatUTs();
  }


  onCancel() { this.r.navigate(['/' + this.labels.redirects.mainUaManagement]); }
  addAuthor() {
    const myWindow = window.open(URL_PREFIX + this.labels.redirects.author, 'blank', this.labels.popupFeature);
    myWindow.focus();
  }
  onSubmit() {
    let flag = false;
    const toLoad = this.superFormGroup.value;
    toLoad.bibliography.thematicUnits = this.localThematics;
    this.superFormGroup.get('bibliography').get('thematicUnits').patchValue(this.localThematics);
    this.rest.get('bibliographyRelation', (data: BibliographyRelation[]) => {
      if(data) {
        for (const b of data) {
          // @ts-ignore
          if (this.superFormGroup.get('bibliography').get('isbn').value == b.bibliography.isbn) {
            this.dialog.messageDialog('Error', 'Ya existe un libro con ISBN', 'Aceptar');
            flag = true;
            break;
          }
        }
      }
      if (this.superFormGroup.get('bibliography').get('authors').value.length > 3) {
        this.dialog.messageDialog(this.labels.alerts.title, this.labels.alerts.outOfBounds, this.labels.button.ok);
      } else {
        if (!this.superFormGroup.valid) {
          console.log(this.superFormGroup.value);
          this.dialog.errorDialog('Error', 'Todos los campos marcados con (*) son obligatorios.', 'Aceptar');
          return;
        } else if (flag == false) {
          this.rest.submit('post', 'bibliographyRelation', this.superFormGroup.value, '');
        }
      }
    });

    this.setCurrentBibliographyNumber();
  }
  catalogError() { this.dialog.errorDialog('Error', this.labels.errors.catalogs, this.labels.button.ok, this.labels.redirects.mainUaManagement); }
  getAllCountries() {
    this.rest.get('country', (cs: Country[], errorMsg) => {
      if (cs) {

        for (const c of cs) {
          this.plcOpt.push(new SelectOptionModel(c, c.name));
        }
        this.getAllTypes();
      } else {
        console.log('count');
          this.catalogError();
      }
    });
  }
  getAllTypes() {
    this.rest.get('bibliographyType', (types: BibliographyType[], errorMsg) => {
      if (types) {

        for (const t of types) {
          this.tpeOpt.push(new SelectOptionModel(t, t.type ));
        }
      } else {
        console.log('tu');
        this.catalogError();
      }
    });
  }
  getAllAuthors(): any {
    this.rest.get('author', (aus: Author[], errorMsg) => {
      if (aus) {
        console.log('entraCatAut');
        for (const a of aus) {
          let flag = false;
          for (const opt of this.authOpt) {
            if (opt.value.id == a.id) {
              flag = true;
              break;
            }
          }
          if (flag == false) {
            this.authOpt.push(new SelectOptionModel(a, a.name + ' ' + a.parentalSurname + ' ' + a.maternalSurname));
          }
        }
      }
    });
  }
  getCatalogs() {
    this.rest.get('author', (aus: Author[], errorMsg) => {
      if (aus) {
        console.log('entr');
        for (const a of aus) {
          if (this.authOpt.indexOf(new SelectOptionModel(a, a.name + ' ' + a.parentalSurname + ' ' + a.maternalSurname)) == -1) {
            this.authOpt.push(new SelectOptionModel(a, a.name + ' ' + a.parentalSurname + ' ' + a.maternalSurname));
          }
        }

      }/* else {
        console.log('aut');
        this.catalogError();
        return;
      }*/
      this.getAllCountries();
    });
  }
  updateAuthor(selectValue) {
    this.superFormGroup.patchValue({bibliography: {authors: selectValue} });
  }
  updateCountry(ev) {
    this.superFormGroup.patchValue({bibliography: {publicationPlace: ev} });
  }
  updateType(ev) {
    this.superFormGroup.patchValue({bibliographyType: ev});
  }
  ngOnInit(): void {
    this.getCatalogs();

  }

}


