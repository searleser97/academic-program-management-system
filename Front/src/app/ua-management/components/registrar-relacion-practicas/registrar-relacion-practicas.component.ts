import {Component, Input, OnInit} from '@angular/core';
import {labels, table} from './strings';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {Observable, of} from 'rxjs';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Practice} from '../../classes/Practice';
import {RESTService} from '../../../shared/services/REST.service';
import {PracticeRelation} from '../../classes/PracticeRelation';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {ThematicUnit} from '../../classes/ThematicUnit';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {EvaluationUA} from '../../classes/EvaluationUA';
import {PracticeRelationEvaluation} from '../../classes/PracticeRelationEvaluation';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-registrar-relacion-practicas',
  templateUrl: './registrar-relacion-practicas.component.html',
  styleUrls: ['./registrar-relacion-practicas.component.scss']
})

export class RegistrarRelacionPracticasComponent implements OnInit {
  @Input() learningUnitId: number;

  labels = labels;
  acabado = false;
  uniTem: ThematicUnit;
  private regex = '^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ .,;:"\\d-\\/]+$';
  luOptions: SelectOptionModel[] = [];
  thOptions: SelectOptionModel[] = [];
  luSelect: SelectDataModel = new SelectDataModel(this.labels.learningUnit, of(this.luOptions));
  thSelect: SelectDataModel = new SelectDataModel(this.labels.thematicUnit, of(this.thOptions));
  learningUnit: LearningUnit = new LearningUnit();
  formGroupPractice;
  formGroup;
  // Tabla
  tableContent: Practice[] = [];
  practiceInfo$: Observable<Practice[]> = of(this.tableContent);
  tableHeader = table.header;
  constructor(private ac: ActivatedRoute, private fb: FormBuilder, private rest: RESTService, private dialog: DialogService) {
    this.learningUnitId = this.ac.snapshot.params.id;
    if (!this.learningUnitId) {
      this.learningUnitId = 1;
    }
    this.formGroup = this.fb.group({
      id: [0, Validators.required],
      learningUnit: ['', Validators.required],
      accreditation: ['', Validators.required],
      totalHours: ['', Validators.required],
      practices: fb.array([]),
      practiceRelationEvaluations: fb.array([fb.group({
        id: [0, Validators.required],
        name: ['', Validators.required],
        percentage: [100, Validators.required]
      })]),
      finished: [false]
    });
    this.loadIfExist();
    this.cargaCatUTs();
  }
    async initProvisionalLearningUnit(lu: LearningUnit)  {
    this.learningUnit = lu;
    if (this.learningUnit) {
      this.formGroup.patchValue({learningUnit: this.learningUnit});
    }

  }
  async damePracticalRelation() {
    return this.rest.getAsync<PracticeRelation>(
      'practiceRelation/practiceRelationsByLearningUnitId/' + this.learningUnitId,
      null, true);
  }
  async loadIfExist() {
    const pr =  await this.damePracticalRelation();
    if (pr) {
      const pre = pr.practiceRelationEvaluations;
      this.practiceInfo$ = of(pr.practices);
      for (let i = 1; i < pre.length; i++) {
        this.addEval();
      }
      this.formGroup.patchValue({
        practiceRelationEvaluations: pr.practiceRelationEvaluations,
        totalHours: pr.totalHours,
        accreditation: pr.accreditation
      });
      this.practices.reset();
      for (const p of pr.practices) {
        this.addPract(p.number, p.name, p.length, p.placeOfPractice, p.thematicUnit);
      }

      if (pr.finished == true) {
        this.acabado = true;
        this.formGroup.disable();
      }
      this.practiceInfo$ = of(pr.practices);
    }
  }
  get practiceRelationEvaluations() {
    return this.formGroup.get('practiceRelationEvaluations') as FormArray;
  }
  get practices() {
    return this.formGroup.get('practices') as FormArray;
  }
  ngOnInit() {

  }
  async onSubmit(isFinished?: boolean) {
    let sum = 0;
    for (let i = 0;  i < this.practiceRelationEvaluations.length; i++) {
      sum += (<PracticeRelationEvaluation>this.practiceRelationEvaluations.at(i).value).percentage;
    }
    if(sum != 100) {
      this.dialog.messageDialog(
        this.labels.avisoCienNotCompleto.title,
        this.labels.avisoCienNotCompleto.message,
        this.labels.avisoCienNotCompleto.ok);
    }else {
      console.log(this.formGroup.value);
      let total = 0;
      for (const c of this.practices.controls) {
        total += parseFloat(c.get('length').value);
      }
      this.formGroup.patchValue({totalHours: total});
      console.log(this.formGroup.value);
      const obj = <PracticeRelation>this.formGroup.value;
      obj.totalHours = total;
      if (isFinished === true) {
        obj.finished = true;
      }
      const pr = await this.damePracticalRelation();
      if (pr) {
        obj.id = pr.id;
        this.rest.submit('patch', 'practiceRelation', obj, null, () => {
          this.loadIfExist();
        });
      } else {
        this.rest.submit('post', 'practiceRelation', obj, null, () => {
          this.loadIfExist();
        });
      }
    }
  }
  async dameUTs() {
    return this.rest.getAsync<ThematicUnit[]>('thematicUnit/thematicUnitByLearningUnitId/' + this.learningUnitId);
  }
  async cargaCatUTs() {
    const uts = await  this.dameUTs();
    if (uts) {
      for (const ut of uts) {
        this.thOptions.push(new SelectOptionModel(ut, ut.content.name));
      }
    }
  }
  addEval(p: number = 100, n: string = '', i: number = 0) {
    this.practiceRelationEvaluations.push(this.fb.group({
      id: [i, Validators.required],
      percentage: [p, Validators.required],
      name: [n, Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])]
    }));
  }
  popEval() {
    if (this.practiceRelationEvaluations.length > 1) {
      this.practiceRelationEvaluations.removeAt(this.practiceRelationEvaluations.length - 1);
    }
  }
  addPract(num: number, nom: string, dur: number, place: string, ut: ThematicUnit) {
    let existe = false;
    const hrs = parseFloat(this.formGroup.get('totalHours').value) + dur;
    for (const c of this.practices.controls) {
      if (c.get('number').value == num) {

        this.dialog.errorDialog('Error', 'Ya existe una práctica con el número indicado.', 'Aceptar');
        existe = true;
        break;
      }
    }
    if (!existe) {
      if (num && nom && dur && place && ut) {
        this.practices.push(this.fb.group({
          id: [0, Validators.required],
          name: [nom, Validators.required],
          number: [num, Validators.required],
          length: [dur, Validators.required],
          placeOfPractice: [place, Validators.required],
          thematicUnit: [ut, Validators.required]
        }));
        console.log(this.practices.value);
        this.formGroup.patchValue({totalHours: hrs});
        this.formGroup.get('totalHours').disable();
        this.practiceInfo$ = of(this.practices.value);
      } else {
        return;
      }
    }
  }

  deletePract(num) {
    let hrs = parseFloat(this.formGroup.get('totalHours').value);
    for (let i = 0; i < this.practices.length; i++) {
      console.log(this.practices.at(i).get('number').value);
      console.log(num);
      if (this.practices.at(i).get('number').value == num) {
        hrs -= parseFloat(this.practices.at(i).get('length').value);
        this.practices.removeAt(i);
        break;
      }

    }
    this.formGroup.patchValue({totalHours: hrs});
    this.formGroup.get('totalHours').disable();
    this.practiceInfo$ = of(this.practices.value);
  }
}
