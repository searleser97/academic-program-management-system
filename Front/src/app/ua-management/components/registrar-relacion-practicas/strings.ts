export const labels = {
  pageTitle: 'Registrar relación de prácticas',
  pageDescription: 'Alguna descripción',
  learningUnit: 'Unidad de Aprendizaje',
  practiceNumber: 'Numero de práctica',
  practiceName: 'Nombre de práctica',
  thematicUnit: 'Unidad Temática',
  duration: 'Duración',
  place: 'Luegar de realización',
  totalHours: 'Total de horas',
  evalAndAcredit: 'Evaluación y Acreditación',
  evalName: 'Nombre de evaluación',
  evalPerc: 'Porcentaje: ',
  acredit: 'Acreditación',
  button: {
    save: 'Guardar',
    addPractice: 'Agregar práctica',
    cancel: 'Finalizar',
    delete: 'Eliminar',
  },
  actions: 'Acciones',
  avisoCienNotCompleto: {
    title: 'Aviso',
    message: 'Los porcentajes de evaluación no cumplen con el porcentaje total obligatorio',
    ok: 'Aceptar'
  }
 };
export const table = {
  header: [
    labels.practiceNumber,
    labels.practiceName,
    labels.thematicUnit,
    labels.place,
    labels.duration,
    labels.actions
  ],

}
