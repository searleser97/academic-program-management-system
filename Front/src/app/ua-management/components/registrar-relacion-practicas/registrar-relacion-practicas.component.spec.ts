import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarRelacionPracticasComponent } from './registrar-relacion-practicas.component';

describe('RegistrarRelacionPracticasComponent', () => {
  let component: RegistrarRelacionPracticasComponent;
  let fixture: ComponentFixture<RegistrarRelacionPracticasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarRelacionPracticasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarRelacionPracticasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
