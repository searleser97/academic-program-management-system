import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarElaboracionAutorizacionComponent } from './registrar-elaboracion-autorizacion.component';

describe('RegistrarElaboracionAutorizacionComponent', () => {
  let component: RegistrarElaboracionAutorizacionComponent;
  let fixture: ComponentFixture<RegistrarElaboracionAutorizacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarElaboracionAutorizacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarElaboracionAutorizacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
