import { Component, OnInit } from '@angular/core';

import { labels } from './strings';

import { FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import { FormService } from '../../../shared/services/form.service';
import { RESTService } from '../../../shared/services/REST.service';
import { SelectOptionModel } from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import { SelectDataModel } from '../../../utils/components/select-form-control/classes/SelectDataModel';
import { of } from 'rxjs';

import { ALFA_REGEX, FLOAT_REGEX } from '../../../../environments/environment';
import { DialogService} from '../../../shared/modules/material/services/dialog.service';
import { LocalStorage} from '../../../shared/classes/LocalStorage';
import { UserService} from '../../../shared/services/user.service';

import { LearningUnit } from 'src/app/learning-unit/classes/learning-unit';
import { HumanResource } from 'src/app/human-resource/classes/human-resource';
import { SyntheticProgram } from '../../classes/SyntheticProgram';
import { Academy } from '../../classes/Academy';

@Component({
  selector: 'app-registrar-elaboracion-autorizacion',
  templateUrl: './registrar-elaboracion-autorizacion.component.html',
  styleUrls: ['./registrar-elaboracion-autorizacion.component.scss']
})
export class RegistrarElaboracionAutorizacionComponent implements OnInit {
  labels = labels;
  fb: FormBuilder;
  formGroup;
  isFinished = false;
  learningUnitID : number;

  //Model
  learningUnit: LearningUnit;
  humanResources: HumanResource[];
  academies: Academy[];

  private elaboratedByOptions: SelectOptionModel[] = [];
  private revisedByOptions: SelectOptionModel[] = [];
  private authorizedByOptions: SelectOptionModel[] = [];
  private approvedByOption: SelectOptionModel[] = [];

  elaboratedBySelect: SelectDataModel;
  revisedBySelect: SelectDataModel;
  authorizedBySelect: SelectDataModel;
  approvedBySelect: SelectDataModel;

  constructor(fb: FormBuilder, private formService: FormService, 
              private userService: UserService,private dialog: DialogService,
              private rest: RESTService) {
    //Init sects
    this.elaboratedBySelect   = new SelectDataModel(this.labels.elaboratedBy, of(this.elaboratedByOptions));
    this.revisedBySelect   = new SelectDataModel(this.labels.revisedBy, of(this.revisedByOptions));
    this.authorizedBySelect   = new SelectDataModel(this.labels.authorizedBy, of(this.authorizedByOptions));
    this.approvedBySelect   = new SelectDataModel(this.labels.approvedBy, of(this.approvedByOption));
    this.learningUnitID = 0;
    //Init Form
    this.fb = fb;
    this.formGroup = fb.group({
      id: [0],
      elaboratedBy:[ , Validators.required],
      revisedBy:[ , Validators.required],
      authorizedBy:[ , Validators.required],
      approvedBy: [ , Validators.required],
      syntheticProgram: [ new SyntheticProgram() ]
    });
  }

  ngOnInit() {
    this.asyncRequest();
  }

  onSave(message: boolean = true) {
    LocalStorage.saveObject('authorizations_'+this.learningUnitID, this.formGroup.value);
    if(message)
      this.dialog.messageDialog('Mensaje', 'Avances guardados exitosamente.', 'Aceptar');
  }

  onSubmit() {
    console.log(this.formGroup.getRawValue());
    this.onSave(false);
    if(this.formGroup.valid) {
      const obj = JSON.parse(JSON.stringify(this.formGroup.value));
      this.rest.huitzoRequestPOST( 'authorizations', obj, null, () => {
        this.disableTodo(true);
      });
    } else {
      this.dialog.errorDialog('Error', 'Los campos marcados con (*) son obligatorios.', 'Aceptar');
    }
  }

  disableTodo(flag: boolean) {
    if (flag == true) {
      console.log('is Finished');
      this.formGroup.disable();
      this.elaboratedBySelect.disabled = true;
      this.revisedBySelect.disabled = true;
      this.authorizedBySelect.disabled = true;
      this.approvedBySelect.disabled = true;
      this.isFinished = true;
    } else {
      this.formGroup.enable();
      this.isFinished = false;
    }
  }

  prevSession(){
    var authorizationsAux = LocalStorage.getObject('authorizations_'+this.learningUnitID);
    if(authorizationsAux){
      this.formGroup.setValue(authorizationsAux);
      /*this.setSelectValue(this.elaboratedByOptions,this.elaboratedBySelect,
        authorizationsAux.elaboratedBy, authorizationsAux.elaboratedBy.name, false);
      this.setSelectValue(this.revisedByOptions,this.revisedBySelect,
        authorizationsAux.revisedBy, authorizationsAux.revisedBy.name, false);
      this.setSelectValue(this.authorizedByOptions,this.authorizedBySelect,
        authorizationsAux.authorizedBy, authorizationsAux.authorizedBy.name, false);
      this.setSelectValue(this.approvedByOption,this.approvedBySelect,
        authorizationsAux.approvedBy, authorizationsAux.approvedBy.name, false);*/
    }
  }

  async initLearningUnitForm(learningUnit: LearningUnit) {
    //this.formGroup.get('learningUnit').setValue(learningUnit);
    let syntheticProgramAux = new SyntheticProgram();
    syntheticProgramAux.learningUnit = learningUnit;
    this.learningUnit = learningUnit;
    this.learningUnitID = learningUnit.id;
    this.prevSession();
    this.formGroup.get('syntheticProgram').setValue(syntheticProgramAux);
    return true;
  }

  //Async request
  async asyncRequest(){
    if( await this.getHumanResourcesByWorkplaceId() &&  await this.getAcademies() ){
      this.setCatalogue();
      return true;
    }
    return false;
  }

  setCatalogue(){
    for(let humanResource of this.humanResources){
      this.revisedByOptions.push(    new SelectOptionModel( humanResource, humanResource.name ) );;
      this.authorizedByOptions.push( new SelectOptionModel( humanResource, humanResource.name ) );;
      this.approvedByOption.push(    new SelectOptionModel( humanResource, humanResource.name ) );;
    }
    for(let academy of this.academies){
      this.elaboratedByOptions.push( new SelectOptionModel( academy, academy.name ) );;
    }
  }

  async getAcademies() {
    const academies = await this.rest.getAsync<Academy[]>('academy');
    if ( academies) {
      this.academies = academies;
      return true;
    }
    return false;
  }

  async getHumanResourcesByWorkplaceId() {
    const humanResources = await this.rest.getAsync<HumanResource[]>(
      'humanResource/humanResourcesByWorkplaceId/' + this.userService.getWorkplace().id);
    if ( humanResources ) {
      this.humanResources = humanResources;
      return true;
    }
    return false;
  }

  setSelectValue( selectOption: SelectOptionModel[], selectDataModel: SelectDataModel,
    selectValueModel: any = null, selectViewVal: (string | number) = null, disable: boolean = false){
    //selectOption.push(new SelectOptionModel(selectValueModel, selectViewVal));
    selectDataModel.valueSelected = selectValueModel;
    selectDataModel.disabled = disable;
  }
  
}
