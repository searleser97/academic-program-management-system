export const labels = {
  pageTitle: 'Registrar elaboración y autorización.',
  pageDescription: 'Alguna descripción',
  elaboratedBy: 'Elaboró',
  revisedBy: 'Revisó',
  authorizedBy: 'Autorizo',
  approvedBy: 'Aprobó',

  button: {
    save: 'Guardar',
    finish: 'Finalizar'
  }
}
