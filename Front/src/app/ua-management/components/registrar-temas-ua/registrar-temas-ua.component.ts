import { Component, OnInit } from '@angular/core';
import {labels} from './strings';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Topic} from '../../classes/Topic';
import {Subtopic} from '../../classes/Subtopic';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {SubtopicContainer} from '../../classes/SubtopicContainer';
import {EvaluationAccreditationUA} from '../../classes/EvaluationAccreditationUA';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {ActivatedRoute, Router} from '@angular/router';
import {cancelWindowConfirm} from '../../super-strings';

@Component({
  selector: 'app-registrar-temas-ua',
  templateUrl: './registrar-temas-ua.component.html',
  styleUrls: ['./registrar-temas-ua.component.scss']
})
export class RegistrarTemasUaComponent implements OnInit {
  labels = labels;
  formGroup: FormGroup;
  topicArr: Topic[];
  learningUnitID: number;
  constructor(private ac: ActivatedRoute, private fb: FormBuilder, private dialog: DialogService, private r: Router) {
    this.learningUnitID = this.ac.snapshot.params.id;
    this.topicArr = this.loadTopicsFromLocalStorage();
    console.log(this.topicArr); console.log('----');
    console.log(this.learningUnitID);
    this.formGroup = fb.group({
      topics: fb.array([
        this.fb.group({
          id: [0, Validators.required],
          number: [1, Validators.required],
          name: [this.topicArr[0].name, Validators.required],
          theoricHours: [this.topicArr[0].theoricHours, Validators.required],
          practicalHours: [this.topicArr[0].practicalHours, Validators.required],
          autonomousHours: [this.topicArr[0].autonomousHours, Validators.required],
          subtopics: [this.topicArr[0].subtopics, Validators.required]
        })
      ])
    });

  }
  private initView() {
    if (this.loadTopicsFromLocalStorage() != null) {
      const topics = this.loadTopicsFromLocalStorage();
      console.log(topics);
      this.topics.at(0).get('number').disable();
      for (let i = 1; i < topics.length; i++) {
        this.addTopic(topics[i].number,
          topics[i].name,
          topics[i].theoricHours,
          topics[i].practicalHours,
          topics[i].autonomousHours,
          topics[i].subtopics);
        this.topics.at(i).get('number').disable();
      }

    } else {
      console.log('es null');
    }
  }
  private loadTopicsFromLocalStorage(): Topic[] {
    console.log(this.learningUnitID + ':topics');
    if (LocalStorage.getObject(this.learningUnitID + ':topics') == null) {
      console.log('no hay');
      return [new Topic()];
    } else {
      console.log('loaded');
      console.log(this.learningUnitID);
      return <Topic[]>(JSON.parse(localStorage.getItem(this.learningUnitID + ':topics')));
    }

  }
  get topics() {
    return this.formGroup.get('topics') as FormArray;
  }
  addTopic( num: number = 0, n: string = '', th: number = 0, pr: number = 0, aut: number = 0, sub: Subtopic[] = null) {
    this.topics.push(this.fb.group({
      id: [0, Validators.required],
      number: [this.topics.length + 1 , Validators.required],
      name: [n, Validators.required],
      theoricHours: [th, Validators.required],
      practicalHours: [pr, Validators.required],
      autonomousHours: [aut, Validators.required],
      subtopics: [sub, Validators.required]
    }));
    this.topics.at(this.topics.length - 1 ).get('number').disable();
    this.topics.at(this.topics.length - 1 ).patchValue({number: this.topics.length});
  }
  popTopic() {
    if (this.topics.length > 1) {
      this.topics.removeAt(this.topics.length - 1);
    }
  }
  closeWindow() { cancelWindowConfirm(this.dialog); }
  ngOnInit() {
    this.initView();

  }

  saveActualState() {
    console.log(this.topics.value);
    console.log(this.formGroup.value);
    let i = 1;
    for (const tp of this.topics.value) {
      tp.number = i;
      i++;
    }
    LocalStorage.saveObject(this.learningUnitID + ':topics', this.topics.value);
  }
  goSubtema(idTopic: number) {
    this.saveActualState();
    this.r.navigate([this.labels.redirects.subtopic + '/' + idTopic + '/' + this.learningUnitID]);
  }
  retrieveSubs() {
    for (let i = 0; i < this.topics.length; i++) {
      this.topics.at(i).get('subtopics').patchValue(<Subtopic[]>LocalStorage.getObject(this.learningUnitID + ':sub' + (i + 1) ));
    }
  }
  onSubmit() {
    this.retrieveSubs();
    if (this.formGroup.valid) {
      this.saveActualState();
      this.dialog.messageDialog(this.labels.successMsg.title,
        this.labels.successMsg.body,
        this.labels.successMsg.ok,
        null, this.onlyClose);
    } else {
      this.dialog.errorDialog(
        this.labels.incompleteMsg.title,
        this.labels.incompleteMsg.body,
        this.labels.incompleteMsg.ok
      );
      return;
    }
  }
  onCancel() {
    this.closeWindow();
  }
  onlyClose() {
    window.close();
  }
}
