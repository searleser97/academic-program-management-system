import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarTemasUaComponent } from './registrar-temas-ua.component';

describe('RegistrarTemasUaComponent', () => {
  let component: RegistrarTemasUaComponent;
  let fixture: ComponentFixture<RegistrarTemasUaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarTemasUaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarTemasUaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
