export const labels = {
  pageTitle: 'Registrar temas de la Unidad Temática',
  topicNum: 'No. Tema: ',
  topicName: 'Nombre del tema',
  hrsDocenteTeoricas: 'Horas con docente teóricas',
  hrsDocentePracticas: 'Horas con docente prácticas',
  hrsAutonomas: 'Horas de aprendizaje autónomo',
  button: {
    accept: 'Aceptar',
    addTopic: 'Agregar tema',
    dropTopic: 'Borrar último tema',
    save: 'Guardar',
    close: 'Cancelar',
    addSub: 'Registrar subtemas'
  },
  redirects: {
    subtopic: '/RSUA'
  },
  popupFeature: 'scrollbar=0, width=720, height=960',
  successMsg: {
    body: 'Registro finalizado exitosamente',
    title: 'Aviso',
    ok: 'Aceptar'
  },
  incompleteMsg: {
    body: 'Todos los campos con (*) son obligatorios.',
    title: 'Error',
    ok: 'Aceptar'
  }
}
