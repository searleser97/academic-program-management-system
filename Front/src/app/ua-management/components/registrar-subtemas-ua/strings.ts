export const labels = {
  pageTitle: 'Registrar subtemas de los temas de la Unidad Temática',
  subtopicNum: 'No. Subtema: ',
  subtopicName: 'Nombre del subtema',
  button: {
    addSubTopic: 'Agregar subtema',
    dropSubTopic: 'Eliminar último subtema',
    save: 'Guardar',
    close: 'Cancelar'
  },
  redirects: {
    topics: '/RTUA'
  },
  successMsg: {
    body: 'Registro finalizado exitosamente.',
    title: 'Aviso',
    ok: 'Aceptar'
  },
  incompleteMsg: {
    body: 'Todos los campos con (*) son obligatorios.',
    title: 'Error',
    ok: 'Aceptar'
  }
}
