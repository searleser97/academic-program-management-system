import { Component, OnInit } from '@angular/core';
import {labels} from './strings';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subtopic} from '../../classes/Subtopic';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {EvaluationAccreditationUA} from '../../classes/EvaluationAccreditationUA';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';
import {cancelAndBackConfirm} from '../../super-strings';

@Component({
  selector: 'app-registrar-subtemas-ua',
  templateUrl: './registrar-subtemas-ua.component.html',
  styleUrls: ['./registrar-subtemas-ua.component.scss']
})
export class RegistrarSubtemasUaComponent implements OnInit {
  labels = labels;
  formGroup: FormGroup;
  currentTopicNumber: number;
  subArr: Subtopic[];
  learnignUnitID: number;
  constructor(private r2: PreviousRouteService, private r: Router, private fb: FormBuilder, private actRoute: ActivatedRoute, private dialog: DialogService) {
    this.currentTopicNumber = this.actRoute.snapshot.params.id;
    this.learnignUnitID = this.actRoute.snapshot.params.id2;
    this.subArr = this.loadSubsFromLocalStorage();
    this.formGroup = fb.group({
      subtopics: fb.array([
        fb.group({
          id: [0],
          number: [this.subArr[0].number, Validators.required],
          name: [this.subArr[0].name, Validators.required]
        })
      ])
    });

  }
  private initView() {
    if (this.loadSubsFromLocalStorage() != null) {
      const sub = this.loadSubsFromLocalStorage();
      this.subtopics.at(0).get('number').disable();
      for (let i = 1; i < sub.length; i++) {
        this.addSubtopic(sub[i].id, sub[i].number, sub[i].name);
        this.subtopics.at(i).get('number').disable();
      }
    }
  }
  closeWindow() {
    this.r.navigate([this.r2.getPreviousUrl() + '/' + this.learnignUnitID]);
  }
  loadSubsFromLocalStorage(): Subtopic[] {
    if (!<Subtopic[]>LocalStorage.getObject(this.learnignUnitID + ':sub' + this.currentTopicNumber)) {
      return [new Subtopic()];
    } else {
      return <Subtopic[]>LocalStorage.getObject(this.learnignUnitID + ':sub' + this.currentTopicNumber);
    }
  }
  get subtopics() {
    return this.formGroup.get('subtopics') as FormArray;
  }
  addSubtopic(ID: number = 0, num: number = 0, n: string = '') {
    this.subtopics.push(this.fb.group({
      id: [ID, Validators.required],
      number: [num, Validators.required],
      name: [n, Validators.required]
    }));
    this.subtopics.at(this.subtopics.length-1).get('number').disable();
  }
  popSubtopic() {
    if (this.subtopics.length > 1) {
      this.subtopics.removeAt(this.subtopics.length - 1);
    }
  }
  ngOnInit() {
    this.initView();
  }
  onSubmit() {
    if(this.formGroup.valid) {
      let i = 1;
      for(let s of this.subtopics.value) {
        s.number = i;
        i++;
      }
      LocalStorage.saveObject(this.learnignUnitID + ':sub' + this.currentTopicNumber, this.subtopics.value);
      this.dialog.messageDialog(
        this.labels.successMsg.title,
        this.labels.successMsg.body,
        this.labels.successMsg.ok,
        null,
        () => {
          this.replaceWindow();
        }
      );
    } else {
      this.dialog.errorDialog(
        this.labels.incompleteMsg.title,
        this.labels.incompleteMsg.body,
        this.labels.incompleteMsg.ok
      );
      return;
    }
  }
  replaceWindow() {
    const to = this.labels.redirects.topics;
    this.r.navigate([to + '/' + this.learnignUnitID]);
  }
  goBack() {
    cancelAndBackConfirm(this.dialog);
  }

}
