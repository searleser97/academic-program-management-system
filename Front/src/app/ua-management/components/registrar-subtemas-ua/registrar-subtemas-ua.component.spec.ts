import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarSubtemasUaComponent } from './registrar-subtemas-ua.component';

describe('RegistrarSubtemasUaComponent', () => {
  let component: RegistrarSubtemasUaComponent;
  let fixture: ComponentFixture<RegistrarSubtemasUaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarSubtemasUaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarSubtemasUaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
