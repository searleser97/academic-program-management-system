import { Component, OnInit } from '@angular/core';
import {labels} from './strings';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RESTService} from '../../../shared/services/REST.service';
import {Author} from '../../classes/Author';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.scss']
})
export class AuthorComponent implements OnInit {
  labels = labels;
  formGroup: FormGroup;
  private regex = '^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ]+$';
  private authToPost: Author;
  constructor(public fb: FormBuilder, private rest: RESTService, private dialog: DialogService) {
    this.formGroup = fb.group({
      id: [0],
      name: ['', Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])],
      parentalSurname: ['', Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])],
      maternalSurname: ['']
    });
  }

  ngOnInit() {
  }
  closeWindow() {
    window.close();
  }
  showErrorDuplicatedAuthor() {
    this.dialog.errorDialog('Error', this.labels.autorExiste, this.labels.button.accept);
  }
  createAuthor() {
    if(this.formGroup.get('name').value == '') {
      this.dialog.errorDialog('Error', 'Todos los campos con (*) son obligatorios', 'Aceptar');
      return;
    }
    if(this.formGroup.get('parentalSurname').value == '') {
      this.dialog.errorDialog('Error', 'Todos los campos con (*) son obligatorios', 'Aceptar');
      return;
    }
    if(!this.formGroup.valid) {
      return;
    }
    console.log(this.formGroup.value);
    if (this.formGroup.get('maternalSurname').value == '' || this.formGroup.get('maternalSurname').value == '') {
      this.formGroup.patchValue({maternalSurname: ' '});
    }
    this.authToPost = <Author>(this.formGroup.value);
    this.rest.get('author', (data: Author[]) => {
      console.log(data);
      if (data) {
        for (const aut of data) {
          if (aut.name == this.authToPost.name && aut.parentalSurname == this.authToPost.parentalSurname && aut.maternalSurname == this.authToPost.maternalSurname) {
            this.showErrorDuplicatedAuthor();
            break;
          } else {
            this.rest.submit('post', 'author', this.formGroup.value, '', () => {
              this.closeWindow();
            });
          }
        }
      } else {
        this.rest.submit('post', 'author', this.formGroup.value, '', () => {
          this.closeWindow();
        });
      }
    });

  }

}
