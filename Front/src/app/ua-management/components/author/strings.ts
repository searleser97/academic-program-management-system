export const labels = {
  pageTitle: 'Registrar autor',
  name: 'Nombre',
  paternalSurname: 'Primer apellido',
  maternalSurname: 'Segundo apellido',
  button: {
    save: 'Guardar',
    cancel: 'Cancelar',
    accept: 'Aceptar'
  },
  autorExiste: 'El autor ya existe'
};
