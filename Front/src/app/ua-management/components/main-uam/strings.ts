import {UaManagementRoutes} from '../../ua-management-routing.module';

export const labels = {
  pageTitle: 'Principal',
  programaSintetico: 'Programa Sintético',
  programaExtenso: 'Programa en Extenso',
  unidadTematica: 'Unidades Temáticas',
  relacionPractica: 'Relación de Prácticas',
  sistemaEval: 'Sistema de Evaluación',
  biblio: 'Bibliografía',
  perfilDocente: 'Registrar Perfil Docente',
  elaboracionYAutorizacion: 'Elaboración y Autorización',
  terminarRegistro: 'Terminar Registro',
  routes: {
    biblio: 'BPHY',
    subEval: 'RSE',
    relPract: 'RRP',
    thematicUnit: 'RUT',
    programExtenso: 'RPE',
    programaSintetico: 'RPS',
    perfilDnte: 'RPD'
  },
  finish: "Finalizar Registro"
};
