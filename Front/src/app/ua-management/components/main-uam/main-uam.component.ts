import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { labels } from './strings';

import { RESTService } from '../../../shared/services/REST.service';
import { HttpClient } from '@angular/common/http';

import { LearningUnit}  from '../../../learning-unit/classes/learning-unit';
import { ExtensiveProgram } from '../../classes/ExtensiveProgram';
import { DialogService } from '../../../shared/modules/material/services/dialog.service';

import { RegistrarProgramaSinteticoComponent} from '../registrar-programa-sintetico/registrar-programa-sintetico.component';
import { RegistrarProgramaExtensoComponent} from '../registrar-programa-extenso/registrar-programa-extenso.component';
import { RegistrarUnidadTematicaComponent} from '../registrar-unidad-tematica/registrar-unidad-tematica.component';
import { RegistrarRelacionPracticasComponent} from '../registrar-relacion-practicas/registrar-relacion-practicas.component';
import { RegistrarSistemaEvaluacionComponent} from '../registrar-sistema-evaluacion/registrar-sistema-evaluacion.component';
import { BibliographyComponent} from '../bibliography/bibliography.component';
import { RegistrarElaboracionAutorizacionComponent} from '../registrar-elaboracion-autorizacion/registrar-elaboracion-autorizacion.component';

@Component({
  selector: 'app-main-uam',
  templateUrl: './main-uam.component.html',
  styleUrls: ['./main-uam.component.scss']
})
export class MainUamComponent implements OnInit {
  // Child Components
  @ViewChild(RegistrarProgramaExtensoComponent) extensiveProgramCoponent: RegistrarProgramaExtensoComponent;
  @ViewChild(RegistrarProgramaSinteticoComponent) syntheticProgramComponent: RegistrarProgramaSinteticoComponent;
  @ViewChild(RegistrarUnidadTematicaComponent) thematicUnitsComponent: RegistrarUnidadTematicaComponent;
  @ViewChild(RegistrarRelacionPracticasComponent) practiceRelationComponent: RegistrarSistemaEvaluacionComponent;
  @ViewChild(BibliographyComponent) bibliographyComponent: BibliographyComponent;
  @ViewChild(RegistrarSistemaEvaluacionComponent) evaluationSystemComponent: RegistrarSistemaEvaluacionComponent;
  @ViewChild(RegistrarElaboracionAutorizacionComponent) eAndAComponent: RegistrarElaboracionAutorizacionComponent;

  // Data Model
  labels = labels;
  idLearningUnit: number;
  learningUnit: LearningUnit;
  extensiveProgram: ExtensiveProgram;

  constructor(private rest: RESTService, private activatedRoute: ActivatedRoute, private dialog: DialogService, private http: HttpClient) {
    this.idLearningUnit = this.activatedRoute.snapshot.params['idLearningUnit'];
  }

  ngOnInit() {
    this.asyncRequest();
  }

  async asyncRequest() {
    if ( await this.getLearningUnit() ) {
      console.log('Success LearningUnit');
    }
    if ( await this.getExtensiveProgram() ) {
      console.log('Success Extensive Program');
    }
  }

  async getLearningUnit() {
    const learningUnit = await this.rest.getAsync<LearningUnit>('learningUnit/' + this.idLearningUnit);
    console.log('unidad de aprendizaje');
    console.log(learningUnit);
    if ( learningUnit ) {
      this.learningUnit = learningUnit;
      console.log(this.bibliographyComponent)
      this.bibliographyComponent.initProvisionalLearningUnit(learningUnit);
      this.syntheticProgramComponent.initProvisionalLearningUnit(learningUnit);
      this.extensiveProgramCoponent.initLearningUnitForm(learningUnit);
      this.thematicUnitsComponent.initProvisionalLearningUnit(learningUnit);
      this.eAndAComponent.initLearningUnitForm(learningUnit);
      this.evaluationSystemComponent.initProvisionalLearningUnit(learningUnit);
      return true;
    }
    return false;
  }

  async getExtensiveProgram() {
    const extensiveProgram = await this.rest.getAsync<ExtensiveProgram>('extensiveProgram/' + this.idLearningUnit);
    if ( extensiveProgram ) {
      this.extensiveProgram = extensiveProgram;
      return true;
    }
    return false;
  }
  onSubmit() {

  }

}
