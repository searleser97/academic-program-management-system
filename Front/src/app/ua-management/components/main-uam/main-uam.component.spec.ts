import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainUamComponent } from './main-uam.component';

describe('MainUamComponent', () => {
  let component: MainUamComponent;
  let fixture: ComponentFixture<MainUamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainUamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
