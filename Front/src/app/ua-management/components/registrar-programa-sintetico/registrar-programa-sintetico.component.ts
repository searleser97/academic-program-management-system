import {Component, Input, OnInit} from '@angular/core';
import {labels} from './strings';
import {FormBuilder, Validators} from '@angular/forms';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {Observable, of} from 'rxjs';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {RESTService} from '../../../shared/services/REST.service';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {HttpClient} from '@angular/common/http';
import {Content} from '../../classes/Content';
import {AccreditationType} from '../../classes/AccreditationType';
import {EvaluationAccreditationUA} from '../../classes/EvaluationAccreditationUA';
import {EvaluationUA} from '../../classes/EvaluationUA';
import {SyntheticProgram} from '../../classes/SyntheticProgram';
import {ActivatedRoute, Router} from '@angular/router';
import {URL_PREFIX} from '../../../../environments/environment';

@Component({
  selector: 'app-registrar-programa-sintetico',
  templateUrl: './registrar-programa-sintetico.component.html',
  styleUrls: ['./registrar-programa-sintetico.component.scss']
})
export class RegistrarProgramaSinteticoComponent implements OnInit {
  @Input() learningUnitID: number;
  private regex = '^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ .,;:"\\d-]+$';
  labels = labels;
  fb: FormBuilder;
  tableHeader = labels.contentTable.tableHeader;
  tableHeader2 = labels.contentTable.tableHeader2;
  isFinished = false;
  contents: Content[] = [];
  content$: Observable<Content[]> = of(this.contents);
  eval$: Observable<EvaluationUA[]>;
  private auOptions: SelectOptionModel[] = [];
  private apOptions: SelectOptionModel[] = [];
  private luOptions: SelectOptionModel[] = [];
  private sOptions: SelectOptionModel[] = [];
  auSelect: SelectDataModel;
  apSelect: SelectDataModel;
  luSelect: SelectDataModel;
  sSelect: SelectDataModel;
  formGroup;
  lear
  learnProv: LearningUnit;
  accType: string = '';
  constructor(private ac: ActivatedRoute, private r: Router, fb: FormBuilder, private rest: RESTService, private dialog: DialogService, private http: HttpClient) {
    this.fb = fb;
    this.luSelect = new SelectDataModel(this.labels.learningUnit, of(this.luOptions));
    this.sSelect = new SelectDataModel(this.labels.semester, of(this.sOptions));
    this.auSelect = new SelectDataModel(this.labels.academicUnit, of(this.auOptions));
    this.apSelect = new SelectDataModel(this.labels.academicProgram, of(this.apOptions));


    this.formGroup = this.fb.group({
      id: [0, Validators.required],
      learningUnit: ['', Validators.required],
      regard: ['', Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])],
      didacticOrientation: ['', Validators.compose([
        Validators.pattern(this.regex),
        Validators.required
      ])],
      evaluationAccreditationUA: ['', Validators.required],
      content: ['', Validators.required]
    });


  }
  initFarolearSelects() {
    this.sOptions.push(new SelectOptionModel(this.learnProv.semester,
      this.learnProv.semester.semesterNumber));
    this.luOptions.push(new SelectOptionModel(this.learnProv,
      this.learnProv.name));
    this.apOptions.push(new SelectOptionModel(this.learnProv.semester.studyPlan.academicProgram,
      this.learnProv.semester.studyPlan.academicProgram.name));
    this.auOptions.push(
      new SelectOptionModel(this.learnProv.semester.studyPlan.academicProgram.workplace,
        this.learnProv.semester.studyPlan.academicProgram.workplace.name)
    );
    this.luSelect.valueSelected = this.learnProv;
    this.sSelect.valueSelected = this.learnProv.semester;
    this.apSelect.valueSelected = this.learnProv.semester.studyPlan.academicProgram;
    this.auSelect.valueSelected = this.learnProv.semester.studyPlan.academicProgram.workplace;
    this.auSelect.disabled = true;
    this.luSelect.disabled = true;
    this.sSelect.disabled = true;
    this.apSelect.disabled = true;
  }
  initProvisionalLearningUnit(lu: LearningUnit) {
    this.learnProv = lu;
    this.learningUnitID = lu.id;
    this.formGroup.patchValue({learningUnit: lu});
    console.log('--------')
    console.log(lu)
    console.log('---------')
    this.rest.get('syntheticProgram/syntheticProgramsByLearningUnitId/' + this.learningUnitID, (sp: SyntheticProgram, errorMsg) => {
      if(sp) {
        // patch with server values

        LocalStorage.saveObject(this.learningUnitID + ':contentArray', sp.content);
        LocalStorage.saveObject(this.learningUnitID + ':evalAndAccredit', sp.evaluationAccreditationUA);
        LocalStorage.saveObject(this.learningUnitID + ':sp', sp);
        this.disableTodo(true);
        console.log('Ahhh')
      }
      if(LocalStorage.getObject(this.learningUnitID + ':sp')) {
        this.formGroup.patchValue(LocalStorage.getObject(this.learningUnitID + ':sp'));
      }
    });
    this.initFarolearSelects();
  }
  /*setProvisionalLearningUnit() {
    if(!this.learningUnitID)
    {
      this.learningUnitID = 1;
    }
    this.rest.get('learningUnit/' + this.learningUnitID.toString(), (lu: LearningUnit, errorMsg) => {
      if(lu){
        this.learnProv = lu;
        this.formGroup.patchValue({learningUnit: lu});
        this.initFarolearSelects();
      }
    });
  }*/
  goContents() {
    const myWindow = window.open(URL_PREFIX + this.labels.redirects.contents + '/' + this.learningUnitID, 'blank', this.labels.popupFeature);
    myWindow.focus();

  }
  loadContentToTable() {
    const buff = <Content[]>LocalStorage.getObject(this.learningUnitID + ':contentArray');
    if(buff) {
      this.content$ = of(buff);
    }
    const buff2 = <EvaluationAccreditationUA>LocalStorage.getObject(this.learningUnitID + ':evalAndAccredit');
    if(buff2) {
      this.accType = '';
      for(let t of buff2.accreditationType) {
        this.accType += t.name + '; ';
      }
      this.eval$ = of(buff2.evaluationUA);
    }
  }
  goEval() {
    const myWindow = window.open(URL_PREFIX + this.labels.redirects.eval + '/' + this.learningUnitID, 'blank', this.labels.popupFeature);
    myWindow.focus();
  }
  disableTodo(flag: boolean) {
    if (flag == true) {
      console.log('is Finished');
      this.formGroup.disable();
      this.isFinished = true;
    } else {
      this.formGroup.enable();
      this.isFinished = false;
    }
  }
  ngOnInit() {
    // this.setProvisionalLearningUnit();

  }
  onSave() {

    this.formGroup.patchValue({content: LocalStorage.getObject(this.learningUnitID + ':contentArray')});
    this.formGroup.patchValue({evaluationAccreditationUA: LocalStorage.getObject(this.learningUnitID + ':evalAndAccredit')});
    LocalStorage.saveObject(this.learningUnitID + ':sp', this.formGroup.value);
    this.dialog.messageDialog(this.labels.savedDialog.title,
      this.labels.savedDialog.body,
      this.labels.savedDialog.ok);
  }
  onSaveWithoutAlert() {
    this.formGroup.patchValue({content: LocalStorage.getObject(this.learningUnitID + ':contentArray')});
    this.formGroup.patchValue({evaluationAccreditationUA: LocalStorage.getObject(this.learningUnitID + ':evalAndAccredit')});
    LocalStorage.saveObject(this.learningUnitID + ':sp', this.formGroup.value);

  }
  onSubmit() {
    this.onSaveWithoutAlert();
    if(this.formGroup.valid) {
      const obj = JSON.parse(JSON.stringify(this.formGroup.value));
      this.rest.huitzoRequestPOST( 'syntheticProgram', obj, null, () => {
        this.disableTodo(true);
      });
      /*this.http.post('http://localhost:8080/syntheticProgram', obj).subscribe(a => {
        console.log(a);
      });*/
    } else {
      this.dialog.errorDialog('Error', 'Todos los campos marcados con (*) son obligatorios.', 'Aceptar');
    }
  }
  regresa() {this.r.navigate( ['/' + this.labels.redirects.uaManagementMain]); }
}
