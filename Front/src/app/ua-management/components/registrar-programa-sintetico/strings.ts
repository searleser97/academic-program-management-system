export const labels = {
  pageTitle: 'Registrar programa sintético.',
  pageDescription: 'Alguna descripción',
  academicUnit: 'Unidad Académica',
  academicProgram: 'Programa Académico',
  learningUnit: 'Unidad de Aprendizaje',
  semester: 'Semestre',
  propositoLeaningUnit: 'Propósito de la Unidad de Aprendizaje',
  contents: 'Registrar Contenidos (*)',
  didacticOrient: 'Orientación didáctica',
  evalAndAcredit: 'Registrar Evaluación y Acreditación (*)',
  biblio: 'Bibliografía',
  button: {
    save: 'Guardar',
    cancel: 'Finalizar'
  },
  redirects: {
    contents: 'RC',
    eval: 'REA',
    uaManagementMain: 'MUAM'
  },
  localStorageKey: 'evalAndAccredit',
  popupFeature: 'scrollbar=0, width=720, height=960',
  contentTable: {
    tableHeader: ['Número', 'Nombre de contenido'],
    tableHeader2: ['Nombre de evaluación', 'Porcentaje']
  },
  savedDialog: {
    title: 'Mensaje',
    body: 'Avances guardados exitosamente.',
    ok: 'Aceptar'
  }
}
