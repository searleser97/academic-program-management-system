import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarEvaluacionAcreditacionComponent } from './registrar-evaluacion-acreditacion.component';

describe('RegistrarEvaluacionAcreditacionComponent', () => {
  let component: RegistrarEvaluacionAcreditacionComponent;
  let fixture: ComponentFixture<RegistrarEvaluacionAcreditacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarEvaluacionAcreditacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarEvaluacionAcreditacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
