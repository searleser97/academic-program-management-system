export const labels = {
  pageTitle: 'Registrar Evaluación y Acreditación',
  acreditacion: 'Acreditación',
  eval: 'Evaluación ',
  percentage: 'Porcentaje: ',
  button : {
    addEval: 'Agregar evaluación',
    save: 'Guardar',
    cancel: 'Cancelar',
    dropEval: 'Eliminar ultima evaluación',
    addAcc: 'Registrar Acreditación'
  },
  localStorageKey: 'evalAndAccredit',
  avisoEval: {
    title: 'Aviso',
    message: 'Registro finalizado exitosamente',
    ok: 'Aceptar'
  },
  avisoCienNotCompleto: {
    title: 'Aviso',
    message: 'Los porcentajes de evaluación no cumplen con el porcentaje total obligatorio',
    ok: 'Aceptar'
  },
  catEmpty: {
    title: 'Error',
    message: 'Por el momento no se puede realizar el registro',
    ok: 'Aceptar'
  }
}
