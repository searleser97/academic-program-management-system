import { Component, OnInit } from '@angular/core';
import {labels} from './strings';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {EvaluationUA} from '../../classes/EvaluationUA';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {Content} from '../../classes/Content';
import {EvaluationAccreditationUA} from '../../classes/EvaluationAccreditationUA';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {of} from 'rxjs';
import {RESTService} from '../../../shared/services/REST.service';
import {AccreditationType} from '../../classes/AccreditationType';
import {ActivatedRoute, Router} from '@angular/router';
import {cancelWindowConfirm} from '../../super-strings';
/*
* Things to do, recover select value
* */
@Component({
  selector: 'app-registrar-evaluacion-acreditacion',
  templateUrl: './registrar-evaluacion-acreditacion.component.html',
  styleUrls: ['./registrar-evaluacion-acreditacion.component.scss']
})
export class RegistrarEvaluacionAcreditacionComponent implements OnInit {
  labels = labels;
  private regex = "^[a-zA-ZáéíóúàèìòùÀÈÌÒÙÁÉÍÓÚñÑüÜ .,;:\"\\d-\\/]+$";
  formGroup: FormGroup;
  evalAccredit: EvaluationAccreditationUA;
  accTyOps: SelectOptionModel[] = [];
  accTySelect: SelectDataModel;
  learningUnitID: number;
  constructor(private ac: ActivatedRoute, private r: Router, private  fb: FormBuilder, private dialog: DialogService, private rest: RESTService) {
    this.learningUnitID = this.ac.snapshot.params.id;
    this.evalAccredit = this.loadEvalFromLocalStorage();
    this.accTySelect = new SelectDataModel(labels.acreditacion, of(this.accTyOps), this.evalAccredit.accreditationType);
    this.formGroup = fb.group({
      accreditationType: ['', Validators.required],
      evaluationUA: this.fb.array([
          this.fb.group({
            id: [0],
            percentage: [this.evalAccredit.evaluationUA[0].percentage, Validators.compose([
              Validators.min(1), Validators.max(100)
            ])],
            name: [this.evalAccredit.evaluationUA[0].name, Validators.compose([
              Validators.pattern(this.regex),
              Validators.required
            ])]
          })
        ]
      )
    });

    if (this.loadEvalFromLocalStorage() != null) {

      const evalArr = this.loadEvalFromLocalStorage().evaluationUA;
      for (let i = 1; i < evalArr.length; i++) {
        this.addEval(evalArr[i].percentage, evalArr[i].name);
      }
    }
  }
  get evaluation() {
    return this.formGroup.get('evaluationUA') as FormArray;
  }
  ngOnInit() {
    this.getAccreditationTypes();

  }

  private loadEvalFromLocalStorage(): EvaluationAccreditationUA {
    if (!<EvaluationAccreditationUA>LocalStorage.getObject(this.learningUnitID + ':' + this.labels.localStorageKey)) {
      return new EvaluationAccreditationUA();
    } else {
      return <EvaluationAccreditationUA>LocalStorage.getObject(this.learningUnitID + ':' + this.labels.localStorageKey);
    }

  }
  addEval(p: number = 1, n: string = '', i: number = 0) {
     this.evaluation.push(this.fb.group({
        id: [i, Validators.required],
        percentage: [p, Validators.required],
       name: [n, Validators.compose([
         Validators.pattern(this.regex),
         Validators.required
       ])]
      }));
  }
  popEval() {
    if (this.evaluation.length > 1) {
      this.evaluation.removeAt(this.evaluation.length - 1);
    }
  }
  createEvalAccredit() {
    if(!this.formGroup.valid) {
      return;
    }
    this.saveEvalFromLocalStorage(this.formGroup.value);
    let sum = 0;
    for (let i = 0;  i < this.evaluation.length; i++) {
      sum += (<EvaluationUA>this.evaluation.at(i).value).percentage;
    }
    if (sum != 100) {
      this.dialog.messageDialog(
        this.labels.avisoCienNotCompleto.title,
        this.labels.avisoCienNotCompleto.message,
        this.labels.avisoCienNotCompleto.ok);
    } else {
      this.dialog.messageDialog(labels.avisoEval.title, this.labels.avisoEval.message, this.labels.avisoEval.ok, null, () => {
        window.close();
      });
    }
  }
  closeWindow() {
    cancelWindowConfirm(this.dialog);
  }
  saveEvalFromLocalStorage(c: EvaluationAccreditationUA): void {
    LocalStorage.saveObject(this.learningUnitID + ':' + this.labels.localStorageKey, c);
  }
  addAccreditation() {

    this.r.navigate(['/AAT']);
  }
  getAccreditationTypes() {
    this.rest.get('accreditationtype', (acrs: AccreditationType[], errorMsg) => {
      if (acrs) {
        for (const acr of acrs) {
          this.accTyOps.push(new SelectOptionModel(acr, acr.name));
        }
      } else {
        this.dialog.errorDialog(this.labels.catEmpty.title, this.labels.catEmpty.message, this.labels.catEmpty.ok, null, () => {
          this.closeWindow();
        });
      }
    });
  }
}
