export const labels = {
  pageTitle: 'Registrar contenido',
  numberOfContents: 'No. de contenidos',
  contentName: 'Nombre del contenido',
  contentNumber: 'Número de contenido',
  button: {
    save: 'Guardar',
    cancel: 'Cancelar'
  },
  avisoContenidos: {
    title: 'Aviso',
    message: 'Registro finalizado exitosamente',
    ok: 'Aceptar'
  }
}
