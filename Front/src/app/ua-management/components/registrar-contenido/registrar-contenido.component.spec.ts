import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarContenidoComponent } from './registrar-contenido.component';

describe('RegistrarContenidoComponent', () => {
  let component: RegistrarContenidoComponent;
  let fixture: ComponentFixture<RegistrarContenidoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarContenidoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarContenidoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
