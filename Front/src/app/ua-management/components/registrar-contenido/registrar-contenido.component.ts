import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './strings';
import {FormArray, FormBuilder, FormGroup, Validator, Validators} from '@angular/forms';
import {LocalStorage} from '../../../shared/classes/LocalStorage';
import {Content} from '../../classes/Content';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {cancelWindowConfirm, confirms} from '../../super-strings';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-registrar-contenido',
  templateUrl: './registrar-contenido.component.html',
  styleUrls: ['./registrar-contenido.component.scss']
})
export class RegistrarContenidoComponent implements OnInit {

  labels = labels;
  fb: FormBuilder;
  formGroup: FormGroup;
  numberOf: number;
  bufferContents: Content[];
  learningUnitID: number;
  constructor(fb: FormBuilder, private dialog: DialogService, private ac: ActivatedRoute) {
    this.fb = fb;
    this.learningUnitID = this.ac.snapshot.params.id;
    this.formGroup = fb.group({
      contents: this.fb.array([
        this.fb.group({
          id: [0],
          number: [1, Validators.required],
          name: [this.loadContenidosFromLocalStorage()[0].name, Validators.required]
        })
        ]
      )
    });
    if (this.loadContenidosFromLocalStorage() != null) {

      const contArr = this.loadContenidosFromLocalStorage();
      for (let i = 1; i < contArr.length; i++) {
        this.addContent(1, contArr[i].name);
      }
      this.numberOf = contArr.length;
    } else {
      this.numberOf = 1;
    }
  }

  ngOnInit() {
  }
  loadContenidosFromLocalStorage(): Content[] {
    if (<Content[]>LocalStorage.getObject(this.learningUnitID + ':contentArray')) {
      return <Content[]>LocalStorage.getObject(this.learningUnitID + ':contentArray');
    } else {
      return [new Content()];
    }
  }
  saveContenidosFromLocalStorage(c: Content[]): void {
    LocalStorage.saveObject(this.learningUnitID + ':contentArray', c);
  }
  createContents() {
    if(this.formGroup.valid) {
      this.saveContenidosFromLocalStorage(this.formGroup.value.contents);
      this.dialog.messageDialog(labels.avisoContenidos.title, this.labels.avisoContenidos.message, this.labels.avisoContenidos.ok, null,
        () => {
          window.close();
      });
    } else {
      this.dialog.errorDialog('Alerta',
        'Todos los contenidos marcados con (*) son obligatorios.',
        'Aceptar');
    }
  }
  get contents() {
    return this.formGroup.get('contents') as FormArray;
  }
  changeContentArray(i: number) {
    if (i < this.contents.length) {
      this.popContent(this.contents.length - i);
    } else {
      this.addContent(i - this.contents.length);
    }
  }
  addContent(cuantos: number, inName: string = '') {
    for (let i = 0; i < cuantos; i++) {
      this.contents.push(this.fb.group({
        id: [0],
        number: [this.contents.length + 1, Validators.required],
        name: [inName, Validators.required]
      }));
    }
  }
  popContent(cuantos: number) {
    const popedSize = this.contents.length - cuantos;
    for (; popedSize < this.contents.length;) {
      this.contents.removeAt(this.contents.length - 1);
    }
  }
  closeWindow() {
    cancelWindowConfirm(this.dialog);
  }
}
