import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrarSubtemaEvaluacionComponent } from './registrar-sistema-evaluacion.component';

describe('RegistrarSubtemaEvaluacionComponent', () => {
  let component: RegistrarSubtemaEvaluacionComponent;
  let fixture: ComponentFixture<RegistrarSubtemaEvaluacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegistrarSubtemaEvaluacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegistrarSubtemaEvaluacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
