export const labels = {
  pageTitle: 'Registrar subtema de evaluación',
  learningUnit: 'Unidad de Aprendizaje',
  period: 'Periodo',
  thematicUnit: 'Unidad Temática',
  evalProcedure: 'Procedimiento de Evaluación',
  percentageText: function(A) {
    return 'La Unidad Temática: ' + A + ' contribuye al total de la evaluación final con un porcentage de: ';
  },
  button: {
    addPeriod: 'Agregar periodo',
    save: 'Guardar',
    cancel: 'Cancelar'
  },
  catPer: [1, 2, 3, 4, 5]
};
