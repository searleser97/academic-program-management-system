import {Component, Input, OnInit} from '@angular/core';
import {labels} from './strings';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {Observable, of} from 'rxjs';
import {Periodo} from '../../classes/Periodo';
import {ActivatedRoute} from '@angular/router';
import {RESTService} from '../../../shared/services/REST.service';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {ThematicUnit} from '../../classes/ThematicUnit';

@Component({
  selector: 'app-registrar-sistema-evaluacion',
  templateUrl: './registrar-sistema-evaluacion.component.html',
  styleUrls: ['./registrar-sistema-evaluacion.component.scss']
})
export class RegistrarSistemaEvaluacionComponent implements OnInit {
  labels = labels;
  localLearningUnit: LearningUnit;
  learningUnitId: number;
  periodGroup;
  utNames: string[];
  formGroup: FormGroup;
  constructor(private fb: FormBuilder, private ac: ActivatedRoute, private rest: RESTService) {

    this.formGroup = fb.group({
      evaluationSystems: fb.array([fb.group({
        id: [0],
        period: ['', Validators.required],
        thematicUnit: ['', Validators.required],
        percentage: ['', Validators.required]
      })])
    });
    this.load();
  }
  async load(){

  }
  get evaluationSystems() {
    return this.formGroup.get('evaluationSystems') as FormArray;
  }
  async dameUTs() {
    return this.rest.getAsync<ThematicUnit[]>('thematicUnit/thematicUnitByLearningUnitId/' + this.learningUnitId);
  }
  ngOnInit() {
  }

  addPeriod() {
  }
  onSubmit() {
  }

  async initProvisionalLearningUnit( lu: LearningUnit ) {
    if(lu) {
      this.learningUnitId = lu.id;
      const arrayOfUTs = await this.dameUTs();
      if(arrayOfUTs != null)
      {
        this.evaluationSystems.at(0).patchValue({thematicUnit: arrayOfUTs[0]});
        this.utNames.push(arrayOfUTs[0].content.name);
        for(let i = 1; i< arrayOfUTs.length; i++) {
          this.utNames.push(arrayOfUTs[i].content.name);
          this.addEvaluationSystem(arrayOfUTs[i]);
        }
        console.log('------------')
        console.log(this.formGroup.value);
        console.log('--------------sasa')
      }
    }

  }
  addEvaluationSystem(ut: ThematicUnit, period: number = 1, perc: number = 100) {
    this.evaluationSystems.push(this.fb.group({
      id: [0],
      period: [period, Validators.required],
      thematicUnit: [ut, Validators.required],
      percentage: [perc, Validators.required]
    }));
    this.formGroup.patchValue({evaluationSystems: this.evaluationSystems});
  }


}
