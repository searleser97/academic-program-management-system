export class StatusStudyPlan {
  id: number;
  name: string;
  description: string;

  constructor(id?, name?, description?) {
    this.id = id || 0;
    this.name = name || '';
    this.description = description || '';
  }
}
