import { StatusStudyPlanModule } from './status-study-plan.module';

describe('StatusStudyPlanModule', () => {
  let statusStudyPlanModule: StatusStudyPlanModule;

  beforeEach(() => {
    statusStudyPlanModule = new StatusStudyPlanModule();
  });

  it('should create an instance', () => {
    expect(statusStudyPlanModule).toBeTruthy();
  });
});
