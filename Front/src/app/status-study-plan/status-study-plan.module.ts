import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatusStudyPlanRoutingModule } from './status-study-plan-routing.module';

@NgModule({
  imports: [
    CommonModule,
    StatusStudyPlanRoutingModule
  ],
  declarations: []
})
export class StatusStudyPlanModule { }
