import {WorkplaceType} from '../../workplace-type/classes/workplace-type';

export class Workplace {
  id: number;
  name: string;
  abbreviation: string;
  workplaceType: WorkplaceType;

  constructor(id?, name?, abbreviation?, workPlaceType?) {
    this.id = id || 0;
    this.name = name || '';
    this.abbreviation = abbreviation || '';
    this.workplaceType = workPlaceType || new WorkplaceType();
  }
}
