import {Injectable} from '@angular/core';
import {User} from '../../user/classes/user';
import {Workplace} from '../../workplace/classes/workplace';
import {LocalStorage} from '../classes/LocalStorage';
import {Role} from '../../role/classes/role';

@Injectable({
  providedIn: 'root'
})

export class UserService {
  user: User;

  constructor() {
  }

  initUser(user: User) {
    this.user = user;
    LocalStorage.saveObject('user', user);
  }

  getId() {
    return this.user.id;
  }

  getToken() {
    return this.user.token;
  }

  getWorkplace(): Workplace {
    return this.user.humanResource.workplace;
  }

  getRoles(): Role[] {
    return this.user.roles;
  }
}

/*
const us: User = {
  'id': 1,
  'token': '1',
  'password': 'admin1',
  'email': 'admin1@test.com',
  'humanResource': {
    'id': 1,
    'name': 'Oscar',
    'firstSurname': 'Barrera',
    'secondSurname': 'Chávez',
    'title': {
      'id': 4,
      'name': 'Doctor',
      'abbreviation': 'Dr.',
      'description': ' Persona que posee el título de Doctor'
    },
    'positions': [{
      'id': 1,
      'name': 'Director',
      'abbreviation': 'Dir.',
      'description': 'Director'
    }],
    'workplace': {
      'id': 15,
      'name': 'Escuela Superior de Cómputo (ESCOM)',
      'abbreviation': 'ESCOM',
      'workplaceType': {
        'id': 2,
        'name': 'Unidad Académica',
        'abbreviation': 'U.A.',
        'description': 'Centro de estudios'
      }
    }
  },
  'roles': [
    {
      'id': 1,
      'name': 'Analista',
      'description': 'Encargado de: ',
      'rank': 1
    },
    {
      'id': 3,
      'name': 'Jefe de Innovación Educativa',
      'description': 'Encargado de: ',
      'rank': 2
    },
    {
      'id': 2,
      'name': 'Jefe de Departamento de Desarrollo e Innovación Curricular',
      'description': 'Encargado de: ',
      'rank': 3
    }
  ],
  'accountBlocked': true
};
*/

