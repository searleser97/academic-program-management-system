import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {RESTResponse} from '../classes/rest-response';
import {RESTRequest} from '../classes/rest-request';
import {SERVER_URL} from '../../../environments/environment';
import {DialogService} from '../modules/material/services/dialog.service';
import {UserService} from './user.service';

@Injectable({
  providedIn: 'root'
})
export class RESTService {

  public static OK = 200;
  public static FAIL = 500;
  public static DBFAIL = 501;

  constructor(private http: HttpClient, private dialogService: DialogService, private userService: UserService) {
  }

  public getUserToken(): string {
    if (this.userService.user)
      return this.userService.getId().toString();
    else
      return '';
  }

  public buildRequest(payload: any): RESTRequest<any> {
    return new RESTRequest<any>(this.getUserToken(), payload);
  }

  // All kind of requests
  public request<T>(method: string, endpoint: string, payload?: any): Observable<RESTResponse<T>> {

    const body = this.buildRequest(payload);

    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json; charset=UTF-8'})
    };

    switch (method.toLowerCase()) {
      case 'get':
        return this.http.get<RESTResponse<T>>(SERVER_URL + endpoint, httpOptions);
      case 'post':
        return this.http.post<RESTResponse<T>>(SERVER_URL + endpoint, body, httpOptions);
      case 'put':
        return this.http.put<RESTResponse<T>>(SERVER_URL + endpoint, body, httpOptions);
      case 'patch':
        return this.http.patch<RESTResponse<T>>(SERVER_URL + endpoint, body, httpOptions);
      case 'delete':
        return this.http.delete<RESTResponse<T>>(SERVER_URL + endpoint, httpOptions);
      default:
        return undefined;
    }
  }

  get<T>(resource: string, callback: (data: T, errorMsg: string) => void): void {
    this.request('get', resource).subscribe((response: RESTResponse<T>) => {
      console.log('get', resource, response);
      if (response.code !== RESTService.OK) {
        callback(null, response.message);
        return;
      }
      callback(response.payload, 'No errors');
    }, () => {
      callback(null, 'Servicios no disponibles.');
    });
  }
  huitzoRequestPOST(resource: string, payload: any, actionAfter?: string, callback?: () => void) {
    this.http.post(SERVER_URL + resource, payload).subscribe(response => {
      console.log('submit', resource, payload, response);
      // @ts-ignore
      if (response.code !== RESTService.OK) {
        // @ts-ignore
        this.dialogService.errorDialog('Error', response.message, 'Aceptar');
        return;
      }
      // @ts-ignore
      this.dialogService.messageDialog('Mensaje:', response.message, 'Aceptar', actionAfter, callback);
    }, () => {
      this.notAvailableServicesDialog();
    });
  }
  submit(method: string, resource: string, payload: any, actionAfter?: string, callback?: () => void) {
    this.request(method, resource, payload).subscribe(response => {
      console.log('submit', resource, payload, response);
      if (response.code !== RESTService.OK) {
        this.dialogService.errorDialog('Error', response.message, 'Aceptar');
        return;
      }
      this.dialogService.messageDialog('Mensaje:', response.message, 'Aceptar', actionAfter, callback);
    }, () => {
      this.notAvailableServicesDialog();
    });
  }

  async getAsync<T>(resource: string, actionAfter?: string, hideErrorAlert?: boolean): Promise<T> {
    try {
      const response = await this.request<T>('get', resource).toPromise();
      console.log('getAsync', resource, response);
      if (response.code === RESTService.OK) {
        return response.payload;
      }
      if (!hideErrorAlert) {
        this.dialogService.errorDialog('Error', response.message, 'Aceptar', actionAfter);
      }
      return null;
    } catch (e) {
      this.notAvailableServicesDialog();
      return null;
    }
  }

  async submitAsync<T>(method: string, resource: string, payload: any, actionAfter?: string, hideAlert?: boolean): Promise<boolean> {
    console.log(payload);
    try {
      const response: RESTResponse<T> = await this.request<T>(method, resource, payload).toPromise();
      console.log(method + 'Async', resource, response);
      if (response.code === RESTService.OK) {
        if (!hideAlert) {
          this.dialogService.messageDialog('Mensaje:', response.message, 'Aceptar', actionAfter);
        }
        return true;
      }
      this.dialogService.errorDialog('Error', response.message, 'Aceptar');
      return false;
    } catch (e) {
      this.notAvailableServicesDialog();
      return false;
    }
  }

  async deleteAsync<T>(resource: string, confirmMsg: string, actionAfter?: string): Promise<boolean> {
    const result = await this.dialogService.simpleConfirmDialog('Mensaje', confirmMsg, 'Si', 'No', true)
      .afterClosed().toPromise();
    if (result === 'ok') {
      try {
        const response: RESTResponse<T> = await this.request<T>('delete', resource).toPromise();
        if (response.code === RESTService.OK) {
          return true;
        }
        this.dialogService.errorDialog('Error', response.message, 'Aceptar', actionAfter);
        return false;
      } catch (e) {
        this.notAvailableServicesDialog();
        return false;
      }
    }
  }

  delete(resource: string, callback: (success: boolean) => void) {
    this.request('delete', resource).subscribe(response => {
      console.log('delete', resource, response);
      if (response.code !== RESTService.OK) {
        this.dialogService.errorDialog('Error', response.message, 'Aceptar');
        callback(false);
        return;
      }
      callback(true);
      this.dialogService.messageDialog('Mensaje:', response.message, 'Aceptar');
    }, () => {
      this.notAvailableServicesDialog();
      callback(false);
    });
  }

  notAvailableServicesDialog(failUrl?: string) {
    this.dialogService.errorDialog('Error', 'Servicios no disponibles.', 'Aceptar', failUrl);
  }

  notAvailableServiceMessage(failures: number, errorMsg, failUrl?: string) {
    if (failures === 1) {
      this.dialogService.errorDialog('Error', errorMsg, 'Aceptar', failUrl);
    }
  }
}
