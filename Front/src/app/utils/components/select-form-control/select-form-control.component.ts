import {Component, EventEmitter, Input, AfterViewInit, AfterViewChecked, OnInit, Output, ViewChild} from '@angular/core';
import {SelectDataModel} from './classes/SelectDataModel';
import {labels} from './strings';
import {Observable} from 'rxjs';
import {MatOption, MatSelect} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';



@Component({
  selector: 'app-select-form-control',
  templateUrl: './select-form-control.component.html',
  styleUrls: ['./select-form-control.component.scss']
})
export class SelectFormControlComponent implements OnInit, AfterViewInit, AfterViewChecked {


  @Input() selectDataObject: SelectDataModel;
  @Input() multiselect?: boolean;
  @ViewChild('matSelect') matSelect: MatSelect;
  @Output()
  onValueChange: EventEmitter<any> = new EventEmitter<any>();
  labels = labels;
  formGroup: FormGroup;
  a: MatOption;
  constructor(private fb: FormBuilder) {
    if (!this.multiselect) { this.multiselect = false; }
    this.formGroup = this.fb.group({
      mySelect: ['', Validators.required]
    });

  }

  ngOnInit() {

  }

  returnVal(val: any) {
    this.onValueChange.emit(val);
  }

  ngAfterViewInit() {
    console.log(this.matSelect.options);

    console.log(this.matSelect.options.length);
  }
  ngAfterViewChecked(): void {
    if (this.selectDataObject.valueSelected) {
      this.matSelect.options.forEach((item) => {
        if (!this.matSelect.multiple) {
          if (item.value.id == this.selectDataObject.valueSelected.id) {
            item.select();
          }
        }
      });
    }
  }


}
