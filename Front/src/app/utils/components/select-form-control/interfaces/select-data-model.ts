import {Observable} from 'rxjs';

export interface SelectDataModelInterface {
  disabled: boolean;
  placeholder: string;
  valueSelected: Object;
  options$: Observable<SelectOptionModelInterface[]>;
}

export interface SelectOptionModelInterface {
  value: any;
  viewValue: string | number;
  disable: boolean;
}
