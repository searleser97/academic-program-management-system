import {SelectOptionModelInterface} from '../interfaces/select-data-model';

export class SelectOptionModel implements SelectOptionModelInterface {
  value: any;
  viewValue: string | number;
  disable: boolean;
  constructor(val: any = null, viewVal: (string | number) = null, disable: boolean = false) {
    this.value = val;
    this.viewValue = viewVal;
  }
}
