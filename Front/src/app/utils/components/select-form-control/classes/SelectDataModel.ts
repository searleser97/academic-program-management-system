import {Observable} from 'rxjs';
import {SelectDataModelInterface, SelectOptionModelInterface} from '../interfaces/select-data-model';
import {Catalogo} from '../interfaces/Catalogo';

export class SelectDataModel implements SelectDataModelInterface {
  disabled: boolean;
  placeholder: string;
  valueSelected: any;
  options$: Observable<SelectOptionModelInterface[]>;

  constructor(placeholder: string, options: Observable<SelectOptionModelInterface[]>, valSelectd: any = null) {
    this.placeholder = placeholder;
    this.valueSelected = valSelectd;
    this.disabled = false;
    this.options$ = options;
  }

}
