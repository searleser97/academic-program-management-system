export class Position {
  id: number;
  name: string;
  abbreviation: string;
  description: string;

  constructor(id?, name?, abbreviation?, description?) {
    this.id = id || 1;
    this.name = name || '';
    this.abbreviation = abbreviation || '';
    this.description = description || '';
  }
}
