import {User} from '../../user/classes/user';
import {LearningUnit} from '../../learning-unit/classes/learning-unit';

export class LearningUnitTask {
  user: User;
  learningUnit: LearningUnit;
  startDate: string;
  deadline: string;


  constructor(startDate?, deadline?, user?, learningUnit?) {
    this.startDate = startDate || '';
    this.deadline = deadline || '';
    this.user = user || new User();
    this.learningUnit = learningUnit || new LearningUnit();
  }
}
