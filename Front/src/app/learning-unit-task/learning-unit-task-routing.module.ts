import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FindComponent} from './components/find/find.component';
import {CreateComponent} from './components/create/create.component';

export const learningUnitTaskRoutes: Routes = [
  {path: 'VTA', component: FindComponent},
  {path: 'VTA/:learningUnitId', component: CreateComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(learningUnitTaskRoutes)],
  exports: [RouterModule]
})
export class LearningUnitTaskRoutingModule {
}
