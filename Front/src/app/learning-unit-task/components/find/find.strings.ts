export const labels = {
  notFound: '-- No se encontraron opciones --',
  title: 'PROGRAMAS ACADEMICOS',
  workplace: 'Unidad Academica',
  academicProgram: 'Programa Academico',
  name: 'Nombre',
  id: 'Clave',
  edit: 'Elaborar'
};
