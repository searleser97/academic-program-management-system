import {Component, OnInit} from '@angular/core';
import {MatTableDataSource} from '@angular/material';
import {LearningUnitTask} from '../../classes/learning-unit-task';
import {RESTService} from '../../../shared/services/REST.service';
import {Router} from '@angular/router';
import {labels} from './find.strings';
import {UserService} from '../../../shared/services/user.service';

@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})
export class FindComponent implements OnInit {

  learningUnitTasks: LearningUnitTask[];
  learningUnitTaskDataSource: MatTableDataSource<LearningUnitTask>;

  labels = labels;

  displayedColumns = [
    'learningUnit.name',
    'learningUnit.learningUnitStatus.name',
    'view',
    'viewPDF'
  ];

  constructor(private rest: RESTService, private router: Router, private userService: UserService) {
    this.learningUnitTaskDataSource = new MatTableDataSource<LearningUnitTask>(this.learningUnitTasks);
  }

  ngOnInit() {
    this.getTasksForUser();
  }

  async getTasksForUser() {
    const learningUnitTasks = await this.rest.getAsync<LearningUnitTask[]>('learningUnitTask/learningUnitTasksByUserId/' +
      this.userService.getId());
    if (!learningUnitTasks) {
      return false;
    }
    this.learningUnitTasks = learningUnitTasks;
    this.learningUnitTaskDataSource.data = this.learningUnitTasks;
    return true;
  }

  view(id) {
    this.router.navigate(['/MUAM/' + id]);
  }

  viewPDF(learningUnitId) {
    // redirect to resource
  }
}
