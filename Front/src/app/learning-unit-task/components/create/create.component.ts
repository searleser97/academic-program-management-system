import {Component, OnInit} from '@angular/core';
import {labels, placeholders} from './create.strings';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable} from 'rxjs';
import {ActivatedRoute} from '@angular/router';
import {FormService} from '../../../shared/services/form.service';
import {LearningUnitTask} from '../../classes/learning-unit-task';
import {User} from '../../../user/classes/user';
import {LearningUnit} from '../../../learning-unit/classes/learning-unit';
import {RESTService} from '../../../shared/services/REST.service';
import {UserService} from '../../../shared/services/user.service';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  labels = labels;
  placeholders = placeholders;

  taskForm: FormGroup;

  users: User[];
  filteredUsers: Observable<User[]>;
  searchUsersCtrl = new FormControl();

  learningUnitId: number;

  constructor(private activatedRoute: ActivatedRoute, private formService: FormService, private rest: RESTService,
              private userService: UserService, private dialogService: DialogService, private previousRouteService: PreviousRouteService) {
    this.learningUnitId = parseInt(this.activatedRoute.snapshot.params['learningUnitId'], 10);
    this.taskForm = this.formService.newGroup(new LearningUnitTask(
      '', '',
      [{}, Validators.compose([Validators.required])],
      new LearningUnit(this.learningUnitId)
    ));
  }

  ngOnInit() {
    this.getActiveUsersForUser();
  }

  async getActiveUsersForUser() {
    let result = true;
    const users = await this.rest.getAsync<User[]>('user/activeUsersForUser/' + this.userService.getId());
    if (!users) {
      this.users = [];
      result = false;
    }
    this.users = users;
    this.filteredUsers = this.formService.getFilteredItems(this.searchUsersCtrl, this.users);
    return result;
  }

  async submit() {
    if (this.taskForm.invalid) {
      return;
    }
    const success = await this.rest.submitAsync<LearningUnitTask>('post', 'learningUnitTask', this.taskForm.getRawValue(), null, true);
    if (success) {
      this.dialogService.confirmDialog('', '¿Desea asignar la tarea a otro usuario?', 'No', 'Si', true,
        this.previousRouteService.getPreviousUrl());
    }

  }

  cancel() {
    this.dialogService.cancelDialog(this.previousRouteService.getPreviousUrl());
  }

}
