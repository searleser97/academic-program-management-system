import { LearningUnitTaskModule } from './learning-unit-task.module';

describe('LearningUnitTaskModule', () => {
  let learningUnitTaskModule: LearningUnitTaskModule;

  beforeEach(() => {
    learningUnitTaskModule = new LearningUnitTaskModule();
  });

  it('should create an instance', () => {
    expect(learningUnitTaskModule).toBeTruthy();
  });
});
