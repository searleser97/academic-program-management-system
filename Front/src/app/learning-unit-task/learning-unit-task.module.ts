import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FindComponent } from './components/find/find.component';
import {MaterialModule} from '../shared/modules/material/material.module';
import { CreateComponent } from './components/create/create.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ReactiveFormsModule
  ],
  declarations: [FindComponent, CreateComponent]
})
export class LearningUnitTaskModule { }
