import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {UserService} from 'src/app/shared/services/user.service';
import {User} from '../../user/classes/user';
import {AuthService} from './auth.service';
import {LocalStorage} from '../../shared/classes/LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authService: AuthService, private userService: UserService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.authService.isAuthenticated()) {
      return true;
    }
    if (LocalStorage.getObject('user') != null) {
      this.authService.authenticate();
      this.userService.initUser(LocalStorage.getObject('user') as User);
      return true;
    }
    this.router.navigate(['inicio']);
    return false;
  }
}
