import { Injectable } from '@angular/core';
import { LocalStorage } from '../../shared/classes/LocalStorage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authenticated = false;

  constructor() { }

  authenticate() {
    this.authenticated = true;
  }

  isAuthenticated(): boolean {
    return this.authenticated;
  }

  logout() {
    LocalStorage.removeAll();
    this.authenticated = false;
  }
}
