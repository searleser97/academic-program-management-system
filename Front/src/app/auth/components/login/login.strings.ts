export const labels = {
  id: 'Numero de Empleado',
  password: 'Contraseña',
  formTitle: 'Inicio de Sesión',
  formSubtitle: 'Ingrese los datos solicitados para entrar al sistema',
  submit: 'Entrar',
  snackBarMessage: 'Credenciales Incorrectas',
  snackBarAction: 'Omitir',
  email: 'Correo'
};

export const placeholders = {
  id: '2014090123',
  email: 'ejemplo@empleado.ipn.mx',
  password: '********'
};
