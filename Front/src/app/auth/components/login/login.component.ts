import {Component, OnInit} from '@angular/core';
import {UserService} from 'src/app/shared/services/user.service';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {labels, placeholders} from './login.strings';
import {FormGroup, Validators} from '@angular/forms';
import {UserCredentials} from '../../classes/user-credentials';
import {RESTService} from '../../../shared/services/REST.service';
import {User} from '../../../user/classes/user';
import {FormService} from '../../../shared/services/form.service';
import {PASSWD_REGEX} from '../../../../environments/environment';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  labels = labels;
  placeholders = placeholders;
  userCredentialsForm: FormGroup;

  submitted = false;
  error = false;

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private formService: FormService,
    private rest: RESTService,
    private dialogService: DialogService
  ) {
    this.userCredentialsForm = this.formService.newGroup(new UserCredentials(
      ['', Validators.compose([Validators.email])],
      ['', Validators.compose([Validators.pattern(PASSWD_REGEX)])]
    ));
  }

  ngOnInit() {
    this.authService.logout();
  }

  submit() {
    const user = new User(0, '', this.userCredentialsForm.getRawValue()['email'], this.userCredentialsForm.getRawValue()['password']);
    this.submitted = true;
    this.rest.request<User>('post', 'user/userByEmailAndPassword', user).subscribe(response => {
      if (response.code === RESTService.OK) {
        this.authService.authenticate();
        this.userService.initUser(response.payload as User);
        this.router.navigate(['/inicio']);
      } else {
        this.error = true;
      }
    }, () => {
      this.error = true;
      this.dialogService.errorDialog('Error', 'Servicios no disponibles por el momento.', 'Aceptar');
    });
  }
}
