import {Component, OnInit} from '@angular/core';
import {causes, labels, otherOptions} from './error404.strings';


@Component({
  selector: 'app-error404',
  templateUrl: './error404.component.html',
  styleUrls: ['./error404.component.scss']
})
export class Error404Component implements OnInit {
  etiquetas = labels;
  causas = causes;
  otrasOpciones = otherOptions;
  hola = 'hola mundo';

  constructor() {
  }

  ngOnInit() {
  }

}
