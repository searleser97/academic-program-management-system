export const labels = {
  pageTitle: 'Ooops... Error 404',
  pageSubtitle: 'Lo sentimos pero la página que buscas no existe.',
  options: 'Por favor verifique la dirección introducida e inténtelo de nuevo',
  otherOpctionsTitle: 'Otras opciones',
  causesTitle: 'Posibles motivos por los que la página solicitada no se encuentra disponible'
};

export const otherOptions = [
  'Utilizar el mapa web del sitio o las opciones de navegación general para ir a otra página.',
];

export const causes = [
  'Puede que haya cambiado de dirección (URL) o no existir',
  'Es posible que ésta no exista o no se haya escrito correctamente su URL, compruébela de nuevo para ver si es correcta.'
];
