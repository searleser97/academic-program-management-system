import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StatusRoutingModule } from './status-routing.module';

@NgModule({
  imports: [
    CommonModule,
    StatusRoutingModule
  ],
  declarations: []
})
export class StatusModule { }
