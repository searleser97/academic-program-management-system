import {Semester} from '../../semester/classes/semester';
import {FormationArea} from '../../formation-area/classes/formation-area';
import {Academy} from '../../ua-management/classes/Academy';
import {LearningUnitStatus} from './learning-unit-status';

export class LearningUnit {
  id: number;
  name: string;
  tepiccredits: number;
  satcacredits: number;
  theoryHoursPerWeek: number;
  practiceHoursPerWeek: number;
  formationArea: FormationArea;
  semester: Semester;
  academy: Academy;
  learningUnitStatus: LearningUnitStatus;

  constructor(id?, name?, tepiccredits?, satcacredits?, theoryHoursPerWeek?, practiceHoursPerWeek?, formationArea?, semester?,
              academy?, learningUnitStatus?) {
    this.id = id || 0;
    this.name = name || '';
    this.tepiccredits = tepiccredits || 0;
    this.satcacredits = satcacredits || 0;
    this.theoryHoursPerWeek = theoryHoursPerWeek || 0;
    this.practiceHoursPerWeek = practiceHoursPerWeek || 0;
    this.formationArea = formationArea || new FormationArea();
    this.semester = semester || new Semester();
    this.academy = academy || new Academy();
    this.learningUnitStatus = learningUnitStatus || new LearningUnitStatus();
  }
}
