import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './create-learning-unit.strings';
import {LearningUnitFormComponent} from '../learning-unit-form/learning-unit-form.component';
import {RESTService} from '../../../shared/services/REST.service';
import {LearningUnit} from '../../classes/learning-unit';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {ActivatedRoute} from '@angular/router';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {Semester} from '../../../semester/classes/semester';
import {StudyPlan} from '../../../study-plan/classes/study-plan';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';

@Component({
  selector: 'app-create-learning-unit',
  templateUrl: './create-learning-unit.component.html',
  styleUrls: ['./create-learning-unit.component.scss']
})

export class CreateLearningUnitComponent implements OnInit {

  @ViewChild(LearningUnitFormComponent) learningUnitFormComponent: LearningUnitFormComponent;
  labels: FormContainerLabels = labels;

  actionAfter = '/unidadesaprendizaje/consultar';
  semesterId: string;
  studyPlanId: string;

  constructor(private activatedRoute: ActivatedRoute, private rest: RESTService, private dialogService: DialogService,
              private previousRoute: PreviousRouteService) {
    this.studyPlanId = this.activatedRoute.snapshot.params['studyPlanId'];
    this.semesterId = this.activatedRoute.snapshot.params['semesterId'];
  }

  ngOnInit() {
    const learningUnit = new LearningUnit();
    learningUnit.semester = new Semester(parseInt(this.semesterId, 10), 0, new StudyPlan(parseInt(this.studyPlanId, 10)));
    this.learningUnitFormComponent.initLearningUnitForm(learningUnit);
  }

  submit() {
    const learningUnit = this.learningUnitFormComponent.getLearningUnit();
    if (learningUnit) {
      this.rest.submitAsync<LearningUnit>('post', 'learningUnit', learningUnit, this.previousRoute.getPreviousUrl());
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.previousRoute.getPreviousUrl());
  }

}

