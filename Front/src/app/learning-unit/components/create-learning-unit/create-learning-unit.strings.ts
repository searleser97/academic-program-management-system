export const labels = {
  formTitle: 'Registrar una Unidad de aprendizaje',
  formSubtitle:  'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
