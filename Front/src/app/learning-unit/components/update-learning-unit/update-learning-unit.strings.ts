export const labels = {
  formTitle: 'Editar una Unidad de aprendizaje',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
