import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './update-learning-unit.strings';
import {LearningUnitFormComponent} from '../learning-unit-form/learning-unit-form.component';
import {RESTService} from '../../../shared/services/REST.service';
import {ActivatedRoute} from '@angular/router';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {LearningUnit} from '../../classes/learning-unit';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';

@Component({
  selector: 'app-update-learning-unit',
  templateUrl: './update-learning-unit.component.html',
  styleUrls: ['./update-learning-unit.component.scss']
})
export class UpdateLearningUnitComponent implements OnInit {

  @ViewChild(LearningUnitFormComponent) learningUnitFormComponent: LearningUnitFormComponent;
  labels = labels;

  learningUnitId: string;

  actionAfter = '/unidadesaprendizaje/consultar';

  failures = 0;

  constructor(private rest: RESTService, private activatedRoute: ActivatedRoute, private dialogService: DialogService,
              private previousRoute: PreviousRouteService) {
    this.learningUnitId = this.activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.getLearningUnit(this.learningUnitId);
  }

  async getLearningUnit(learningUnitId) {
    const learningUnit = await this.rest.getAsync<LearningUnit>('learningUnit/' + learningUnitId);
    if (learningUnit) {
      this.learningUnitFormComponent.initLearningUnitForm(learningUnit);
    }
  }

  submit() {
    const leaningUnit = this.learningUnitFormComponent.getLearningUnit();
    if (leaningUnit) {
      this.rest.submitAsync<LearningUnit>('patch', 'learningUnit', leaningUnit, this.previousRoute.getPreviousUrl());
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.previousRoute.getPreviousUrl());
  }

}
