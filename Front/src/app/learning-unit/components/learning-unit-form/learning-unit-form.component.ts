import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Semester} from '../../../semester/classes/semester';
import {FormationArea} from '../../../formation-area/classes/formation-area';
import {Observable} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../../shared/services/form.service';
import {RESTService} from '../../../shared/services/REST.service';
import {labels, placeholders} from './learning-unit-form.strings';
import {LearningUnit} from '../../classes/learning-unit';
import {ALFA_REGEX, FLOAT_REGEX} from '../../../../environments/environment';
import {Academy} from '../../../ua-management/classes/Academy';
import {LearningUnitStatus} from '../../classes/learning-unit-status';

@Component({
  selector: 'app-learning-unit-form',
  templateUrl: './learning-unit-form.component.html',
  styleUrls: ['./learning-unit-form.component.scss']
})
export class LearningUnitFormComponent implements OnInit {

  @Output() onSubmit = new EventEmitter<boolean>();
  @Input() studyPlanId: string;
  @Input() disableSemester: boolean;

  labels = labels;
  placeholders = placeholders;

  semesters: Semester[];
  formationAreas: FormationArea[];
  academies: Academy[];

  filteredSemesters: Observable<Semester[]>;
  filteredFormationAreas: Observable<FormationArea[]>;
  filteredAcademies: Observable<Academy[]>;

  searchFormationAreaCtrl = new FormControl();
  searchSemesterCtrl = new FormControl();
  searchAcademiesCtrl = new FormControl();

  learningUnitForm: FormGroup;
  learningUnit: LearningUnit;

  failures = 0;
  failUrl = 'unidadesaprendizaje/consultar';

  constructor(private formService: FormService, private rest: RESTService) {

    this.learningUnitForm = this.formService.newGroup(new LearningUnit(
      [0],
      ['', Validators.compose([Validators.pattern(ALFA_REGEX), Validators.required])],
      [{value: 0, disabled: true}, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [0, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [0, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [0, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [{}, Validators.compose([Validators.required])],
      [{}, Validators.compose([Validators.required])],
      [{}, Validators.compose([Validators.required])],
      [new LearningUnitStatus(), Validators.compose([Validators.required])]
    ));
    this.learningUnitForm.get('theoryHoursPerWeek').valueChanges.subscribe(val => {
      this.setTEPICCredits();
    });
    this.learningUnitForm.get('practiceHoursPerWeek').valueChanges.subscribe(val => {
      this.setTEPICCredits();
    });
  }

  setTEPICCredits() {
    this.learningUnitForm.get('tepiccredits')
      .setValue(2 * parseFloat(this.learningUnitForm.get('theoryHoursPerWeek').value) +
        parseFloat(this.learningUnitForm.get('practiceHoursPerWeek').value));
  }

  ngOnInit() {
    if (this.disableSemester) {
      this.learningUnitForm.get('semester').disable();
    }
  }

  getErrors(controlName: string): string[] {
    return Object.keys(this.learningUnitForm.get(controlName).errors || {});
  }

  getLearningUnit(): LearningUnit {
    for (const controlName in this.learningUnitForm.controls) {
      if (this.learningUnitForm.controls.hasOwnProperty(controlName)) {
        this.learningUnitForm.controls[controlName].markAsTouched();
      }
    }
    return this.learningUnitForm.invalid ? null : this.learningUnitForm.getRawValue();
  }

  async initLearningUnitForm(learningUnit: LearningUnit) {
    this.learningUnitForm.setValue(learningUnit);
    this.learningUnit = learningUnit;

    if (!await this.getSemestersByStudyPlanId() || !await this.getFormationAreas() || !await this.getAcademies()) {
      return false;
    }
    return true;
  }

  async getSemestersByStudyPlanId() {
    this.semesters = await this.rest.getAsync<Semester[]>('semester/semestersByStudyPlanId/' + this.learningUnit.semester.studyPlan.id);
    if (!this.semesters) {
      return false;
    }
    this.filteredSemesters = this.formService.getFilteredItems(this.searchSemesterCtrl, this.semesters);
    this.learningUnitForm.get('semester')
      .setValue(this.semesters.find(opt => opt.id === this.learningUnit.semester.id));
    return true;
  }

  async getFormationAreas() {
    this.formationAreas = await this.rest.getAsync<FormationArea[]>('formationArea');
    if (!this.formationAreas) {
      return false;
    }
    this.filteredFormationAreas = this.formService.getFilteredItems(this.searchFormationAreaCtrl, this.formationAreas);
    this.learningUnitForm.get('formationArea')
      .setValue(this.formationAreas.find(opt => opt.id === this.learningUnit.formationArea.id));
    return true;
  }

  async getAcademies() {
    const academies = await this.rest.getAsync<Academy[]>('academy');
    if (!academies) {
      return false;
    }
    this.academies = academies;
    this.filteredAcademies = this.formService.getFilteredItems(this.searchAcademiesCtrl, this.academies);
    this.learningUnitForm.get('academy')
      .setValue(this.academies.find(opt => opt.id === this.learningUnit.academy.id));
    return true;
  }
}
