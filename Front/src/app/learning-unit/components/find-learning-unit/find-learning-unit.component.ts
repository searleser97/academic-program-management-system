import {Component, Input, OnInit} from '@angular/core';
import {labels} from './find-learning-unit.strings';
import {LearningUnit} from '../../classes/learning-unit';
import {MatTableDataSource} from '@angular/material';
import {RESTService} from '../../../shared/services/REST.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-find-learning-unit',
  templateUrl: './find-learning-unit.component.html',
  styleUrls: ['./find-learning-unit.component.scss']
})
export class FindLearningUnitComponent implements OnInit {

  @Input() semesterId: string;
  @Input() studyPlanId: string;

  labels = labels;

  displayedColumns = [
    'name',
    'tepiccredits',
    'satcacredits',
    'theoryHoursPerWeek',
    'practiceHoursPerWeek',
    'formationArea',
    'edit',
    'manage',
    'delete'
  ];

  learningUnits: LearningUnit[];
  learningUnitsDataSource: MatTableDataSource<LearningUnit>;

  failures = 0;
  failUrl = '';

  constructor(private rest: RESTService, private router: Router) {
    this.learningUnitsDataSource = new MatTableDataSource(this.learningUnits);
  }

  ngOnInit() {
    this.getLearningUnitsBySemesterId(this.semesterId);
  }

  getLearningUnitsBySemesterId(semesterId) {
    this.rest.get<LearningUnit[]>('learningUnit/learningUnitsBySemesterId/' + semesterId, (learningUnits, errorMsg) => {
      if (!learningUnits) {
        this.rest.notAvailableServiceMessage(++this.failures, errorMsg, this.failUrl);
        return;
      }
      this.learningUnits = learningUnits;
      this.learningUnitsDataSource = new MatTableDataSource<LearningUnit>(this.learningUnits);
    });
  }

  edit(id) {
    this.router.navigate(['/unidadesaprendizaje/editar/' + id]);
  }

  view(id) {
    this.router.navigate(['/MUAM/' + id]);
  }

  add() {
    this.router.navigate(['/unidadesaprendizaje/registrar/' + this.studyPlanId + '/' + this.semesterId]);
  }

  manage(id) {
    this.router.navigate(['/VTA/' + id]);
  }

  async delete(id, name) {
    const success = await this.rest.deleteAsync('learningUnit/' + id,
      '¿Está seguro que desea eliminar la Unidad de Aprendizaje ' + name + '?');
    if (success) {
      this.getLearningUnitsBySemesterId(this.semesterId);
    }
  }

}
