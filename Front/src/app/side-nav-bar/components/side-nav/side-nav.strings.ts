class SubOption {
  description: string;
  path: string;

  constructor(description, path) {
    this.description = description;
    this.path = path;
  }
}

class Option {
  description: string;
  path: string;
  icon: string;
  hasSuboptions: boolean;
  suboptions: SubOption[];
  ranks: Set<number>;

  constructor(description, path, icon, hasSuboptions, suboptions, ranks?) {
    this.description = description;
    this.path = path;
    this.icon = icon;
    this.hasSuboptions = hasSuboptions;
    this.suboptions = suboptions;
    this.ranks = ranks;
  }
}

export const menuDB = [
  new Option('Ver Tareas', '/VTA', 'chevron_right', false, [], new Set([1, 3, 4, 5, 6, 9])),
  new Option('Gestionar Programas Academicos', '/programasacademicos/consultar', 'chevron_right', false, [],
    new Set([4, 5, 6, 9])),
  new Option('Gestionar Usuarios', '/usuarios/consultar', 'chevron_right', false, [],
    new Set([4, 5, 6, 9])),
  new Option('Gestionar Recursos Humanos', '/recursoshumanos/consultar', 'chevron_right', false, [],
    new Set([4])),
  new Option('Salir', '/login', 'exit_to_app', false, [], new Set([1, 3, 4, 5, 6, 9]))
];

