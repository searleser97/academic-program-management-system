import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {ShowUaComponent} from './components/show-ua/show-ua.component';
import {RevisionComponent} from './components/main-revision/revision.component';

const base = 'paquetes/';

export const UaPackagesRoutes: Routes = [
  {path: base+'showUa', component: ShowUaComponent},
  {path: base+'revision', component: RevisionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(UaPackagesRoutes)],
  exports: [RouterModule]
})
export class UaPackagesRoutingModule {
}
