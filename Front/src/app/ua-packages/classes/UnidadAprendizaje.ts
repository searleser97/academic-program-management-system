export class UnidadAprendizaje {
    "programaSintetico": ProgramaSintetico;
}

export class ProgramaSintetico{
    "unidadAcademica" : string;
    "programaAcademico" : string;
    "unidadAprendizaje" : string;
    "semestre" : number;
    "proposito" : string;
    "contenidos" : Contenido[];
    "orientacionDidactica": string;
    "evaluacionYAcreditacion": string;
    "bibliografia": Object;
    constructor( unidadAcademica, programaAcademico, unidadAprendizaje, semestre, 
        proposito, contenidos, orientacionDidactica, evaluacionYAcreditacion, bibliografia ){
        this.unidadAcademica = unidadAcademica;
        this.programaAcademico = programaAcademico;
        this.unidadAprendizaje = unidadAprendizaje;
        this.semestre = semestre;
        this.proposito = proposito;
        this.contenidos = contenidos;
        this.orientacionDidactica = orientacionDidactica;
        this.evaluacionYAcreditacion = evaluacionYAcreditacion;
        this.bibliografia = bibliografia;
    }
}

export class ProgramaExtenso{
    "unidadAcademica" : string;
    "programaAcademico" : string;
    "unidadAprendizaje" : string;
    "areaFormacion" : string;
    "modalidad" : string;
    "tipoUA" : string;
    "vigencia" : string;
    "semestre" : number;
    "creditos" : number;
    "intencionEducativa" : string;
    "proposito" : string;
    "tiempoAsignado": TiempoAsignado;
    "contenidos" : Contenido[];
    "orientacionDidactica": string;
    "evaluacionYAcreditacion": string;
    "bibliografia": Object;
    constructor( programaSintetico: ProgramaSintetico,
        areaFormacion, modalidad, tipoUA, vigencia, creditos, intencionEducativa,tiempoAsignado){
        this.unidadAcademica = programaSintetico.unidadAcademica;
        this.programaAcademico = programaSintetico.programaAcademico;
        this.unidadAprendizaje = programaSintetico.unidadAprendizaje;
        this.areaFormacion = areaFormacion;
        this.modalidad = modalidad;
        this.tipoUA = tipoUA;
        this.vigencia = vigencia;
        this.semestre = programaSintetico.semestre;
        this.creditos = creditos;
        this.intencionEducativa = intencionEducativa;
        this.proposito = programaSintetico.proposito;
        this.tiempoAsignado = tiempoAsignado;
        this.contenidos = programaSintetico.contenidos;
        this.orientacionDidactica = programaSintetico.orientacionDidactica;
        this.evaluacionYAcreditacion = programaSintetico.evaluacionYAcreditacion;
        this.bibliografia = programaSintetico.bibliografia;
    }
}

export class Contenido{
    "numero": number;
    "nombre": string;
    constructor(numero, nombre){
        this.numero = numero;
        this.nombre = nombre;
    }
}

export class Acreditacion{
    "nombre" : string;
}

export class TiempoAsignado{
    "horaTeoricaSemana": number;
    "horaPracticaSemana": number;
    "horaTeoricaSemestre": number;
    "horaPracticaSemestre": number;
    "horaAprendizajeAutonomo": number;
    "horaTotalSemestre": number;
    constructor( horaTeoricaSemana, horaPracticaSemana, horaAprendizajeAutonomo){
        this.horaTeoricaSemana = horaTeoricaSemana;
        this.horaTeoricaSemestre = this.horaTeoricaSemana * 18;
        this.horaPracticaSemana = horaPracticaSemana;
        this.horaPracticaSemestre = this.horaPracticaSemana * 18;
        this.horaAprendizajeAutonomo = horaAprendizajeAutonomo;
        this.horaTotalSemestre = this.horaTeoricaSemestre + this.horaPracticaSemestre + this.horaAprendizajeAutonomo;
    }
}