
export class Practice {
  practiceNumber: number;
  practiceName: string;
  thematicUnit: string;
  duration: number;
  constructor(number: number, name: string, thematicUnit: string, duration: number)
  {
    this.practiceName = name;
    this.practiceNumber = number;
    this.thematicUnit = thematicUnit;
    this.duration = duration;
  }
}
