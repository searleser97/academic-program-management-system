import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisarProgramaExtensoComponent } from './registrar-programa-extenso.component';

describe('RevisarProgramaExtensoComponent', () => {
  let component: RevisarProgramaExtensoComponent;
  let fixture: ComponentFixture<RevisarProgramaExtensoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisarProgramaExtensoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisarProgramaExtensoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
