export const labels = {
  pageTitle: 'Programa en extenso.',
  pageDescription: 'Alguna descripción',
  academicUnit: 'Unidad Académica',
  academicProgram: 'Programa Académico',
  learningUnit: 'Unidad de Aprendizaje',
  formationArea: 'Área de formación',
  mode: 'Modalidad',
  learningUnitType: 'Tipo de unidad de aprendizaje',
  vige: 'Vigencia',
  semester: 'Semestre',
  credits: 'Creditos',
  educIntention: 'Intención educativa',
  propositoLeaningUnit: 'Propósito de la Unidad de Aprendizaje',
  didacticOrient: 'Orientación didáctica',
  timesAsig: 'Tiempos asignados',
  TheoryTimePerWeek: 'Horas tería/semana',
  PracticeTimePerWeek: 'Horas práctica/semana',
  TheoryTimePerSemester: 'Horas tería/semestre',
  PracticeTimePerSemester: 'Horas tería/semestre',
  AutoTimePerSemester: 'Horas de aprendizaje autónomo',
  TotalTimePerSemester: 'Horas totales/semestre',
  button: {
    save: 'Guardar',
    cancel: 'Cancelar'
  }
}
