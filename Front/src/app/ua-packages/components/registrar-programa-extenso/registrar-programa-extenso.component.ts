import { Component, OnInit, Input } from '@angular/core';
import {labels} from './strings';
import {FormBuilder, Validators} from '@angular/forms';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {of} from 'rxjs';
import { ProgramaExtenso } from '../../classes/UnidadAprendizaje';

@Component({
  selector: 'app-registrar-programa-extenso',
  templateUrl: './registrar-programa-extenso.component.html',
  styleUrls: ['./registrar-programa-extenso.component.scss']
})

export class RevisarProgramaExtensoComponent implements OnInit {
  labels = labels;
  @Input() public programaExtenso: ProgramaExtenso;

  fb: FormBuilder;
  formGroup;
  private auOptions: SelectOptionModel[] = [];
  private apOptions: SelectOptionModel[] = [];
  private luOptions: SelectOptionModel[] = [];
  private sOptions: SelectOptionModel[] = [];
  private faOptions: SelectOptionModel[] = [];
  private moOptions: SelectOptionModel[] = [];
  private typeOptions: SelectOptionModel[] = [];
  faSelect: SelectDataModel = new SelectDataModel(this.labels.formationArea, of(this.faOptions));
  moSelect: SelectDataModel = new SelectDataModel(this.labels.mode, of(this.moOptions));
  typeSelect: SelectDataModel = new SelectDataModel(this.labels.learningUnitType, of(this.typeOptions));
  auSelect: SelectDataModel = new SelectDataModel(this.labels.academicUnit, of(this.auOptions));
  apSelect: SelectDataModel = new SelectDataModel(this.labels.academicProgram, of(this.apOptions));
  luSelect: SelectDataModel = new SelectDataModel(this.labels.learningUnit, of(this.luOptions));
  sSelect: SelectDataModel = new SelectDataModel(this.labels.semester, of(this.sOptions));

  constructor(fb: FormBuilder) {
    this.fb = fb;
    this.formGroup = fb.group({
      vigencia: ['', Validators.required],
      creditos: ['', Validators.required],
      intencion: ['', Validators.required],
      proposito: ['', Validators.required],
      learningUnit: ['', Validators.required],
      academicProgram: ['', Validators.required],
      academicUnit: ['', Validators.required],
      semester: ['', Validators.required],
      formationArea: ['', Validators.required],
      mode:['', Validators.required],
      type: ['', Validators.required]
    });
  }

  ngOnInit() {}
  onSubmit() {
    console.log(this.formGroup.value);
  }


}
