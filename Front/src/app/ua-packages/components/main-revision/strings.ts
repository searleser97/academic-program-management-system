export const labels = {
  tagTitle: [
    'Programa sintético',
    'Programa Extenso',
    'Unidades Temáticas',
    'Relación de Prácticas',
    'Subtema Evaluación',
    'Bibliografía',
  ]
}