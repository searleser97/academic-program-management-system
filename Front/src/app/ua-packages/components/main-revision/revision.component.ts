import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators, FormsModule} from '@angular/forms';

import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {of} from 'rxjs';

import { labels } from './strings';
import { ProgramaSintetico, ProgramaExtenso, Contenido, TiempoAsignado } from '../../classes/UnidadAprendizaje';

@Component({
  selector: 'app-revision',
  templateUrl: './revision.component.html',
  styleUrls: ['./revision.component.scss']
})
export class RevisionComponent implements OnInit {
  labels = labels;
  public programaSintetico:ProgramaSintetico = new ProgramaSintetico (
      "Escuela Superior de Cómputo","Ingenieria en Sistemas", "Introducción a los microcontroladores",6,
      "Programa los recursos periféricos de un microcontrolador usando lenguajes ensamblador y de alto nivel",
      [new Contenido(1,"Arquitectura del microcontrolador"), new Contenido(2," Periféricos básicos del microcontrolador")],
      "La presente unidad se abordará a partir de la estrategia  aprendizaje basada en casos y el método de enseñanza heurístico,  de  tal forma  que  el  alumno  ponga  en  práctica  los  conocimientos  adquiridos  y  desarrollen  las habilidades  de  abstracción.",
      {},
      {}
  );
  public programaExtenso:ProgramaExtenso = new ProgramaExtenso(
    this.programaSintetico,
    "Profesional",
    "Presencial",
    "Teórico – práctica Obligatoria",
    "Agosto 2011",
    "7.5 TEPIC –  4.39 SATCA ",
    "Esta unidad de aprendizaje contribuye al perfil del egresado de Ingeniería en Sistemas Computacionales, al desarrollar las habilidades de abstracción, análisis, diseño e implementación de algoritmos eficientes usando un microcontrolador para el desarrollo de sistemas embebidos y de algoritmos de procesamiento digital de señales.",
    new TiempoAsignado(3.0,1.5,0)
  );
  
  constructor() {

  }

  ngOnInit() {
  }

}
