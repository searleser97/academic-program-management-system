import { Component, OnInit, Input } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';

import {SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import {SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import {of} from 'rxjs';

import { labels } from './strings';
import { ProgramaSintetico } from '../../classes/UnidadAprendizaje';

@Component({
  selector: 'app-programa-sintetico',
  templateUrl: './registrar-programa-sintetico.component.html',
  styleUrls: ['./registrar-programa-sintetico.component.scss']
})
export class RevisarProgramaSinteticoComponent implements OnInit {
  //Comunicacion padre
  @Input() public programaSintetico: ProgramaSintetico;
  
  labels = labels;
  

  ngOnInit() {}
  onSubmit() {}

}
