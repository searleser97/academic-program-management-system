export const labels = {
  pageTitle: 'Programa sintético.',
  pageDescription: 'Alguna descripción',
  academicUnit: 'Unidad Académica',
  academicProgram: 'Programa Académico',
  learningUnit: 'Unidad de Aprendizaje',
  semester: 'Semestre',
  propositoLeaningUnit: 'Propósito de la Unidad de Aprendizaje',
  contents: 'Contenidos',
  didacticOrient: 'Orientación didáctica',
  evalAndAcredit: 'Evaluación y Acreditación',
  biblio: 'Bibliografía',
  button: {
    save: 'Guardar',
    cancel: 'Cancelar'
  }
}
