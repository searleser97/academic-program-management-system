import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisarProgramaSinteticoComponent } from './registrar-programa-sintetico.component';

describe('RevisarProgramaSinteticoComponent', () => {
  let component: RevisarProgramaSinteticoComponent;
  let fixture: ComponentFixture<RevisarProgramaSinteticoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisarProgramaSinteticoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisarProgramaSinteticoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
