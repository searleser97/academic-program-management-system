export const labels = {
  pageTitle: 'Registrar relación de prácticas',
  pageDescription: 'Alguna descripción',
  learningUnit: 'Unidad de Aprendizaje',
  practiceNumber: 'Numero de práctica',
  practiceName: 'Nombre de práctica',
  thematicUnit: 'Unidad Temática',
  duration: 'Duración',
  place: 'Luegar de realización',
  totalHours: 'Total de horas',
  evalAndAcredit: 'Evaluación y Acreditación',
  button: {
    save: 'Guardar',
    addPractice: 'Agregar práctica',
    cancel: 'Cancelar',
    delete: 'Eliminar',
  },
  actions: 'Acciones'
 };
export const table = {
  header: [
    labels.practiceNumber,
    labels.practiceName,
    labels.thematicUnit,
    labels.duration,
    labels.actions
  ],

}
