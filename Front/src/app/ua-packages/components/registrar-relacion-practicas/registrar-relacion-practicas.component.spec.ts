import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisarRelacionPracticasComponent } from './registrar-relacion-practicas.component';

describe('RevisarRelacionPracticasComponent', () => {
  let component: RevisarRelacionPracticasComponent;
  let fixture: ComponentFixture<RevisarRelacionPracticasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisarRelacionPracticasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisarRelacionPracticasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
