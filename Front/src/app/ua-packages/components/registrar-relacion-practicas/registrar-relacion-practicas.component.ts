import { Component, OnInit } from '@angular/core';
import { labels, table} from './strings';
import { SelectOptionModel} from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import { SelectDataModel} from '../../../utils/components/select-form-control/classes/SelectDataModel';
import { Observable, of} from 'rxjs';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import { Practice} from '../../classes/Practice';

@Component({
  selector: 'app-registrar-relacion-practicas',
  templateUrl: './registrar-relacion-practicas.component.html',
  styleUrls: ['./registrar-relacion-practicas.component.scss']
})
export class RevisarRelacionPracticasComponent implements OnInit {
  labels = labels;
  luOptions: SelectOptionModel[] = [];
  thOptions: SelectOptionModel[] = [];
  plOptions: SelectOptionModel[] = [];
  plSelect: SelectDataModel = new SelectDataModel(this.labels.place, of(this.plOptions));
  luSelect: SelectDataModel = new SelectDataModel(this.labels.learningUnit, of(this.luOptions));
  thSelect: SelectDataModel = new SelectDataModel(this.labels.thematicUnit, of(this.thOptions));
  fb: FormBuilder;
  formGroupPractice;
  formGroup;
  // Tabla
  tableContent: Practice[] = [
    new Practice(1, 'Prueba', 'Unidad tematica', 3)
  ];
  practiceInfo$: Observable<Practice[]> = of(this.tableContent);
  tableHeader = table.header;
  constructor(fb: FormBuilder) {
    this.fb = fb;
    this.formGroupPractice = this.fb.group({
      practiceNumber: ['', Validators.required],
      practiceName: ['', Validators.required],
      thematicUnit: ['', Validators.required],
      duration: ['', Validators.required]
    });
    this.formGroup = this.fb.group({
      learningUnit: ['', Validators.required],
      place: ['', Validators.required],
      totalHours: ['', Validators.required]
    });
  }

  ngOnInit() {
  }
  onSubmit() { }
  deletePract(practice) {

  }
}
