export const labels = {
  pageTitle: 'Registrar unidad temática.',
  pageDescription: 'Alguna descripción',
  learningUnit: 'Unidad de Aprendizaje',
  thematicUnitNumber: 'No. Unidad Temática',
  thematicUnitName: 'Nombre',
  competenceUnit: 'Unidad de Competencia',
  topics: 'Temas',
  learningStrategies: 'Estrategias de Aprendizaje',
  learningEvaluation: 'Evaluación de los aprendizajes',
  button: {
    save: 'Guardar',
    cancel: 'Cancelar'
  }
}
