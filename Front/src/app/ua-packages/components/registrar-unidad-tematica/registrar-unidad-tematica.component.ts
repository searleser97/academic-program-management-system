import { Component, OnInit } from '@angular/core';
import { labels} from './strings';
import { FormBuilder, Validators } from '@angular/forms';
import { SelectOptionModel } from '../../../utils/components/select-form-control/classes/SelectOptionModel';
import { SelectDataModel } from '../../../utils/components/select-form-control/classes/SelectDataModel';
import { of } from 'rxjs';

@Component({
  selector: 'app-registrar-unidad-tematica',
  templateUrl: './registrar-unidad-tematica.component.html',
  styleUrls: ['./registrar-unidad-tematica.component.scss']
})
export class RevisarUnidadTematicaComponent implements OnInit {
  labels = labels;
  fb: FormBuilder;
  private auOptions: SelectOptionModel[] = [];
  auSelect: SelectDataModel = new SelectDataModel(this.labels.learningUnit, of(this.auOptions));
  formGroup;
  constructor(fb: FormBuilder) {
    this.fb = fb;
    this.formGroup = this.fb.group({
      learningUnit: ['', Validators.required],
      thematicUnitNumber: ['', Validators.required],
      thematicUnitName: ['', Validators.required],
      competenceUnit: ['', Validators.required],
      learningStrategies: ['', Validators.required]
    });
  }

  ngOnInit() {}
  onSubmit() {}

}
