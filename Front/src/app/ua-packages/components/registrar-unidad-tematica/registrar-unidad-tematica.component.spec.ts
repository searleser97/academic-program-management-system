import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RevisarUnidadTematicaComponent } from './registrar-unidad-tematica.component';

describe('RevisarUnidadTematicaComponent', () => {
  let component: RevisarUnidadTematicaComponent;
  let fixture: ComponentFixture<RevisarUnidadTematicaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RevisarUnidadTematicaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RevisarUnidadTematicaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
