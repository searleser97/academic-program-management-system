import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatTabsModule } from '@angular/material';
import { MaterialModule } from '../shared/modules/material/material.module';
import { UtilsModule } from '../utils/utils.module';

import { UaPackagesRoutingModule } from './ua-packages-routing.module';
import { ShowUaComponent } from './components/show-ua/show-ua.component';
import { RevisionComponent } from './components/main-revision/revision.component';
import { RevisarProgramaSinteticoComponent } from './components/registrar-programa-sintetico/registrar-programa-sintetico.component';
import { RevisarProgramaExtensoComponent} from './components/registrar-programa-extenso/registrar-programa-extenso.component';
import { RevisarUnidadTematicaComponent } from './components/registrar-unidad-tematica/registrar-unidad-tematica.component';
import { RevisarRelacionPracticasComponent } from './components/registrar-relacion-practicas/registrar-relacion-practicas.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    UtilsModule,
    UaPackagesRoutingModule,
    MatTabsModule,
    MaterialModule,
    FormsModule
  ],
  declarations: [
    ShowUaComponent,
    RevisionComponent,
    RevisarProgramaSinteticoComponent,
    RevisarProgramaExtensoComponent,
    RevisarUnidadTematicaComponent,
    RevisarRelacionPracticasComponent
  ]
})
export class UaPackagesModule { }
