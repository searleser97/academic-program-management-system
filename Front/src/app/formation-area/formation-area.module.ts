import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormationAreaRoutingModule } from './formation-area-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormationAreaRoutingModule
  ],
  declarations: []
})
export class FormationAreaModule { }
