import { FormationAreaModule } from './formation-area.module';

describe('FormationAreaModule', () => {
  let formationAreaModule: FormationAreaModule;

  beforeEach(() => {
    formationAreaModule = new FormationAreaModule();
  });

  it('should create an instance', () => {
    expect(formationAreaModule).toBeTruthy();
  });
});
