import {StudyPlan} from '../../study-plan/classes/study-plan';

export class Semester {
  id: number;
  semesterNumber: number;
  studyPlan: StudyPlan;


  constructor(id?, SemesterNumber?, studyPlan?) {
    this.id = id || 0;
    this.semesterNumber = SemesterNumber || 0;
    this.studyPlan = studyPlan || new StudyPlan();
  }
}
