import {Component, Input, OnInit} from '@angular/core';
import {Semester} from '../../classes/semester';
import {RESTService} from '../../../shared/services/REST.service';
import {StudyPlan} from '../../../study-plan/classes/study-plan';

@Component({
  selector: 'app-find-semester',
  templateUrl: './find-semester.component.html',
  styleUrls: ['./find-semester.component.scss']
})
export class FindSemesterComponent implements OnInit {

  @Input() studyPlanId: string;

  semesters = [];

  constructor(private rest: RESTService) {
  }

  ngOnInit() {
  }

  initSemesters() {
    this.getSemestersByStudyPlanId(this.studyPlanId);
  }

  async getSemestersByStudyPlanId(studyPlanId) {
    this.semesters = await this.rest.getAsync<Semester[]>('semester/semestersByStudyPlanId/' + studyPlanId, null, true);
  }

  async add() {
    const success = await this.rest.submitAsync<Semester>('post', 'semester',
      new Semester(0, 0, new StudyPlan(this.studyPlanId)), null, true);
    if (success) {
      this.getSemestersByStudyPlanId(this.studyPlanId);
    }
  }

  async delete(id, semesterNumber) {
    const success = await this.rest.deleteAsync('semester/' + id,
      '¿Está seguro que desea eliminar el Semestre ' + semesterNumber + '?');
    if (success) {
      this.getSemestersByStudyPlanId(this.studyPlanId);
    }
  }
}
