export class Role {
  id: number;
  name: string;
  description: string;
  rank: number;

  constructor(id?, name?, description?, rank?) {
    this.id = id || 0;
    this.name = name || '';
    this.description = description || '';
    this.rank = rank || 0;
  }
}
