import {AcademicProgram} from '../../academic-program/classes/academic-program';
import {Modality} from '../../modality/classes/modality';
import {StatusStudyPlan} from '../../status-study-plan/classes/status-study-plan';

export class StudyPlan {
  id: number;
  year: number;
  totalTEPICCredits: number;
  totalSATCACredits: number;
  totalTheoryHours: number;
  totalPracticeHours: number;
  academicProgram: AcademicProgram;
  modality: Modality;
  statusStudyPlan: StatusStudyPlan;

  constructor(id?, year?, totalTEPICCredits?, totalSATCACredits?, totalTheoryHours?, totalPracticeHours?, academicProgram?, modality?,
              statusStudyPlan?) {
    this.id = id || 0;
    this.year = year || 0;
    this.totalTEPICCredits = totalTEPICCredits || 0;
    this.totalSATCACredits = totalSATCACredits || 0;
    this.totalTheoryHours = totalTheoryHours || 0;
    this.totalPracticeHours = totalPracticeHours || 0;
    this.academicProgram = academicProgram || new AcademicProgram();
    this.modality = modality || new Modality();
    this.statusStudyPlan = statusStudyPlan || new StatusStudyPlan();
  }
}
