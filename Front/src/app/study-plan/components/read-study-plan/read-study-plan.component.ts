import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {labels} from './read-study-plan.strings';
import {StudyPlan} from '../../classes/study-plan';
import {ActivatedRoute, Router} from '@angular/router';
import {RESTService} from '../../../shared/services/REST.service';
import {MatTableDataSource} from '@angular/material';
import {FindSemesterComponent} from '../../../semester/components/find-semester/find-semester.component';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';

@Component({
  selector: 'app-read-study-plan',
  templateUrl: './read-study-plan.component.html',
  styleUrls: ['./read-study-plan.component.scss']
})
export class ReadStudyPlanComponent implements OnInit {

  labels = labels;
  @Input() studyPlanId: string;

  @ViewChild(FindSemesterComponent) findSemesterComponent: FindSemesterComponent;

  studyPlan: StudyPlan;
  studyPlanSource: MatTableDataSource<StudyPlan>;

  displayedColumns = [
    'year',
    'modality',
    'totalTEPICCredits',
    'totalSATCACredits',
    'totalTheoryHours',
    'totalPracticeHours',
    'edit'
  ];

  constructor(private rest: RESTService, private router: Router, private activatedRoute: ActivatedRoute,
              private dialogService: DialogService) {
    this.studyPlanId = this.activatedRoute.snapshot.params['id'];
    this.studyPlan = new StudyPlan();
    this.studyPlanSource = new MatTableDataSource<StudyPlan>([this.studyPlan]);
  }

  ngOnInit() {
    this.getStudyPlan(this.studyPlanId);
  }

  async getStudyPlan(studyPlanId) {
    const studyPlan = await this.rest.getAsync<StudyPlan>('studyPlan/' + studyPlanId);
    if (!studyPlan) {
      return false;
    }
    this.studyPlanSource = new MatTableDataSource<StudyPlan>([this.studyPlan = studyPlan]);
    this.findSemesterComponent.initSemesters();
    return true;
  }

  async edit(id) {
    const result = await this.dialogService.simpleConfirmDialog('Mensaje',
      '¿Está seguro que desea rediseñar el plan de estudios? (Se hará una copia de este)',
      'Si', 'No', true).afterClosed().toPromise();
    if (result === 'ok') {
      const studyPlan = await this.rest.getAsync<StudyPlan>('studyPlan/createCopy/' + id);
      if (!studyPlan) {
        return false;
      }
      this.router.navigate(['planesdeestudios/leer/' + studyPlan.id]);
      return true;
    }
    return false;
  }
}
