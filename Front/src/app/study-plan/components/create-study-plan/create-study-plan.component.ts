import {Component, OnInit, ViewChild} from '@angular/core';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {labels} from './create-study-plan.strings';
import {StudyPlanFormComponent} from '../study-plan-form/study-plan-form.component';
import {RESTService} from '../../../shared/services/REST.service';
import {StudyPlan} from '../../classes/study-plan';
import {ActivatedRoute} from '@angular/router';
import {AcademicProgram} from '../../../academic-program/classes/academic-program';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {Workplace} from '../../../workplace/classes/workplace';

@Component({
  selector: 'app-create-study-plan',
  templateUrl: './create-study-plan.component.html',
  styleUrls: ['./create-study-plan.component.scss']
})
export class CreateStudyPlanComponent implements OnInit {

  labels: FormContainerLabels = labels;
  @ViewChild(StudyPlanFormComponent) studyPlanFormComponent: StudyPlanFormComponent;
  academicProgramId: number;
  workplaceId: number;

  constructor(private rest: RESTService, private activatedRoute: ActivatedRoute, private previousRoute: PreviousRouteService,
              private dialogService: DialogService) {
    this.workplaceId = parseInt(this.activatedRoute.snapshot.params['workplaceId'], 10);
    this.academicProgramId = parseInt(this.activatedRoute.snapshot.params['academicProgramId'], 10);
  }

  ngOnInit() {
    const studyPlan = new StudyPlan();
    studyPlan.academicProgram = new AcademicProgram(this.academicProgramId, null, null, new Workplace(this.workplaceId));
    this.studyPlanFormComponent.initStudyPlanForm(studyPlan);
  }

  submit() {
    const studyPlan = this.studyPlanFormComponent.getStudyPlan();
    if (studyPlan) {
      this.rest.submitAsync<StudyPlan>('post', 'studyPlan', studyPlan, this.previousRoute.getPreviousUrl());
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.previousRoute.getPreviousUrl());
  }

}
