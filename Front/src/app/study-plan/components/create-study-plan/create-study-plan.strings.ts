export const labels = {
  formTitle: 'Registrar Plan de Estudios',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
