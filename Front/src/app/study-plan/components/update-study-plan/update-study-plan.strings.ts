export const labels = {
  formTitle: 'Editar Plan de Estudios',
  formSubtitle: 'Los campos marcados con (*) son obligatorios',
  submit: 'Finalizar',
  cancel: 'Cancelar'
};
