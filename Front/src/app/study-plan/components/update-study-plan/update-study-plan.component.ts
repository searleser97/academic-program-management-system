import {Component, OnInit, ViewChild} from '@angular/core';
import {labels} from './update-study-plan.strings';
import {FormContainerLabels} from '../../../shared/classes/form-container-labels';
import {StudyPlanFormComponent} from '../study-plan-form/study-plan-form.component';
import {RESTService} from '../../../shared/services/REST.service';
import {StudyPlan} from '../../classes/study-plan';
import {ActivatedRoute} from '@angular/router';
import {DialogService} from '../../../shared/modules/material/services/dialog.service';
import {PreviousRouteService} from '../../../shared/services/previous-route.service';

@Component({
  selector: 'app-update-study-plan',
  templateUrl: './update-study-plan.component.html',
  styleUrls: ['./update-study-plan.component.scss']
})
export class UpdateStudyPlanComponent implements OnInit {

  labels: FormContainerLabels = labels;
  @ViewChild(StudyPlanFormComponent) studyPlanFormComponent: StudyPlanFormComponent;
  studyPlanId: number;

  constructor(private rest: RESTService, private activatedRoute: ActivatedRoute, private dialogService: DialogService,
              private previousRoute: PreviousRouteService) {
    this.studyPlanId = activatedRoute.snapshot.params['id'];
  }

  ngOnInit() {
    this.getStudyPlan(this.studyPlanId);
  }

  async getStudyPlan(studyPlanId) {
    const studyPlan = await this.rest.getAsync<StudyPlan>('studyPlan/' + studyPlanId);
    if (studyPlan) {
      this.studyPlanFormComponent.initStudyPlanForm(studyPlan);
    }
  }

  submit() {
    const studyPlan = this.studyPlanFormComponent.getStudyPlan();
    if (studyPlan) {
      this.rest.submitAsync<StudyPlan>('patch', 'studyPlan', studyPlan, this.previousRoute.getPreviousUrl());
    }
  }

  cancel() {
    this.dialogService.cancelDialog(this.previousRoute.getPreviousUrl());
  }
}
