import {Component, Input, OnInit} from '@angular/core';
import {labels, placeholders} from './find-study-plan.strings';
import {FormControl, FormGroup} from '@angular/forms';
import {Observable} from 'rxjs';
import {FormService} from '../../../shared/services/form.service';
import {StudyPlan} from '../../classes/study-plan';
import {RESTService} from '../../../shared/services/REST.service';
import {AcademicProgram} from '../../../academic-program/classes/academic-program';
import {UserService} from '../../../shared/services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-find-study-plan',
  templateUrl: './find-study-plan.component.html',
  styleUrls: ['./find-study-plan.component.scss']
})
export class FindStudyPlanComponent implements OnInit {

  @Input() academicProgramId: number;
  @Input() workplaceId: number;

  labels = labels;
  placeholders = placeholders;

  searchAcademicProgramCtrl = new FormControl();

  form: FormGroup;

  academicPrograms: AcademicProgram[];
  filteredAcademicPrograms: Observable<AcademicProgram[]>;

  displayedColumns = [
    'id',
    'year',
    'modality',
    'studyPlanStatus',
    'edit'
  ];

  studyPlans: StudyPlan[];

  constructor(private formService: FormService, private rest: RESTService, private userService: UserService,
              private router: Router) {
    this.form = new FormGroup({
      academicProgram: new FormControl({})
    });
  }

  ngOnInit() {
    this.asyncRequests();
  }

  async asyncRequests() {
    if (!await this.getAcademicProgramsByWorkplaceId(this.workplaceId) ||
      !await this.getStudyPlansByAcademicProgramId(this.academicProgramId)) {
      return false;
    }
    return true;
  }

  async getAcademicProgramsByWorkplaceId(workplaceId) {
    this.academicPrograms = await this.rest.getAsync<AcademicProgram[]>('academicProgram/academicProgramsByWorkPlaceId/' +
      workplaceId);
    if (!this.academicPrograms) {
      return false;
    }
    this.filteredAcademicPrograms = this.formService.getFilteredItems(this.searchAcademicProgramCtrl, this.academicPrograms);
    this.form.get('academicProgram').setValue(this.academicPrograms.find(opt => opt.id === this.academicProgramId));
    return true;
  }

  async getStudyPlansByAcademicProgramId(academicProgramId) {
    this.studyPlans = await this.rest.getAsync<StudyPlan[]>('studyPlan/studyPlansByAcademicProgramId/' + academicProgramId);
    if (!this.studyPlans) {
      return false;
    }
    return true;
  }

  edit(id) {
    this.router.navigate(['planesdeestudios/editar/' + id]);
    console.log('edit: ' + id);
  }

  view(id) {
    this.router.navigate(['planesdeestudios/leer/' + id]);
    console.log('view: ' + id);
  }

  add() {
    this.router.navigate(['planesdeestudios/registrar/' + this.workplaceId + '/' + this.academicProgramId]);
    console.log('add');
  }

}
