import {ComboBoxOption} from '../../../shared/classes/combo-box-option';

export const labels = {
  notFound: '-- No se encontraron opciones --',
  academicProgram: 'Programa Academico',
  year: 'Año',
  mode: 'Modalidad',
  totalTEPICCredits: 'Creditos Totales TEPIC',
  totalSATCACredits: 'Creditos Totales SATCA',
  totalTheoryHours: 'Total Horas/Teoría',
  totalPracticeHours: 'Total Horas/Práctica',
  errors: {
    year: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
      maxlength: 'Escribe información válida',
      minlength: 'Escribe información válida'
    },
    totalTEPICCredits: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    totalSATCACredits: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    totalTheoryHours: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    totalPracticeHours: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    mode: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    },
    academicProgram: {
      required: 'Este campo es requerido',
      pattern: 'Escribe información válida',
    }
  }
};

export const placeholders = {
  search: 'Buscar...',
  year: '2008',
  mode: 'Escolarizada',
  totalTEPICCredits: '2.3',
  totalSATCACredits: '1.8',
  totalTheoryHours: '16',
  totalPracticeHours: '9'
};

export const modes = [
  'Escolarizado',
  'No Escolarizada',
  'Mixta'
];

export const academicPrograms = [
  new ComboBoxOption(0, 'Ingenieria en Sistemas Computacionales'),
  new ComboBoxOption(1, 'Ingenieria en Sistemas Automotrices')
];
