import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {labels, placeholders} from './study-plan-form.strings';
import {Observable} from 'rxjs';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {FormService} from '../../../shared/services/form.service';
import {RESTService} from '../../../shared/services/REST.service';
import {AcademicProgram} from '../../../academic-program/classes/academic-program';
import {UserService} from '../../../shared/services/user.service';
import {StudyPlan} from '../../classes/study-plan';
import {Modality} from '../../../modality/classes/modality';
import {FLOAT_REGEX, INT_REGEX} from '../../../../environments/environment';

@Component({
  selector: 'app-study-plan-form',
  templateUrl: './study-plan-form.component.html',
  styleUrls: ['./study-plan-form.component.scss']
})
export class StudyPlanFormComponent implements OnInit {

  @Output() onSubmit = new EventEmitter<boolean>();
  @Input() academicProgramId: string;

  labels = labels;
  placeholders = placeholders;

  modes: Modality[];
  filteredModes: Observable<Modality[]>;

  academicPrograms: AcademicProgram[];
  filteredAcademicPrograms: Observable<AcademicProgram[]>;

  searchModesCtrl = new FormControl();
  searchAcademicProgramCtrl = new FormControl();


  studyPlanForm: FormGroup;
  studyPlan: StudyPlan;

  constructor(private formService: FormService, private rest: RESTService, private userService: UserService) {
    this.studyPlanForm = this.formService.newGroup(new StudyPlan(
      [0],
      [0, Validators.compose([Validators.pattern(INT_REGEX), Validators.required])],
      [{value: 0, disabled: true}, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [0, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [0, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [0, Validators.compose([Validators.pattern(FLOAT_REGEX), Validators.required])],
      [{}, Validators.compose([Validators.required])],
      [{}, Validators.compose([Validators.required])]
    ));
    this.studyPlanForm.get('totalTheoryHours').valueChanges.subscribe(val => {
      this.setTEPICCredits();
    });
    this.studyPlanForm.get('totalPracticeHours').valueChanges.subscribe(val => {
      this.setTEPICCredits();
    });
  }

  setTEPICCredits() {
    this.studyPlanForm.get('totalTEPICCredits')
      .setValue(2 * parseFloat(this.studyPlanForm.get('totalTheoryHours').value) +
        parseFloat(this.studyPlanForm.get('totalPracticeHours').value));
  }

  ngOnInit() {
  }

  getErrors(controlName: string): string[] {
    return Object.keys(this.studyPlanForm.get(controlName).errors || {});
  }

  getStudyPlan(): StudyPlan {
    for (const controlName in this.studyPlanForm.controls) {
      if (this.studyPlanForm.controls.hasOwnProperty(controlName)) {
        this.studyPlanForm.controls[controlName].markAsTouched();
      }
    }
    return this.studyPlanForm.invalid ? null : this.studyPlanForm.getRawValue();
  }

  async initStudyPlanForm(studyPlan: StudyPlan) {
    this.studyPlanForm.setValue(studyPlan);
    this.studyPlan = studyPlan;

    if (!await this.getModes() || !await this.getAcademicProgramsByWorkplaceId(this.studyPlan.academicProgram.workplace.id)) {
      return false;
    }
    return true;
  }

  async getAcademicProgramsByWorkplaceId(workplaceId) {
    this.academicPrograms = await this.rest.getAsync<AcademicProgram[]>('academicProgram/academicProgramsByWorkPlaceId/' + workplaceId);
    console.log(this.academicPrograms);
    if (!this.academicPrograms) {
      return false;
    }
    this.filteredAcademicPrograms = this.formService.getFilteredItems(this.searchAcademicProgramCtrl, this.academicPrograms);
    this.studyPlanForm.get('academicProgram')
      .setValue(this.academicPrograms.find(opt => opt.id === this.studyPlan.academicProgram.id));
    return true;
  }

  async getModes() {
    this.modes = await this.rest.getAsync<Modality[]>('modality');
    if (!this.modes) {
      return false;
    }
    this.filteredModes = this.formService.getFilteredItems(this.searchModesCtrl, this.modes);
    this.studyPlanForm.get('modality').setValue(this.modes.find(opt => opt.id === this.studyPlan.modality.id));
    return true;
  }

}
