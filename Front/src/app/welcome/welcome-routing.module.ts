import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WelcomeComponent} from './components/welcome/welcome.component';
import {AuthGuardService} from '../auth/services/auth-guard.service';

export const welcomeRoutes: Routes = [
  {path: 'inicio', component: WelcomeComponent, canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forChild(welcomeRoutes)],
  exports: [RouterModule]
})
export class WelcomeRoutingModule { }
