import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { WelcomeRoutingModule } from './welcome-routing.module';
import { WelcomeComponent } from './components/welcome/welcome.component';
import {MaterialModule} from '../shared/modules/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    WelcomeRoutingModule,
    MaterialModule
  ],
  exports: [
    WelcomeComponent
  ],
  declarations: [WelcomeComponent]
})
export class WelcomeModule { }
