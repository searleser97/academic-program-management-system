import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthorComponent} from './ua-management/components/author/author.component';
import {RegistrarContenidoComponent} from './ua-management/components/registrar-contenido/registrar-contenido.component';
import {RegistrarEvaluacionAcreditacionComponent} from './ua-management/components/registrar-evaluacion-acreditacion/registrar-evaluacion-acreditacion.component';
import {AddAccreditationTypeComponent} from './ua-management/components/add-accreditation-type/add-accreditation-type.component';
import {RegistrarTemasUaComponent} from './ua-management/components/registrar-temas-ua/registrar-temas-ua.component';
import {RegistrarSubtemasUaComponent} from './ua-management/components/registrar-subtemas-ua/registrar-subtemas-ua.component';
import {RegistrarEvalLearningsComponent} from './ua-management/components/registrar-eval-learnings/registrar-eval-learnings.component';

const routes: Routes = [
  {path: 'ATHR', component: AuthorComponent},
  {path: 'RC/:id', component: RegistrarContenidoComponent},
  {path: 'RC', component: RegistrarContenidoComponent},
  {path: 'REA/:id', component: RegistrarEvaluacionAcreditacionComponent},
  {path: 'REA', component: RegistrarEvaluacionAcreditacionComponent},
  {path: 'AAT', component: AddAccreditationTypeComponent},
  {path: 'RTUA/:id', component: RegistrarTemasUaComponent},
  {path: 'RTUA', component: RegistrarTemasUaComponent},
  {path: 'RSUA/:id/:id2', component: RegistrarSubtemasUaComponent},
  {path: 'REDA/:id', component: RegistrarEvalLearningsComponent},
  {path: 'REDA', component: RegistrarEvalLearningsComponent},
  {
    path: '**',
    redirectTo: 'error-404',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
