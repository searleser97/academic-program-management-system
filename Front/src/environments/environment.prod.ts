export const environment = {
  production: true
};

export const SERVER_URL = 'https://apmsrest.herokuapp.com/';
export const ALFA_REGEX = '[A-Za-záéíóúüñÁÉÍÓÚÜÑ]+(((-|\')[A-Za-záéíóúüñÁÉÍÓÚÜÑ]+)|\\.)?' +
  '( [A-Za-záéíóúüñÁÉÍÓÚÜÑ]+(((-|\')[A-Za-záéíóúüñÁÉÍÓÚÜÑ]+)|\\.)?)*';
export const FLOAT_REGEX = '\\d+(\\.\\d{1,2})?';
export const INT_REGEX = '\\d{4}';
export const ALFANUM_REGEX = '[\\wéíóúüñÁÉÍÓÚÜÑ]+( [\\wáéíóúüñÁÉÍÓÚÜÑ]+)*';
export const PASSWD_REGEX = '[^áéíóúüñÁÉÍÓÚÜÑ\\s]{6,}';
export const URL_PREFIX = 'public/';
