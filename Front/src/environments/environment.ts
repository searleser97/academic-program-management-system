// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false
};

export const SERVER_URL = 'http://localhost:8080/';
export const ALFA_REGEX = '[A-Za-záéíóúüñÁÉÍÓÚÜÑ]+(((-|\')[A-Za-záéíóúüñÁÉÍÓÚÜÑ]+)|\\.)?' +
  '( [A-Za-záéíóúüñÁÉÍÓÚÜÑ]+(((-|\')[A-Za-záéíóúüñÁÉÍÓÚÜÑ]+)|\\.)?)*';
export const FLOAT_REGEX = '\\d+(\\.\\d{1,2})?';
export const INT_REGEX = '\\d{4}';
export const ALFANUM_REGEX = '[\\wéíóúüñÁÉÍÓÚÜÑ]+( [\\wáéíóúüñÁÉÍÓÚÜÑ]+)*';
export const PASSWD_REGEX = '[^áéíóúüñÁÉÍÓÚÜÑ\\s]{6,}';
export const URL_PREFIX = '';

/*
 * For easier debugging in development modality, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production modality because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
